module.exports = function (game) {

  var preloader = {}

  preloader.preload = function () {
    // this.preloadBg = this.add.sprite(0,0, 'preloaderBg')
    var text = 'loading'
      , style = { font: '65px Arial', fill: 'white', align: 'center' }

    game.add.text(game.world.centerX - 130, game.world.centerY, text, style)

    game.load.atlasJSONHash('editorAtlas', 'images/editor-atlas.png#grunt-cache-bust', 'images/editor-atlas.json#grunt-cache-bust')
    game.load.atlasJSONHash('images','/images/default-atlas.png#grunt-cache-bust','/images/default-atlas.json#grunt-cache-bust')
  }

  preloader.create = function () {
    game.state.start('editor')
  }

  return preloader
}
