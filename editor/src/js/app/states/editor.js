var gameProperties = require('properties')
  , properties = require('editor-properties')
  , _ = require('lodash')
  , EntityPallet = require('../plugins/entity-palette')
  , setupEntityBuilder = require('../util/entity-builder')
  , moduleManager = require('../util/module-manager')
  , persistanceService = require('../util/persistance-service')
  , Selection = require('../plugins/selection')
  , CameraManager = require('../plugins/camera-manager')
  , Grid = require('../plugins/grid')
  , LevelLogicOverlay = require('../plugins/level-logic-overlay')
  , GroupManager = require('../plugins/group-manager/group-manager')
  , AspectRatioOverlays = require('../plugins/aspect-ratio-overlays')
  , BackgroundManager = require('../plugins/background-manager')
  , DraggingSelectionOverlay = require('../plugins/dragging-selection-overlay')
  , UImanager = require('../plugins/ui-manager')
  , setupWorkspace = require('../util/workspace')

require('jQuery')

module.exports = function (game) {

  var editor = {}
    , entityBuilder
    , workspace = null

    // Plugins.
    , plugins =
      { aspectRatioOverlays:null
      , grid:null
      , cameraManager:null
      , selection:null
      , entityPalette:null
      , uiManager:null
      , groupManager:null
      , levelLogicOverlay:null
      , draggingSelectionOverlay:null
      , backgroundManager: null
      }

  function configureDomEvents() {
    persistanceService.setupFileUploadEvent(editor.displayLevelData)
    setupUnsavedChangesCheck()
  }

  function setupUnsavedChangesCheck() {
    window.onbeforeunload = function() {
      if (workspace.hasUnsavedChanges()) {
        return 'you have unsaved changes'
      }
    }
  }

  editor.preload = function () {

    game.world.setBounds(-Infinity, -Infinity, Infinity, Infinity)
    moduleManager.load()

    workspace = setupWorkspace(game)

    entityBuilder = setupEntityBuilder(game, workspace, moduleManager)

    centerGame()

    plugins.backgroundManager = game.plugins.add(BackgroundManager, workspace)
    plugins.grid = game.plugins.add(Grid, workspace)

    plugins.cameraManager = game.plugins.add(CameraManager, workspace, plugins)

    //we need to init the workspace here so that it creates the groups on top of the previous plugins, but below the ones created afterwards.
    workspace.init()

    plugins.aspectRatioOverlays = game.plugins.add(AspectRatioOverlays, workspace)
    plugins.levelLogicOverlay = game.plugins.add(LevelLogicOverlay, workspace)

    plugins.selection = game.plugins.add(Selection, workspace, entityBuilder)
    plugins.entityPalette = game.plugins.add(EntityPallet, workspace, moduleManager.getList(), plugins, entityBuilder)
    plugins.groupManager = game.plugins.add(GroupManager, workspace, plugins)
    plugins.draggingSelectionOverlay = game.plugins.add(DraggingSelectionOverlay, plugins)
    plugins.uiManager = game.plugins.add(UImanager, workspace, plugins)

    $(function() {
      configureDomEvents()
    })
  }

  editor.displayLevelData = function (loadedLevelData, filename) {
    workspace.clear()
    centerGame()
    plugins.cameraManager.resetCamera()

    workspace.setGroups(loadedLevelData.groups)

    _.each(loadedLevelData.entities, function(entity) {

      var group = workspace.getGroupByName(entity.group)
        , en = entityBuilder(entity, entity.x, entity.y, group, true, true)

      plugins.selection.setupEntityForSelection(en)
    }, this)

    workspace.setLevelName(filename.split('.')[0])
    workspace.updateLevelHash()

    workspace.backgroundColour = loadedLevelData.metaData.backgroundColour
    workspace.events.backgroundColourChanged.dispatch()

    game.camera.reset()
  }

  function centerGame() {
    //center the game into the center of the screen

    // TODO: we can pull this from the game properties, but I think it'd be best to create a screen size object
    // E.g. screenBorders.getDesktop()

    game.x = (game.width / 2) - (gameProperties.logicWidth / 2)
    game.y = (game.height / 2) - (gameProperties.logicHeight / 2)
  }

  return editor
}
