module.exports =
  { title: 'Phaser Level Editor'
  , description: 'A level editor for the Phaser framework.'
  , analyticsId: 'UA-53588707-1'
  , showStats: true

  , excludeFilePatterns: ['abstract', '.DS_Store']

 // , collisionAreaPath: 'F:\\PhaserLE\\src\\js\\game\\entities\\common\\collision_area.js'

  , defaultLevelName: 'untitled'

  // Grid size for selection to snap to.
  , grid:
    { x: 40
    , y: 40
    , offsetX:0
    , offsetY:0
    }

  , buttonPadding: 0
  , toolbox:
    { maxItemSize: { x: 50, y: 50 }
    , padding: { x: 10, y: 24 }
    }

  , showGameLogicSize: true
  , drawResolutionOutlines:
    {
      small:
      { x:320
      , y:240
      , visible: true
      }
    , medium:
      { x:480
      , y:320
      , visible: true
      }
    , large:
      { x:720
      , y:480
      , visible: true
      }
    , xlarge:
      { x:960
      , y:640
      , visible: true
      }
    , xxlarge:
      { x:1440
      , y:960
      , visible: true
      }
    }
  }
