var _ = require('lodash')

  // Instance variables.
 , moduleManager = {}
 , moduleList = {}

moduleManager.load = function () {
  
  _.each(window.modulePaths, function (path) {

    var cleanPath = path.replace(/\\/g, '/')
      , pathBits = cleanPath.split('/')
      , fileName = pathBits[pathBits.length - 1]
      , moduleName = fileName.split('.')[0]

    moduleList[moduleName] = require(path)
  })
}

moduleManager.create = function (moduleId, game, x, y) {
  return new moduleList[moduleId](game, x, y)
}

moduleManager.getList = function () {
  return moduleList
}

module.exports = moduleManager
