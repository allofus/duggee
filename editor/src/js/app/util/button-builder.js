var properties = require('editor-properties')

module.exports = function(game, currentX, currentY) {

  var addButton = function(image, callback, callback2, atlas, hoverFrame) {
    var button = game.add.sprite(currentX, currentY, atlas)
      , buttonArguments = arguments

    button.animations.add('idle',[image])
    button.animations.add('hover',[hoverFrame])
    button.animations.play('idle')

    button.fixedToCamera = true
    button.inputEnabled = true
    button.input.useHandCursor = true

    button.events.onInputOver.add(function(button) {
      button.animations.play('hover')
    })

    button.events.onInputOut.add(function(button) {
      button.animations.play('idle')
    })

    button.events.onInputDown.add(function(button) {
      button.animations.play('idle')
    })

    button.events.onInputDown.add(function() {
      // Take any arguments passed after callback and pass them along to it.
      callback.apply(this, Array.prototype.slice.call(buttonArguments, 2))
    }, this)

    currentX += button.width + properties.buttonPadding
  }

  return addButton
}
