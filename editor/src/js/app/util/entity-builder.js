var _ = require('lodash')
  , Phaser = require('Phaser')
  , CollisionArea = require('./collision-area')

module.exports = function(game, workspace, moduleManager) {

  var newId = 0
    , functionsToNerf = ['update', 'postUpdate', 'preUpdate']
    , functionsToNerf_collisionEntity = ['postUpdate', 'preUpdate']
    , emptyFunction = function() {}

  var entityBuilder = function(entityData, x, y, group, addToWorkspace, isInEditor) {

    group = group || workspace.getActiveGroup()

    var entity
    // TODO: needs to be the currentlySeletedGroup
    if (entityData.group === 'collisions' || group.name === 'collisions') {
      //collision entities need a rectangle passed to this for their size. We also need to leave the update method as it was.
      entity = new CollisionArea(game, x, y, new Phaser.Rectangle(entityData.x,entityData.y,entityData.width,entityData.height), isInEditor)
      nerfUpdateFunction(entity, functionsToNerf_collisionEntity)
    } else {
      entity = moduleManager.create(entityData.levelEntityId, game, x, y)
      nerfUpdateFunction(entity, functionsToNerf)
    }

    if (isInEditor) {
      entity.inputEnabled = true
      entity.input.useHandCursor = true
      entity.input.priorityID = ++workspace.priorityID
      entity.mouseOffset = { x: entity.width / 2, y: entity.height / 2 }
    }
    entity.levelEntityId = entityData.levelEntityId
    entity.levelEditorProperties = entityData.properties ? entityData.properties : {}

    if (addToWorkspace) {
      workspace.entities.push(entity)
    }

    if (entity.init) {
      entity.init(isInEditor)
    }

    game.add.existing(entity)
    group.add(entity)
    entity.group = group

    entity.id = newId++

    return entity
  }

  function nerfUpdateFunction(entity, functions) {
    _.each(functions, function(property) {
      entity[property] = emptyFunction
    })
  }

  return entityBuilder
}
