var properties = require('editor-properties')
  , Phaser = require('Phaser')
  , _ = require('lodash')
  , md5 = require('blueimp-md5').md5
  , emptyArray = require('../util/empty-array')
  , levelPrinter = require('../util/level-printer')

  // Instance variables.
  , workspace =
    { entities: []
    , draggingEntity: null
    , levelName: properties.defaultLevelName
    , lastSavedLevelHash: null
    , events:
      { levelNameChanged: new Phaser.Signal()
      , levelCleared: new Phaser.Signal()
      , groupsSet: new Phaser.Signal()
      , groupRemoved: new Phaser.Signal()
      , activeGroupChanged: new Phaser.Signal()
      , aspectRatioSettingChanged: new Phaser.Signal()
      , gameLogicSettingChanged: new Phaser.Signal()
      , selectedEntityChanged: new Phaser.Signal()
      , gridChanged: new Phaser.Signal()
      , backgroundColourChanged: new Phaser.Signal()
      , inputPriorityReset: new Phaser.Signal()
      }
    , groups: []
    , container: null
    , priorityID: 0
    , backgroundColour: '0x000000'
    }

  // Private.
  , activeGroup = null

module.exports = function(game) {

  workspace.init = function() {
    createDefaultGroup()
    workspace.updateLevelHash()
  }

  workspace.deleteGroup = function(name, createDefault) {

    createDefault = _.isUndefined(createDefault) ? true : createDefault

    var group = _.find(workspace.groups, { name: name })
      , newGroups = []

    _.each(workspace.groups, function(group) {
      if (group.name !== name) {
        newGroups.push(group)
      }
    })

    group.destroy()

    workspace.groups = newGroups
    workspace.events.groupRemoved.dispatch(name)

    if (createDefault && workspace.groups.length <= 0) {
      createDefaultGroup()
    }

    workspace.setActiveGroup()
  }

  workspace.addGroup = function(groupName, properties, collisionData) {

    var group = game.add.group(workspace.container, groupName)
    group.levelEditorProperties = properties || {}
    group.levelEditorCollisionData = collisionData || []

    var lastGroup = _.max(workspace.groups, 'z')

    if (lastGroup && lastGroup.z >= 0) {
      group.z = lastGroup.z + 1
    } else {
      group.z = 0
    }

    workspace.groups.push(group)
    workspace.setActiveGroup(group)

    return group
  }

  workspace.getGroupByName = function(name) {
    return _.find(workspace.groups, { name: name })
  }

  workspace.setLevelName = function (text) {
    workspace.levelName = text
    workspace.events.levelNameChanged.dispatch(text)
  }

  workspace.lockGroup = function (group) {

    group.isLocked = !group.isLocked

    group.forEach(function(entity) {
      entity.isLocked = group.isLocked
    })

    resetInputPriority()
  }

  workspace.updateLevelHash = function() {
    workspace.hashLevelData = hashLevelData()
  }

  workspace.clear = function () {
    // hideOpenOptionOverlays()
    //plugins.selection.clearSelectedEntities()

    _.each(workspace.entities, function (item) {
      item.destroy()
    })

    emptyArray(workspace.entities)
    deleteAllGroups()

    //entityBuilder.setLevelData(workspace.entities)
    //TODO - We need to dispatch signals here.

    workspace.updateLevelHash()

    workspace.events.levelCleared.dispatch()
  }

   workspace.hasUnsavedChanges = function () {
    return hashLevelData() !== workspace.hashLevelData
  }

  workspace.setGroups = function(groups) {

    deleteAllGroups(false)

    _.each(groups, function(group) {
      workspace.addGroup(group.name, group.properties, group.collisionData)
    })

    workspace.events.groupsSet.dispatch()
  }

  workspace.getActiveGroup = function() {
    return activeGroup
  }

  workspace.setActiveGroupViaEntityClick = function(group) {
    workspace.setActiveGroup(group, true)
  }

  workspace.setActiveGroup = function(group, viaEntityClick) {

    viaEntityClick = _.isUndefined(viaEntityClick) ? false : viaEntityClick

    if (workspace.groups.length <= 0) {
      return
    }

    if (_.isUndefined(group)) {
      group = _.last(workspace.groups)
    }

    activeGroup = group
    resetInputPriority()
    workspace.events.activeGroupChanged.dispatch(group.name, viaEntityClick)

    console.info('Active Group: %s', group.name)
  }

  workspace.orderGroups = function() {
    workspace.groups = _.sortBy(workspace.groups, 'z')
    workspace.container.sort()
    workspace.events.groupsSet.dispatch()
  }

  workspace.moveGroup = function (workspace, group, forward) {
  var currentPosition = group.z
    , lastGroup = _.max(workspace.groups, 'z')
    , lastPosition = lastGroup.z
    , newPosition = forward ? currentPosition + 1 : currentPosition - 1

  if ((!forward && currentPosition === 0) || (forward && currentPosition === lastPosition)) {
    return
  }

  var groupAtDestination = _.find(workspace.groups, { z: newPosition })
  groupAtDestination.z = currentPosition
  group.z = newPosition
  workspace.orderGroups()
}

  function resetInputPriority() {

    workspace.priorityID = 0

    _.each(workspace.groups, function(group) {

      if (group.name !== activeGroup.name) {
        workspace.setGroupInputPriority(group)
      }
    })

    workspace.setGroupInputPriority(activeGroup)
    workspace.events.inputPriorityReset.dispatch(workspace.priorityID)
  }

  workspace.setGroupInputPriority = function(group) {
    group.forEach(function (entity) {
      if (group.isLocked) {
        // A priorityID of -1 prevents input events from being triggered.
        entity.input.priorityID = -1
      } else {
        entity.input.priorityID = ++workspace.priorityID
      }
    })
  }

  function hashLevelData() {
    return md5(JSON.stringify(levelPrinter(workspace)))
  }

  function deleteAllGroups(createDefault) {

    createDefault = _.isUndefined(createDefault) ? true : createDefault

    _.each(workspace.groups, function(group) {
      workspace.deleteGroup(group.name, createDefault)
    })
  }

  function createDefaultGroup() {

    if (!workspace.container) {
      workspace.container = game.add.group(game.world, 'workspace')
    }

    workspace.addGroup('defaultGroup')
    workspace.addGroup('collisions')
    workspace.events.groupsSet.dispatch()
  }

  return workspace
}
