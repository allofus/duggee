var _ = require('lodash')

module.exports = function (workspace) {

  var packedLevelData = {}

  packedLevelData.entities = _.map(workspace.entities, function (entity) {

    var data =
      { levelEntityId: entity.levelEntityId
      , properties: entity.levelEditorProperties
      , x: entity.x
      , y: entity.y
      , width: entity.width
      , height: entity.height
      , group: entity.group.name
      }

    return data
  })

  packedLevelData.metaData =
    { levelName: workspace.levelName
    , backgroundColour : workspace.backgroundColour
    }

  packedLevelData.groups = _.map(workspace.groups, function (group) {

    var data =
      { name: group.name
      , properties: group.levelEditorProperties
      , collisionData: group.levelEditorCollisionData
      }

    return data
  })

  return packedLevelData
}

