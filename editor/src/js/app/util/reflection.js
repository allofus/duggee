var reflection = {}
  , _ = require('lodash')

reflection.exists = function(object, key) {
  return typeof reflection.lookupValue(object, key) !== 'undefined'
}

reflection.get = function(object, key) {

  var parts = key.split('.')
    , currentValue = object

  _.each(parts, function(key) {

    // If the object path can't be found.
    if (typeof currentValue === 'undefined' || typeof currentValue[key] === 'undefined') {
      return currentValue = undefined
    }

    currentValue = currentValue[key]
  })

  return currentValue
}

reflection.set = function(object, key, value) {
  var parts = key.split('.')
    , finalKey = parts.pop()
    , currentDestination = object

  _.each(parts, function(part) {

    if (typeof currentDestination[part] === 'undefined') {
      currentDestination[part] = {}
    }

    currentDestination = currentDestination[part]
  })

  currentDestination[finalKey] = value
}

reflection.flatten = function(source) {

  var hash = {}

  function step(object, currentKey) {
    _.forIn(object, function(value, key) {

      var newKey = currentKey ? currentKey + '.' + key : key

      if (_.isObject(value)) {
        step(value, newKey)
      } else {
        hash[newKey] = value
      }
    })
  }

  step(source)

  return hash
}

module.exports = reflection
