var Phaser = require('Phaser')
  , editorProperties = require('editor-properties')
  , properties = require('properties')

//  Here is a custom game object
var CollisionArea = function ( game, x, y, rect , isLE) {
  Phaser.Sprite.call(this, game, x, y, '')

  this.x = rect.x
  this.y = rect.y
  this.width = rect.width
  this.height = rect.height
  this.doesCollide = true

  if (properties.debugPlatforms || isLE) {
    this.collisionGraphics = this.game.add.graphics(0,0)
    this.collisionGraphics.beginFill(0x00FF00, .8)
    this.collisionGraphics.drawRect(0, 0, this.width, this.height)
    this.collisionGraphics.endFill()
  }

  this.game.physics.arcade.enable(this)
  this.body.immovable = true

  this.inputEnabled = true

  this.levelEntityId = 'collision_area'
}

CollisionArea.prototype = Object.create(Phaser.Sprite.prototype)
CollisionArea.prototype.constructor = CollisionArea

CollisionArea.prototype.update = function() {
  if (this.collisionGraphics) {
    this.collisionGraphics.x = this.x
    this.collisionGraphics.y = this.y
  }
}

module.exports = CollisionArea
