var modal = require('modal')
  , entityOptionsTemplate = require('../../../../templates/modals/entity-property-form.jade')
  , optionsRowTemplate = require('../../../../templates/modals/entity-option.jade')
  , objectMapper = require('../object-mapper')

module.exports = function(entity, snapGridCallback) {


  var entityProperties = objectMapper.flatten(entity.levelEditorProperties)
    , modalOptions =
    { title: entity.levelEntityId
    , overlayClassName: 'inspector'
    , content: $(entityOptionsTemplate({ object: entityProperties }))
    , buttons: []
    }

  modal(modalOptions)

  var $modalElement = $('.inspector')
    , $form = $('.inspector form')

  $modalElement.find('.add').click(function() {

    $form.find('input[name=key]').unbind('blur')

    $modalElement.find('form').append(optionsRowTemplate())

    $form.find('input[name=key]').bind('blur', function(event) {
      objectMapper.getValueFromInput(event, entity)
    })
  })

  $modalElement.find('.save').click(function() {
    objectMapper.map(entity, $form)
  })

  $modalElement.find('.snap-grid').click(function() {
    snapGridCallback(entity)
  })
}

