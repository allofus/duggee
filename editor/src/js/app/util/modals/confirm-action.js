var modal = require('modal')

module.exports =  function (callback) {
  var modalOptions =
    { title: 'Clear Level'
    , content: 'Are you sure about that?'
    , overlayClassName: 'confirm'
    , buttons:
      [ { text: 'Confirm', event: 'confirm', className: 'btn-danger', keyCodes: [13] }
      , { text: 'Cancel', event: 'cancel', className: 'btn-default', keyCodes: [27] }
      ]
    }

  modal(modalOptions)
    .on('confirm', function () {
      callback()
    })
}
