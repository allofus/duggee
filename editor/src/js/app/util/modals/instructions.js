var modal = require('modal')
  , instructionsTemplate = require('../../../../templates/modals/instructions.jade')
  , properties = require('editor-properties')
 // , optionsRowTemplate = require('../../../../templates/modals/entity-option.jade')

module.exports = function(workspace,game) {

  var modalOptions =
    { overlayClassName: 'instructions'
    , title: 'Instructions'
    , content: $(instructionsTemplate())
    , buttons: [{ text: 'Cancel', event: 'cancel', className: 'btn-default'  }]
    }

  modal(modalOptions)

  $( '#smallAspectRatio' ).change(function() {
    properties.drawResolutionOutlines.small.visible = !properties.drawResolutionOutlines.small.visible
    workspace.events.aspectRatioSettingChanged.dispatch()
  })

  $( '#mediumAspectRatio' ).change(function() {
    properties.drawResolutionOutlines.medium.visible = !properties.drawResolutionOutlines.medium.visible
    workspace.events.aspectRatioSettingChanged.dispatch()
  })

  $( '#largeAspectRatio' ).change(function() {
    properties.drawResolutionOutlines.large.visible = !properties.drawResolutionOutlines.large.visible
    workspace.events.aspectRatioSettingChanged.dispatch()
  })

  $( '#xlargeAspectRatio' ).change(function() {
    properties.drawResolutionOutlines.xlarge.visible = !properties.drawResolutionOutlines.xlarge.visible
    workspace.events.aspectRatioSettingChanged.dispatch()
  })

  $( '#xxlargeAspectRatio' ).change(function() {
    properties.drawResolutionOutlines.xxlarge.visible = !properties.drawResolutionOutlines.xxlarge.visible
    workspace.events.aspectRatioSettingChanged.dispatch()
  })

  $( '#gridWidth' ).blur(function() {
    properties.grid.x = parseInt($( '#gridWidth' ).val())
  })

  $( '#gridHeight' ).blur(function() {
    properties.grid.y = parseInt($( '#gridHeight' ).val())
  })

  $( '#gridOffsetX' ).blur(function() {
    properties.grid.offsetX = parseInt($( '#gridOffsetX' ).val())
  })

  $( '#gridOffsetY' ).blur(function() {
    properties.grid.offsetY = parseInt($( '#gridOffsetY' ).val())
  })

  $( '#gameLogic' ).change(function() {
    properties.showGameLogicSize = !properties.showGameLogicSize
    workspace.events.gameLogicSettingChanged.dispatch()
  })

  $( '#backgroundColour' ).change(function() {
    //validation found here: http://stackoverflow.com/a/8027444
    if (/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test($( '#backgroundColour' ).val())) {
      workspace.backgroundColour = $( '#backgroundColour' ).val()
      workspace.events.backgroundColourChanged.dispatch()
    }
  })

 // $modalElement.find('.add').click(function() {
   // $modalElement.find('form').append(optionsRowTemplate())
  //})
}
