var Phaser = require('Phaser')
  , _ = require('lodash')

  , showCreateGroupModal = require('./create-group-modal')
  , showGroupOptionModal = require('./show-group-inspection-modal')

  // Instance variables.
  , workspace = null
  , GroupManager = null
  , tabs = null
  , addTabButton = null

  , fontStyle = { font: '15px Arial', fill: 'black', align: 'left' }

  , initalX = 4
  , tabPos =
    { y: 25
    , x: initalX
    }

GroupManager = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

GroupManager.prototype = Object.create(Phaser.Plugin.prototype)
GroupManager.prototype.constructor = GroupManager

GroupManager.prototype.init = function (_workspace) {
  workspace = _workspace

  workspace.events.groupsSet.add(_.bind(this.refreshGroups, this))
  workspace.events.groupRemoved.add(_.bind(this.refreshGroups, this))
  workspace.events.activeGroupChanged.add(setActiveGroup)

  this.refreshGroups()
  workspace.setActiveGroup(workspace.groups[0])
}

GroupManager.prototype.refreshGroups = function () {

  resetGraphics()
  tabPos.x = initalX

  _.each(workspace.groups, function(group) {

    var tab =
        { sprite: this.game.add.sprite(tabPos.x, tabPos.y, 'editorAtlas', 'tab.png')
        , text: this.game.add.text(0,tabPos.y + 8, group.name, fontStyle)
        , name: group.name
        , group: group
        }

    centerTabText(tab)
    tab.text.fixedToCamera = true
    tab.sprite.fixedToCamera = true
    tab.sprite.inputEnabled = true
    tab.sprite.input.useHandCursor = true

    tab.sprite.events.onInputDown.add(function() {
      workspace.setActiveGroup(group)
      showGroupOptionModal(workspace, tab)
    })

    tab.sprite.events.onInputOver.add(function(sprite) {
      group.forEach(function(entity) {
        entity.drawHighlightRect(0.5)
      })

      if (sprite.frameName !== 'tab-selected.png') {
        sprite.frameName = 'tab-hover.png'
      }
    })

    tab.sprite.events.onInputOut.add(function(sprite) {
      group.forEach(function(entity) {
        entity.highlightGraphics.clear()
      })

      if (sprite.frameName !== 'tab-selected.png') {
        sprite.frameName = 'tab.png'
      }
    })

    tabPos.x += tab.sprite.width + 10

    tabs.push(tab)
  }, this)

  drawAddGroupbutton.call(this)
}

function centerTabText( tab) {
  tab.text.x = tab.sprite.x + (tab.sprite.width / 2) - (tab.text.width / 2)
}

function resetGraphics() {
  _.each(tabs, function(tab) {
    tab.sprite.destroy()
    tab.text.destroy()
  })

  if (addTabButton) {
    addTabButton.destroy()
  }

  tabs = []
}

function drawAddGroupbutton() {
  addTabButton = this.game.add.sprite(tabPos.x - 5, tabPos.y + 3, 'editorAtlas', 'add-group.png')

  addTabButton.fixedToCamera = true
  addTabButton.inputEnabled = true
  addTabButton.input.useHandCursor = true

  var self = this

  addTabButton.events.onInputDown.add(function () {
    showCreateGroupModal.call(self, workspace)
  })
}

function setActiveGroup(groupName) {
  _.each(tabs, function(tab) {
    tab.sprite.frameName = 'tab.png'
  })

  var activeTab = _.find(tabs, { name: groupName })

  if (activeTab) {
    activeTab.sprite.frameName = 'tab-selected.png'
  }
}

module.exports = GroupManager
