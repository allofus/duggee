var modal = require('modal')
  , groupFormTemplate = require('../../../../templates/modals/group-property-form.jade')
  , optionsRowTemplate = require('../../../../templates/modals/entity-option.jade')
  , objectMapper = require('../../util/object-mapper')
  , _ = require ('lodash')

module.exports = function createGroupModal(workspace, tab) {

 var groupProperties = objectMapper.flatten(tab.group.levelEditorProperties)
    , otherGroups = _.reject(workspace.groups, { name: tab.group.name })
    , modalOptions =
    { title: 'Group Properties'
    , overlayClassName: 'inspector'
    , content: $(groupFormTemplate(
        { group: groupProperties
        , name: tab.group.name
        , otherGroups: otherGroups
        }
      ))
    , buttons: []
    }

  var modalControls = modal(modalOptions)
    , $modalElement = $('.inspector')
    , $form = $('.group-property-form')

  setupLockButton($modalElement, tab, workspace)
  setupSaveAndDeleteEvents($modalElement, $form, workspace, tab, modalControls)
  setupOrderButtonEvents($modalElement, workspace, tab)
  setupObjectPropertyEvents($modalElement, $form, tab)

  restoreGroupCollisionState($modalElement, tab.group)
  setupGroupCollisionEvents($modalElement)
}

function restoreGroupCollisionState($modal, group) {

  if (!group.levelEditorCollisionData.length) {
    return
  }

  _.each(group.levelEditorCollisionData, function(data) {

    var $itemForm = $('.collision-item.js-' + data.collidesWith)
      , $inputs = $itemForm.find('.collision-item-inputs')

    $itemForm.find('input[type=checkbox]').prop('checked', true)
    $inputs.find('input.' + data.type).prop('checked', true)

    $inputs.show()

    if (data.actionCallback) {
      $inputs.find('input[name=action-callback]').val(data.actionCallback)
    }

    if (data.processCallback) {
      $inputs.find('input[name=process-callback]').val(data.processCallback)
    }
  })
}

function setupGroupCollisionEvents($modal) {
  $modal.find('.checkbox input[type=checkbox]').change(function() {

    var $form = $(this).parents('.collision-item').find('.collision-item-inputs')
    $form.toggle()
    $form.find('.collide').attr('checked', 'checked')
  })
}

function setupLockButton($modal, tab, workspace) {
  $modal.find('.lock').click(function() {
    workspace.lockGroup(tab.group)
    showLockedStatus(tab.group, $modal.find('.lock'))
  })
  showLockedStatus(tab.group, $modal.find('.lock'))
}

function setupSaveAndDeleteEvents($modal, $form, workspace, tab, modalControls) {
  $modal.find('.save').click(function() {
    mapNameToGroup(tab, $form)
    objectMapper.map(tab.group, $form)
    setCollisionData(tab.group, $form)
  })

  $modal.find('.delete').click(function() {
    workspace.deleteGroup(tab.group.name)
    modalControls.close()
  })
}

function mapNameToGroup(tab, $form) {
  var name = $form.find('input[name=name]').val()
  tab.text.text = tab.group.name = name
}

function setCollisionData(group, $form) {
  group.levelEditorCollisionData = []

  $form.find('input[type=checkbox]:checked').each(function() {

    var $itemForm = $(this).parents('.collision-item').find('.collision-item-inputs')
      , actionCallback =  $itemForm.find('input[name=action-callback]').val()
      , processCallback =  $itemForm.find('input[name=process-callback]').val()
      , collisionData =
        { collidesWith: $(this).val()
        , type: $itemForm.find('input[type=radio]:checked').val()
        , actionCallback: actionCallback ? actionCallback : false
        , processCallback: processCallback ? processCallback : false
        }

    group.levelEditorCollisionData.push(collisionData)
  })
}

function showLockedStatus(group, $lockButton) {
  if (group.isLocked) {
    $lockButton.addClass('btn-warning')
    $lockButton.html('locked')
  } else {
    $lockButton.removeClass('btn-warning')
    $lockButton.html('lock')
  }
}

function setupObjectPropertyEvents($modal, $form, tab) {
  $modal.find('.add').click(function() {

    $form.find('input[name=key]').unbind('blur')

    $modal.find('.property-container').append(optionsRowTemplate())

    $form.find('input[name=key]').bind('blur', function(event) {
      objectMapper.getValueFromInput(event, tab.group)
    })
  })
}


function setupOrderButtonEvents($modal, workspace, tab) {
  $modal.find('.increase-order').click(function() {
    workspace.moveGroup(workspace, tab.group, true)
  })

  $modal.find('.decrease-order ').click(function() {
    workspace.moveGroup(workspace, tab.group, false)
  })

}
