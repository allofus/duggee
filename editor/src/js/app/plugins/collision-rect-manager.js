var Phaser = require('Phaser')
  , properties = require('editor-properties')
  , CollisionRectManager = null

  //instance vars
  , workspace = null

CollisionRectManager = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

CollisionRectManager.prototype = Object.create(Phaser.Plugin.prototype)
CollisionRectManager.prototype.constructor = CollisionRectManager
CollisionRectManager.prototype.group = null

CollisionRectManager.prototype.init = function ($workspace, $plugins) {
  workspace = $workspace
  workspace.events.collisionAreaCreated.add( addCollisionArea, this )
}

function addCollisionArea( collisionRect) {
  //workspace.collisionRects.push(  )
}

module.exports = CollisionRectManager
