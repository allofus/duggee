var Phaser = require('Phaser')
  , gameProperties = require('properties')
  , properties = require('editor-properties')
  , display = require('../../../../../src/js/lib/screen-properties')
  , AspectRatioOverlays = null

  //instance vars
    //resolution overlays
  , overlay_small = null
  , overlay_medium = null
  , overlay_large = null
  , overlay_xlarge = null
  , overlay_xxlarge = null

  , smallAspectRatioLabel = null
  , mediumAspectRatioLabel = null
  , largeAspectRatioLabel = null
  , xlargeAspectRatioLabel = null
  , xxlargeAspectRatioLabel = null


AspectRatioOverlays = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

AspectRatioOverlays.prototype = Object.create(Phaser.Plugin.prototype)
AspectRatioOverlays.prototype.constructor = AspectRatioOverlays
AspectRatioOverlays.prototype.group = null

AspectRatioOverlays.prototype.init = function (workspace) {
   createAspectRatioOverlays(this.game)
   createAspectRatioLabels(this.game)
   drawDeviceAspectRatioOverlays(this.game)
   updateVisibility()

   workspace.events.aspectRatioSettingChanged.add(updateVisibility)
}

function updateVisibility() {
  smallAspectRatioLabel.visible = properties.drawResolutionOutlines.small.visible
  mediumAspectRatioLabel.visible = properties.drawResolutionOutlines.medium.visible
  largeAspectRatioLabel.visible = properties.drawResolutionOutlines.large.visible
  xlargeAspectRatioLabel.visible = properties.drawResolutionOutlines.xlarge.visible
  xxlargeAspectRatioLabel.visible = properties.drawResolutionOutlines.xxlarge.visible

  overlay_small.visible = properties.drawResolutionOutlines.small.visible
  overlay_medium.visible = properties.drawResolutionOutlines.medium.visible
  overlay_large.visible = properties.drawResolutionOutlines.large.visible
  overlay_xlarge.visible = properties.drawResolutionOutlines.xlarge.visible
  overlay_xxlarge.visible = properties.drawResolutionOutlines.xxlarge.visible
}

function createAspectRatioLabels(game) {
  var style = { font: '13px Arial', fill: 'white', align: 'left' }

  smallAspectRatioLabel = game.add.text( (game.width/2 ) - (properties.drawResolutionOutlines.small.x/2), (game.height/2 )-(properties.drawResolutionOutlines.small.y/2), 'Small 320x240', style)
  smallAspectRatioLabel.y -= smallAspectRatioLabel.height
  smallAspectRatioLabel.fixedToCamera = true

  mediumAspectRatioLabel = game.add.text( (game.width/2 ) - (properties.drawResolutionOutlines.medium.x/2), (game.height/2 )-(properties.drawResolutionOutlines.medium.y/2), 'Normal 480x320', style)
  mediumAspectRatioLabel.y -= mediumAspectRatioLabel.height
  mediumAspectRatioLabel.fixedToCamera = true

  largeAspectRatioLabel = game.add.text( (game.width/2 ) - (properties.drawResolutionOutlines.large.x/2), (game.height/2 )-(properties.drawResolutionOutlines.large.y/2), 'Large 720x480', style)
  largeAspectRatioLabel.y -= largeAspectRatioLabel.height
  largeAspectRatioLabel.fixedToCamera = true

  xlargeAspectRatioLabel = game.add.text( (game.width/2 ) - (properties.drawResolutionOutlines.xlarge.x/2), (game.height/2 )-(properties.drawResolutionOutlines.xlarge.y/2), 'X Large 960x640', style)
  xlargeAspectRatioLabel.y -= xlargeAspectRatioLabel.height
  xlargeAspectRatioLabel.fixedToCamera = true

  xxlargeAspectRatioLabel = game.add.text( (game.width/2 ) - (properties.drawResolutionOutlines.xxlarge.x/2), (game.height/2 )-(properties.drawResolutionOutlines.xxlarge.y/2), 'XX Large 1440x960', style)
  xxlargeAspectRatioLabel.y -= xxlargeAspectRatioLabel.height
  xxlargeAspectRatioLabel.fixedToCamera = true
}

function createAspectRatioOverlays(game) {
  overlay_small = game.add.graphics(0,0)
  overlay_small.lineStyle(.4, 0x6CE8C9, .3);
  overlay_small.fixedToCamera = true

  overlay_medium = game.add.graphics(0,0)
  overlay_medium.lineStyle(.4, 0x6CE8C9, .3);
  overlay_medium.fixedToCamera = true

  overlay_large = game.add.graphics(0,0)
  overlay_large.lineStyle(.4, 0x6CE8C9, .3);
  overlay_large.fixedToCamera = true

  overlay_xlarge = game.add.graphics(0,0)
  overlay_xlarge.lineStyle(.4, 0x6CE8C9, 3);
  overlay_xlarge.fixedToCamera = true

  overlay_xxlarge = game.add.graphics(0,0)
  overlay_xxlarge.lineStyle(.4, 0x6CE8C9, .3);
  overlay_xxlarge.fixedToCamera = true
}

function drawDeviceAspectRatioOverlays(game) {
  //small size
  if (properties.drawResolutionOutlines.small.visible) {
    overlay_small.clear()
    overlay_small.lineStyle(.4, 0x6CE8C9, 1);
    overlay_small.drawRect(game.width/2-properties.drawResolutionOutlines.small.x/2, game.height/2-properties.drawResolutionOutlines.small.y/2, properties.drawResolutionOutlines.small.x, properties.drawResolutionOutlines.small.y)
  }

  //normal size
  if (properties.drawResolutionOutlines.medium.visible) {
    overlay_medium.clear()
    overlay_medium.lineStyle(.4, 0x6CE8C9, 1);
    overlay_medium.drawRect(game.width/2-properties.drawResolutionOutlines.medium.x/2, game.height/2-properties.drawResolutionOutlines.medium.y/2, properties.drawResolutionOutlines.medium.x, properties.drawResolutionOutlines.medium.y)
  }
  //large size
  if (properties.drawResolutionOutlines.large.visible) {
    overlay_large.clear()
    overlay_large.lineStyle(.4, 0x6CE8C9, 1);
    overlay_large.drawRect(game.width/2-properties.drawResolutionOutlines.large.x/2, game.height/2-properties.drawResolutionOutlines.large.y/2, properties.drawResolutionOutlines.large.x, properties.drawResolutionOutlines.large.y)
  }

  //Xlarge size
  if (properties.drawResolutionOutlines.xlarge.visible) {
    overlay_xlarge.clear()
    overlay_xlarge.lineStyle(.4, 0x6CE8C9, 1);
    overlay_xlarge.drawRect(game.width/2-properties.drawResolutionOutlines.xlarge.x/2, game.height/2-properties.drawResolutionOutlines.xlarge.y/2, properties.drawResolutionOutlines.xlarge.x, properties.drawResolutionOutlines.xlarge.y)
  }

  //XXlarge size
  if (properties.drawResolutionOutlines.xxlarge.visible) {
    overlay_xxlarge.clear()
    overlay_xxlarge.lineStyle(.4, 0x6CE8C9, 1)
    overlay_xxlarge.drawRect(game.width/2-properties.drawResolutionOutlines.xxlarge.x/2, game.height/2-properties.drawResolutionOutlines.xxlarge.y/2, properties.drawResolutionOutlines.xxlarge.x, properties.drawResolutionOutlines.xxlarge.y)
  }
}

module.exports = AspectRatioOverlays
