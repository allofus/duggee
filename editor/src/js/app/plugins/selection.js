var Phaser = require('Phaser')
  , properties = require('editor-properties')
  , Selection = null
  , _ = require('lodash')
  , MathUtils = require('MathUtils')
  , entityInspector = require('../util/modals/entity-inspector')

  // Instance variables.
  , clipBoard = []
  , selectedEntity = null
  , selectedEntityList = []
  , lastSelectedEntitiyPos = { x: 0, y: 0 }
  , workspace = null
  , entityBuilder
  , mouseIsDown = false
  , selectionStartCtr = 0
  , draggingSelection = null

Selection = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)

  game.input.mouse.mouseDownCallback = _.bind(handleMouseDown, this)
  game.input.mouse.mouseUpCallback = _.bind(handleMouseUp, this)
  game.input.keyboard.onDownCallback = _.bind(handleKeyDown, this)

  game.input.keyboard.addKeyCapture(
    [ Phaser.Keyboard.SHIFT
    , Phaser.Keyboard.SPACEBAR
    ]
  )
}

Selection.prototype = Object.create(Phaser.Plugin.prototype)
Selection.prototype.constructor = Selection

Selection.prototype.init = function ($workspace, $entityBuilder) {
  workspace = $workspace
  entityBuilder = $entityBuilder

  workspace.events.levelCleared.add(this.clearSelectedEntities)
  workspace.events.groupRemoved.add(this.clearSelectedEntities)
  workspace.events.activeGroupChanged.add(handleGroupChange, this)
}

function handleGroupChange(viaEntityClick) {
  if (!viaEntityClick) {
    this.clearSelectedEntities()
    this.selectAll(workspace.getActiveGroup())
  }
}

Selection.prototype.selectAll = function(group) {
  group.forEach(addEntityToSelectedEntitiesList, this)
}
Selection.prototype.beginDragBoxPoint = {x:0, y:0}
Selection.prototype.isDraggingSelectionBox = false

Selection.prototype.getSelectedEntity = function() {
  return selectedEntity
}

Selection.prototype.update = function () {

  //dont do anything if the user is entering text into a text field.
  if ($('input:focus').length >= 1) return

  if (workspace.draggingEntity && !this.game.input.keyboard.isDown(Phaser.Keyboard.SHIFT) && !this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
    workspace.draggingEntity.x = ((this.game.input.mousePointer.x - workspace.draggingEntity.mouseOffset.x  ) + this.game.camera.x) * (1-(workspace.draggingEntity.group.scale.x -1))
    workspace.draggingEntity.y = ((this.game.input.mousePointer.y - workspace.draggingEntity.mouseOffset.y  ) + this.game.camera.y) * (1-(workspace.draggingEntity.group.scale.y -1))

    if (!this.game.input.keyboard.isDown(Phaser.Keyboard.ALT)) {
      snapEntityToGrid(workspace.draggingEntity)
    }

    var diff = {x:0, y:0}

    diff.x = workspace.draggingEntity.x - lastSelectedEntitiyPos.x
    diff.y = workspace.draggingEntity.y - lastSelectedEntitiyPos.y

    if (diff.x !== 0 || diff.y !== 0) {
      _.each(selectedEntityList, function(entity) {
        if (workspace.draggingEntity.id !== entity.id) {
          entity.x += diff.x
          entity.y += diff.y
        }
      })

      draggingSelection = true
    }

    lastSelectedEntitiyPos.x = workspace.draggingEntity.x
    lastSelectedEntitiyPos.y = workspace.draggingEntity.y
  } else {
    draggingSelection = false
  }

  if (mouseIsDown && !this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && draggingSelection === false && selectedEntity === null) {
    selectionStartCtr += this.game.time.physicsElapsed
    if (selectionStartCtr >= .1) {
      this.isDraggingSelectionBox = true
    }
  } else {
    selectionStartCtr = 0
  }
}

function snapEntityToGrid( entity) {

 if (entity.body) {
  entity.x = Math.round(entity.x/properties.grid.x) * (properties.grid.x) - entity.body.offset.x + properties.grid.offsetX
  entity.y = Math.round(entity.y/properties.grid.y) * (properties.grid.y) - entity.body.offset.y + properties.grid.offsetY
 } else {
  entity.x = Math.round(entity.x/properties.grid.x) * (properties.grid.x)// + properties.grid.offsetX
  entity.y = Math.round(entity.y/properties.grid.y) * (properties.grid.y)// + properties.grid.offsetY
 }
}

Selection.prototype.render = function() {
  drawAllSelectedEntitiesHighlightRect()
}

Selection.prototype.selectEntity = function(entity) {

  if (entity.isLocked || this.game.input.mousePointer.y > 879) {
    return
  }

  if (selectedEntity && !this.game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) {
    this.clearSelectedEntities()
  }

  if (workspace.getActiveGroup().name !== entity.group.name) {
    workspace.setActiveGroupViaEntityClick(entity.group)
  }

  addEntityToSelectedEntitiesList(entity)

  if (selectedEntityList.length === 1) {
    entityInspector(entity, snapGridToEntity)
  }

  selectedEntity = entity
  entity.selected = true

  lastSelectedEntitiyPos.x = selectedEntity.x
  lastSelectedEntitiyPos.y = selectedEntity.y

  //setup workspace dragging.
  workspace.draggingEntity = selectedEntity

  if (selectedEntity.body) {
    selectedEntity.mouseOffset.x = this.game.input.mousePointer.x-(selectedEntity.x-this.game.camera.x)-selectedEntity.body.offset.x
    selectedEntity.mouseOffset.y = this.game.input.mousePointer.y-(selectedEntity.y-this.game.camera.y)-selectedEntity.body.offset.y
  } else {
    selectedEntity.mouseOffset.x = this.game.input.mousePointer.x-(selectedEntity.x-this.game.camera.x)
    selectedEntity.mouseOffset.y = this.game.input.mousePointer.y-(selectedEntity.y-this.game.camera.y)
  }

  workspace.events.selectedEntityChanged.dispatch(selectedEntity)
}

Selection.prototype.deSelectEntity = function(entity) {
  selectedEntity = null
  entity.selected = false
  hideOpenOptionOverlays()
}

Selection.prototype.clearSelectedEntities = function() {

  _.each(selectedEntityList, function(entity) {
    entity.highlightGraphics.clear()
    entity.selected = false
  })

  workspace.draggingEntity = null

  selectedEntityList = []
  selectedEntity = null
  hideOpenOptionOverlays()
}

Selection.prototype.setupEntityForSelection = function(entity) {

  this.isDraggingSelectionBox = false
  entity.highlightGraphics = this.game.add.graphics(0, 0)

  entity.drawHighlightRect = function(alpha) {

    if (entity.isLocked || !isEntityInWorkspaceRect(entity, this.game)) {
      return
    }

    entity.highlightGraphics.clear()
    entity.highlightGraphics.lineStyle(1, 0x00FF00, alpha)

    if (entity.body) {
      entity.highlightGraphics.drawRect(entity.x + entity.body.offset.x, entity.y + entity.body.offset.y, entity.body.width, entity.body.height)
    } else {
      entity.highlightGraphics.drawRect(entity.x, entity.y, entity.width, entity.height)
    }
  }

  entity.justAddedToWorkspace = true
  entity.events.onInputDown.add(this.selectEntity, this, entity)
  //entity.events.onInputUp.add(this.deSelectEntity, this, entity)

  entity.events.onInputOver.add(function() {
    if (_.findIndex(selectedEntityList, { 'id': entity.id }, this) === -1 && isEntityInWorkspaceRect(entity, this.game)) {
      entity.drawHighlightRect(0.5)
    }
  }, this)

  entity.events.onInputOut.add(function() {
    if (!entity.selected && isEntityInWorkspaceRect(entity, this.game)) {
      entity.highlightGraphics.clear()
    }
  }, this)
}

function drawAllSelectedEntitiesHighlightRect() {
  _.each(selectedEntityList, function(entity) {
    if (!entity.destroyPhase) {
     entity.drawHighlightRect(1)
    }
  })
}

function addEntityToSelectedEntitiesList(en) {
  //check item isnt already selected...

  if (en.isLocked) {
    return
  }

  var found = false;

  _.each(selectedEntityList, function(entity) {
    if (entity.id === en.id) {
      found = true
      return
    }
  })

  if (found) {
    return
  }

  selectedEntityList.push(en)

  en.selected = true
  en.drawHighlightRect(1)
  hideOpenOptionOverlays()
}

function hideOpenOptionOverlays() {
  $('.inspector').remove()
}

function handleKeyDown(e) {

  //do not do anything if typing
  if ($('input:focus').length >= 1) {
    return
  }
  //check to see if we need to delete entities
  if (this.game.input.keyboard.isDown(Phaser.Keyboard.BACKSPACE) || this.game.input.keyboard.isDown(Phaser.Keyboard.DELETE)) {
    deleteAllSelectedEntities(e)
  }

  //if the user presses shift + G, then set the current grid size to that of the selected entity.
  if (this.game.input.keyboard.isDown(Phaser.Keyboard.G) && this.game.input.keyboard.isDown(Phaser.Keyboard.CONTROL) && selectedEntity) {
    snapGridToEntity(selectedEntity)
    e.preventDefault()
  }

  if (this.game.input.keyboard.isDown(Phaser.Keyboard.CONTROL)) {
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.C)) {
      //clone the array to your clipboard
      clipBoard = selectedEntityList.slice(0)

      e.preventDefault()

    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.V)) {

      this.clearSelectedEntities()
        //paste items....
      var topLeftPos = {x:Infinity, y:Infinity}

      _.each(clipBoard, function(entity) {
        if (entity.x < topLeftPos.x) {
          topLeftPos.x = entity.x
        }

        if (entity.y < topLeftPos.y) {
          topLeftPos.y = entity.y
        }
      })

      _.each(clipBoard, function(entity) {
        var en = entityBuilder(entity, (entity.x + this.game.input.mousePointer.x + this.game.camera.x) -topLeftPos.x,(entity.y + this.game.input.mousePointer.y + this.game.camera.y) - topLeftPos.y, null, false, true)
        this.setupEntityForSelection(en)
        addEntityToSelectedEntitiesList(en)
        workspace.entities.push(en)
      }, this)

      e.preventDefault()

    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
      this.selectAll(workspace.getActiveGroup())
      e.preventDefault()
    }
  }
}

function snapGridToEntity(entity) {
  properties.grid.x = entity.body.width
  properties.grid.y = entity.body.height
  properties.grid.offsetX = entity.body.offset.x
  properties.grid.offsetY = entity.body.offset.y

  workspace.events.gridChanged.dispatch(entity)
  snapEntityToGrid( entity )
}

function deleteAllSelectedEntities(e) {

  if (e) {
    e.preventDefault()
  }

  for(var i = selectedEntityList.length-1; i >= 0; i--) {
    for(var j = workspace.entities.length-1; j >=0; j--) {
      if (selectedEntityList[i].id === workspace.entities[j].id) {
        workspace.entities.splice(j,1)
      }
    }

    //clear collision areas.
    if (selectedEntityList[i].collisionGraphics) {
      selectedEntityList[i].collisionGraphics.destroy()
      selectedEntityList[i].collisionGraphics = null
    }

    selectedEntityList[i].destroy()
    selectedEntityList[i].highlightGraphics.destroy()
    selectedEntityList[i].selected = false
    selectedEntityList.splice(i,1)
  }

  hideOpenOptionOverlays()
}

function removeEntityFromWorkspace(en) {

  if (en.isLocked) {
    return
  }

  for(var i = selectedEntityList.length-1; i >= 0; i--) {
    if (selectedEntityList[i].levelEntityId === en.levelEntityId) {
      selectedEntityList.splice(i,1)
      break;
    }
  }

  for(var j = workspace.entities.length-1; j >=0; j--) {
    if (workspace.entities[j].levelEntityId === en.levelEntityId) {
      workspace.entities.splice(j,1)
      break;
    }
  }

  en.destroy()
  en.highlightGraphics.clear()
  en.selected = false
}

function handleMouseUp(e) {
  ///check if we are clicking on a model.
  if ($('.inspector').position() && MathUtils.isPointInsideRect(e.x, e.y, $('.inspector').position().left, $('.inspector').position().top, $('.inspector').width(), $('.inspector').height() )) return

  if (workspace.draggingEntity && !isEntityInWorkspaceRect(workspace.draggingEntity, this.game)) {
    removeEntityFromWorkspace(workspace.draggingEntity)
    workspace.draggingEntity = null
  }

  if (this.isDraggingSelectionBox) {
    selectEntitiesWithinSelectionBox(this.game, this.beginDragBoxPoint)
  }

  if (!this.game.input.keyboard.isDown(Phaser.Keyboard.SHIFT) && !this.game.input.keyboard.isDown(Phaser.Keyboard.CONTROL)) {
    workspace.draggingEntity = null
  }

  //91 == CMD on mac
  if (this.game.input.keyboard.isDown(Phaser.Keyboard.CONTROL) || this.game.input.keyboard.isDown(91)) {
    this.addCollisionRectangle()
  } else {
    //check that we are not on an entity
    var overEn = false
    var en
    _.each(workspace.entities, function(entity) {
      if (MathUtils.isPointInsideRect(this.game.input.mousePointer.x + this.game.camera.x ,this.game.input.mousePointer.y + this.game.camera.y ,entity.x * entity.group.scale.x,entity.y * entity.group.scale.y,entity.width * entity.group.scale.x,entity.height * entity.group.scale.x)) {
        overEn = true
        en = entity
      }
    }, this)

    if (overEn === false && this.isDraggingSelectionBox === false && this.game.input.mousePointer.y >= 140) {
      this.clearSelectedEntities()
    }

    if (overEn && en.justAddedToWorkspace) {
      en.justAddedToWorkspace = false
      this.clearSelectedEntities()
    }
  }

  //stop
  this.isDraggingSelectionBox = false
  mouseIsDown = false
  selectionStartCtr = 0
}

Selection.prototype.addCollisionRectangle = function() {
  //create new hit area the size of the drag rectangle.
  var dragRect = getDragRectangle(this.game, this.beginDragBoxPoint)

  //add mouse offset
  dragRect.x += this.game.camera.x
  dragRect.y += this.game.camera.y

  var collisionGroup = workspace.getGroupByName('collisions')
  var newEn = entityBuilder(dragRect, 0,0, collisionGroup, true, true)
  this.setupEntityForSelection(newEn)
  addEntityToSelectedEntitiesList(newEn)

  collisionGroup.add(newEn)
  workspace.entities.push( newEn )
}

function isEntityInWorkspaceRect(entity, game) {
  return entity.y + entity.height < game.camera.y + game.height - properties.toolbox.maxItemSize.y - properties.toolbox.padding.y && entity.y > game.camera.y + 50
}

function handleMouseDown() {

  if (this.game.input.mousePointer.y >= this.game.height - properties.toolbox.maxItemSize.y - properties.toolbox.padding.y || this.game.input.mousePointer.y < 50) return

  this.beginDragBoxPoint.x = this.game.input.mousePointer.x
  this.beginDragBoxPoint.y = this.game.input.mousePointer.y

  mouseIsDown = true
}

function getDragRectangle(game, dragPoint) {

  var w = game.input.mousePointer.x - dragPoint.x
  var h = game.input.mousePointer.y - dragPoint.y

  if (w < 0 ) w *= -1
  if (h < 0 ) h *= -1

  var startX = 0
    , endY = 0

  dragPoint.x < game.input.mousePointer.x ? startX = dragPoint.x : startX = game.input.mousePointer.x
  dragPoint.y < game.input.mousePointer.y ? endY = dragPoint.y : endY = game.input.mousePointer.y

  return new Phaser.Rectangle(startX, endY,w,h)
}

function selectEntitiesWithinSelectionBox(game, dragPoint) {

  var dragRect = getDragRectangle(game, dragPoint)

  if (!game.input.keyboard.isDown(Phaser.Keyboard.CONTROL)) {
    _.each(workspace.entities, function(entity) {
      if (MathUtils.doesRectOverlap(entity.x-game.camera.x, entity.y-game.camera.y, entity.width, entity.height,
                                   dragRect.x, dragRect.y,dragRect.width,dragRect.height)) {
        addEntityToSelectedEntitiesList(entity)
      }
    }, this)
  }

  if (selectedEntityList.length === 1) {
    entityInspector(selectedEntityList[0], snapGridToEntity)
  }
}

module.exports = Selection
