var Phaser = require('Phaser')
  , properties = require('editor-properties')
  , Grid = null

  //instance vars
  , gridGraphics = null
  , selectedEntity = null

Grid = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

Grid.prototype = Object.create(Phaser.Plugin.prototype)
Grid.prototype.constructor = Grid
Grid.prototype.group = null

Grid.prototype.init = function (workspace) {
  gridGraphics = this.game.add.graphics(0,0)

  drawGroupGridSize(this.game)

  workspace.events.gridChanged.add( handleGridChange, this )
}

Grid.prototype.update = function () {
  drawGroupGridSize(this.game)
}

function handleGridChange(entity) {
  selectedEntity = entity
}

function drawGroupGridSize(game) {
  gridGraphics.clear()
  gridGraphics.lineStyle(.4, 0xFFFFFF, .5);

  var startPos = {x:properties.grid.offsetX, y: properties.grid.offsetY }

  for(var i = startPos.x; i < game.camera.x + game.width; i += properties.grid.x) {
    gridGraphics.moveTo(i,0 )
    gridGraphics.lineTo(i,game.camera.y + game.height);
  }

  for(i = startPos.y; i <game.camera.y + game.height; i += properties.grid.y) {
    gridGraphics.moveTo(0 ,i )
    gridGraphics.lineTo(game.camera.x + game.width,i);
  }
}

module.exports = Grid
