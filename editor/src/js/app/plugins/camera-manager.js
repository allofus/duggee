var Phaser = require('Phaser')
  , CameraManager = null
  , workspace = null
  , properties = require('editor-properties')

  , cursors = null
  , plugins = null
  , mouseCamera = null

CameraManager = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

CameraManager.prototype = Object.create(Phaser.Plugin.prototype)
CameraManager.prototype.constructor = CameraManager

CameraManager.prototype.init = function (_workspace, _plugins) {
  cursors = this.game.input.keyboard.createCursorKeys()
  workspace = _workspace
  plugins = _plugins

  //mouse world/camera movement
  mouseCamera = null
}

CameraManager.prototype.update = function () {
  checkKeyMovement(this.game)

    //drag the camera using the mouse
  if (!workspace.draggingEntity && this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
    document.body.style.cursor = 'pointer'
    moveCameraByPointer(this.game.input.mousePointer, this.game)
    moveCameraByPointer(this.game.input.pointer1, this.game)
  } else {
    document.body.style.cursor = 'default'
  }
}

CameraManager.prototype.resetCamera = function() {
  this.game.camera.x = 0
  this.game.camera.y = 0
}

CameraManager.prototype.render = function() {
  moveCamera(this.game)
}

function moveCamera(game) {
  if (mouseCamera) {
    //scroller.visible = false
   plugins.entityPalette.hideEntities()
  } else {

   // scroller.visible = true
   plugins.entityPalette.showEntities()
  }
}

//http://www.html5gamedevs.com/topic/2410-drag-the-camera/
//http://jsfiddle.net/YHj24/21/
function moveCameraByPointer(pointer, game) {
  if (!pointer.timeDown) { return }
  if (pointer.isDown && !pointer.targetObject) {
      if (mouseCamera) {
          game.camera.x += (mouseCamera.x - pointer.position.x)
          game.camera.y += (mouseCamera.y - pointer.position.y)
         // game.camera.x = Math.round(game.camera.x/properties.grid.x) * properties.grid.x
       //   game.camera.y = Math.round(game.camera.y/properties.grid.y) * properties.grid.y
      }
      mouseCamera = pointer.position.clone()
      //mouseCamera.x = Math.round(mouseCamera.x/properties.grid.x) * properties.grid.x
      //mouseCamera.y = Math.round(mouseCamera.y/properties.grid.y) * properties.grid.y
  }
  if (pointer.isUp) {
   mouseCamera = null
  }
}

function checkKeyMovement(game) {
  if (cursors.left.isDown) {
    game.camera.x -= 1
  } else if (cursors.right.isDown) {
    game.camera.x += 1
  }

  if (cursors.up.isDown) {
    game.camera.y -= 1
  } else if (cursors.down.isDown) {
    game.camera.y += 1
  }
}

module.exports = CameraManager
