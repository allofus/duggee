var Phaser = require('Phaser')
  , UImanager = {}
  , _ = require('lodash')
  , createButtonBuilder = require('../util/button-builder')
  , persistanceService = require('../util/persistance-service')
  , confirmAction = require('../util/modals/confirm-action')
  , instructions = require('../util/modals/instructions')

  // Instance variables.
  , scrollStep = 0.05
  , gameScale = 1
  , levelNameText = null
  , mousePosText = null
  , plugins = null
  , workspace = null
  , game = null

UImanager = function ($game, parent) {
  Phaser.Plugin.call(this, $game, parent)

  game = $game
}

UImanager.prototype = Object.create(Phaser.Plugin.prototype)
UImanager.prototype.constructor = UImanager
UImanager.prototype.group = null

UImanager.prototype.init = function ($workspace, $plugins) {

  plugins = $plugins
  workspace = $workspace

  addListeners()

  //draw background
  var bg = this.game.add.graphics(0, 0);
  bg.fixedToCamera = true
  bg.beginFill(0x161616, .8);
  bg.drawRect(0,0, this.game.width,20)

  addText(this.game)
  addUIButtons(this.game)
  this.group = this.game.add.group()
}

UImanager.prototype.update = function() {
  mousePosText.x = this.game.camera.x + this.game.input.mousePointer.x + 20
  mousePosText.y = this.game.camera.y + this.game.input.mousePointer.y
  mousePosText.text = 'x:' + Math.round(this.game.camera.x + this.game.input.mousePointer.x)  + 'px \ny:' + Math.round(this.game.camera.y + this.game.input.mousePointer.y) +'px'

}

function setLevelName (levelName) {
  levelNameText.text = levelName
}

function addUIButtons (game) {
  var buttonBuilder = createButtonBuilder(game, 0, 3)

  buttonBuilder('save.png', persistanceService.save, workspace, 'editorAtlas', 'save-hover.png')
  buttonBuilder('save-as.png', persistanceService.saveAs, workspace, 'editorAtlas', 'save-as-hover.png')
  buttonBuilder('load.png', persistanceService.loadLevel , null, 'editorAtlas', 'load-hover.png')
  buttonBuilder('clear.png', confirmAction, workspace.clear, 'editorAtlas', 'clear-hover.png')

  buttonBuilder = createButtonBuilder(game, 10, plugins.entityPalette.getScrollerPosition().y -90)
  buttonBuilder('instructions.png', showInstructions, null, 'editorAtlas', 'instructions-hover.png')
}

function showInstructions() {
  closeInstructions()
  instructions(workspace, game)
}

function closeInstructions() {
  $('.instructions').remove()
}

function addText (game) {
  var style = { font: '20px Arial', fill: 'white', align: 'left' }
  levelNameText = game.add.text(game.width/2, -2, workspace.levelName, style)
  levelNameText.x -= levelNameText.width/2
  levelNameText.fixedToCamera = true

  style = { font: '11px Arial', fill: 'white', align: 'left' }
  mousePosText = game.add.text(0,0, workspace.levelName, style)
}

function addListeners () {
  var element = document.getElementById('game')
  //http://www.sitepoint.com/html5-javascript-mouse-wheel/
  // IE9, Chrome, Safari, Opera
  element.addEventListener('mousewheel', _.bind(MouseWheelHandler, this), false)
  // Firefox
  element.addEventListener('DOMMouseScroll', _.bind(MouseWheelHandler, this), false)

  workspace.events.levelNameChanged.add(setLevelName)
}

function MouseWheelHandler (e) {

  return
    // cross-browser wheel delta
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))

    e.preventDefault()

    gameScale += delta * scrollStep

    if (gameScale < .1) {
      gameScale = .1
    }

    if (gameScale > 4) {
      gameScale = 4
    }

    _.each(workspace.groups, function(group) {
    group.scale.x = gameScale
    group.scale.y = gameScale
    }, this)

    return false
}

module.exports = UImanager
