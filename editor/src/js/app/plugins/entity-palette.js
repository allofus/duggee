var Phaser = require('Phaser')
  , properties = require('editor-properties')
  , Palette = null
  , _ = require('lodash')
  , entityBuilder = require('../util/entity-builder')

  // Instance variables.
  , paletteWidth = 0
  , scroller = null
  , plugins = null
  , entityBuilder = null
  , workspace = null

Palette = function ( game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

function getMaxFitRatio( entity, maxWidth, game) {
  var max = properties.toolbox.maxItemSize

  if (maxWidth < game.width - 100) {
   max = { x: 60, y: 60 }
  }

  var calculatedScale = Math.min(max.x / entity.width, max.y / entity.height)

  if (entity.width < max.x || entity.height < max.y || calculatedScale > entity.scale.x || calculatedScale > entity.scale.y) {
    if (calculatedScale > entity.scale.y) {
      return  entity.scale.x
    } else if (calculatedScale > entity.scale.x) {
      return  entity.scale.y
    }
  }

  return calculatedScale
}

Palette.prototype = Object.create(Phaser.Plugin.prototype)
Palette.prototype.constructor = Palette

Palette.prototype.init = function ( $workspace, modules, $plugins, $entityBuilder) {

  workspace = $workspace
  plugins = $plugins
  entityBuilder = $entityBuilder

  workspace.events.inputPriorityReset.add(function() {
    workspace.setGroupInputPriority(this.entities)
  }, this)

  //draw background
  var bg = this.game.add.graphics(0, 0);
  bg.beginFill(0x161616, .8);
  bg.fixedToCamera = true
  bg.drawRect(0, this.game.height -80, this.game.width, 90)

  this.entities = this.game.add.group()
  this.build(modules)

  if (this.getWidth() > this.game.width - 100) {
    createScroller(this.game)
  }

  workspace.events.activeGroupChanged.add(handleActiveGroupChanged, this)
}

function handleActiveGroupChanged(groupName) {

  if (groupName === 'collisions') {
    this.entities.visible = false
  } else {
    this.entities.visible = true
  }

}

function createScroller(game) {
  // TODO: needs to be refactored into entity-palette, but has ties in camera logic.
  scroller = game.add.sprite(0, game.height - 29, 'editorAtlas')
  scroller.animations.add('idle',['scroller.png'])
  scroller.animations.add('hover',['scroller-hover.png'])
  scroller.animations.add('selected',['scroller-selected.png'])
  scroller.animations.play('idle')

  scroller.fixedToCamera = true
  scroller.inputEnabled = true
  scroller.input.useHandCursor = true

  scroller.events.onDragStart.add(function(sprite) {
    sprite.animations.play('selected')
  })

  scroller.events.onDragStop.add(function(sprite) {
    sprite.animations.play('hover')
  })

  scroller.events.onInputOver.add(function(sprite) {
    sprite.animations.play('hover')
  })

  scroller.events.onInputOut.add(function(sprite) {
    sprite.animations.play('idle')
  })
  //lockCenter, bringToTop, pixelPerfect, alphaThreshold, boundsRect, boundsSprite
  scroller.input.enableDrag(false, false, false,255,new Phaser.Rectangle(0,game.height - scroller.height,game.width,scroller.height))
  scroller.input.setDragLock(true, false)
}

Palette.prototype.showEntities = function() {

  this.entities.forEach(function(en) {
    en.visible = true
    en.x = Math.floor((this.game.camera.x+en.startPos.x)-((this.getScrollerPosition().x-this.game.camera.x) / (this.game.width-142 ) * (this.getWidth() - this.game.width )))
    en.y = this.game.camera.y+en.startPos.y
  }, this)
}

Palette.prototype.hideEntities = function() {
  plugins.entityPalette.entities.forEach(function(en) {
      en.visible = false
  }, this)
}

Palette.prototype.addEntityToWorkspace = function (entity) {
  //bounce out if the user is using the slider.
  if (scroller && this.game.input.mousePointer.y >= this.getScrollerPosition().y - this.game.camera.y) return
  if (!scroller && this.game.input.mousePointer.y >= this.getScrollerPosition().y) return
  //add a new item to the level.
  var newEntity = entityBuilder(entity, entity.x, entity.y, workspace.getActiveGroup(), null ,true)

  plugins.selection.clearSelectedEntities()
  plugins.selection.setupEntityForSelection(newEntity)
  plugins.selection.selectEntity(newEntity)

  workspace.draggingEntity = newEntity
  workspace.entities.push(newEntity)

  //reset the dragging entities position.
  entity.x = entity.editorStartXPos
  entity.y = entity.editorStartYPos

  return entity
}

Palette.prototype.getScrollerPosition = function() {

  if (!scroller) {
    return {x:this.game.camera.x, y:this.game.height}
  }

  return { x: scroller.x, y: scroller.y }
}

Palette.prototype.build = function (modules) {

  var xPos = properties.toolbox.padding.x
    , yPos = this.game.height - properties.toolbox.maxItemSize.y - properties.toolbox.padding.y

  _.each(modules, function(module, entityName) {

    if (entityName !== 'collision_area') {

      var entity = entityBuilder({ levelEntityId: entityName }, xPos, yPos, this.entities, false, true)

      entity.editorStartXPos = xPos
      entity.editorStartYPos = yPos
      entity.isEditorItem = true

      var startPos = { x: xPos, y: yPos }
      entity.startPos = startPos

      entity.scale.setTo(getMaxFitRatio(entity, this.getWidth(), this.game))
      entity.anchor.setTo(0, 0)

      // add a new entity on mouse down
      entity.events.onInputDown.add(this.addEntityToWorkspace, this, entity)

      xPos += entity.width + properties.toolbox.padding.x
      paletteWidth = xPos
    }
  }, this)
}

Palette.prototype.entities = null

Palette.prototype.getWidth = function () {
  return paletteWidth
}



module.exports = Palette
