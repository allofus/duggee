var Phaser = require('Phaser')
  , gameProperties = require('properties')
  , LevelLogicOverlay = null
  , properties = require('editor-properties')
  //instance vars
  , graphic = null

LevelLogicOverlay = function (game, parent) {
  Phaser.Plugin.call(this, game, parent)
}

LevelLogicOverlay.prototype = Object.create(Phaser.Plugin.prototype)
LevelLogicOverlay.prototype.constructor = LevelLogicOverlay
LevelLogicOverlay.prototype.group = null

LevelLogicOverlay.prototype.init = function (workspace) {
  graphic = this.game.add.graphics(0,0)
  graphic.fixedToCamera = true
  this.drawLevelLogicSize(this.game.x, this.game.y)

  workspace.events.gameLogicSettingChanged.add(this.drawLevelLogicSize, this)
}

LevelLogicOverlay.prototype.drawLevelLogicSize = function() {

  var pos = {x:0,y:0}

  this.game.x === undefined ? pos.x = 0 : pos.x = this.game.x
  this.game.y === undefined ? pos.y = 0 : pos.y = this.game.y

  graphic.clear()
  if (properties.showGameLogicSize) {
    graphic.lineStyle(2, 0xFFFFFF, .5);
    graphic.drawRect( pos.x,  pos.y, gameProperties.logicWidth, gameProperties.logicHeight);
  }
}


module.exports = LevelLogicOverlay
