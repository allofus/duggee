var express = require('express')
  , bodyParser = require('body-parser')
  , app = express()
  , path = require('path')
  , fs = require('fs')
  , levelPath = path.resolve(__dirname + '/../../src/js/game/levels/')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/../../build/index.html'))
})

app.use('/editor', express.static(path.join(__dirname, '/../build/')))

app.get('/editor', function(req, res) {
  res.sendFile(path.join(__dirname, '/../build/index.html'))
})

app.post('/editor/saveding', function(req, res) {

  var result = JSON.stringify(req.body.levelData)
    , levelName = req.body.levelData.metaData.levelName

  fs.writeFile(path.join(levelPath, levelName + '.json'), result, function(err) {
    if (err) {
      console.error(err)
      res.status(500).json(err)
    } else {
      res.status(200).end()
    }
  })
})

module.exports = app
