var fs = require('fs')
  , path = require('path')
  , _ = require('lodash')
  , exludePatterns = require('../src/js/app/properties').excludeFilePatterns

module.exports = function (modulePath) {

  function walk (dir) {
    var results = []
      , list = fs.readdirSync(dir)

    list.forEach(function(file) {
      file = path.resolve(dir, file)
      var stat = fs.statSync(file)

      if (stat && stat.isDirectory()) {
        results = results.concat(walk(file))
      } else {

        var containsExclude = false

        _.each(exludePatterns, function(pattern) {
          if (file.indexOf(pattern) !== -1) {
            containsExclude = true
          }
        })

        if (!containsExclude) {
          results.push(file)
        }
      }
    })

    return results
  }

 return walk(modulePath)
}
