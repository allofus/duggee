# PhaserLE Alpha v0.1.0

Craig, use KFBlake SSH key, add to puttygen manually.

PhaserLE is a level editor available on the desktop for Linux, Mac OSX and Windows operating systems that increases the productivity of game developers.

## Getting up and Running

### Node.js and Grunt

You will need to have [Node.js](http://nodejs.org/download/) and the grunt-cli installed: `npm install -g grunt-cli`.

### Get the Code

Either, download and unpack [The PhaserLE Source](https://bitbucket.org/GrindheadGames/phaserle/get/HEAD.zip), or clone from an existing project:

    git clone git@bitbucket.org:GrindheadGames/phaserle.git

Finally, inside the project, you need to install the project's various NPM dependencies:

    npm install

## Developing

Start Grunt's default target to spin up a development build of your new project:

    grunt

All of the files required to run the game will live in the `/src` folder, this will include any JavaScript, images, HTML ([Jade](http://jade-lang.com/)), and CSS ([Stylus](http://learnboost.github.io/stylus/)). When the default grunt task is invoked, these files are compiled to a `/build` directory.

Files in the `/build` directory will always be generated and excluded from Git by the `/.gitignore`, as such these will removed without warning and should generally not be edited.

### Recommendations

* Use relative file paths for any assets loaded by your HTML or JavaScript. This will negate any potential path issues when the game is later uploaded to a webserver.
* When working with [Texture Atlases](http://en.wikipedia.org/wiki/Texture_atlas), place individual sprites and template files in the `/assets` directory and output your atlases and their associated data to the `/src/images` directory.
* Borwserify is crazy powerful. I'm not going to quote Spiderman, but you should definitely check out [Substack's Browserify Handbook](https://github.com/substack/browserify-handbook).

### Updating or Adding Libraries

The project comes with an unminified version of Phaser with arcade physics, this can be replaced if you require updates or one of the alternate physics engines. (Except P2, which currently requires a little fiddling to get working with Browserify.)

When adding new libraries that aren't CommonJS compatible, you'll have to update the [Browserify Shim configuration](https://github.com/thlorenz/browserify-shim#you-will-always).

### Available Targets

    grunt

Configures and runs an unminified development build optimised for fast watch performance with source maps and live reload.

    grunt build

Creates an uglified, production ready build with no source maps.

    grunt optimise

Lossy compression of all png's in the `/src/images/` directory using [pngquant](http://pngquant.org/).

(Linux users will need to have a version of pngquant available on their paths.)

    grunt zip

Compiles the current build into `/build/{title}-{YYYY-MM-DD}.zip` with an internal folder. This is intended for use when transferring the build to a third party for upload webserver.

    grunt cocoon

Compiles the current build into `/build/build.zip` ready for upload to [CocoonJs](https://www.ludei.com/cocoonjs/).

### Coding Style and Linting

[Ben Gourley's JavaScript Style Guide](https://github.com/bengourley/js-style-guide).
