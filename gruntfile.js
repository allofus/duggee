var properties = require('./src/js/game/properties')
  , editorProperties = require('./editor/src/js/app/properties')
  , moduleList = require('./editor/util/module-list')
  , path = require('path')
  , moduleData = moduleList(path.join(__dirname + '/src/js/game/entities'))

module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-browserify')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-compress')
  grunt.loadNpmTasks('grunt-contrib-connect')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-jade')
  grunt.loadNpmTasks('grunt-contrib-jshint')
  grunt.loadNpmTasks('grunt-contrib-stylus')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-open')
  grunt.loadNpmTasks('grunt-pngmin')
  grunt.loadNpmTasks('grunt-express')
  grunt.loadNpmTasks('grunt-mkdir')
  grunt.loadNpmTasks('grunt-ftp-deploy')

  var productionBuild = grunt.cli.tasks.length && grunt.cli.tasks[0] === 'build'

  grunt.initConfig(
    { pkg: grunt.file.readJSON('package.json')

    , project:
      { src: 'src/js'
      , js: '<%= project.src %>/game/{,*/}*.js'
      , dest: 'build/js'
      , bundle: 'build/main.js'
      , port: properties.port
      , banner:
        '/*!\n' +
        ' * <%= pkg.title %>\n' +
        ' * <%= pkg.description %>\n' +
        ' * <%= pkg.url %>\n' +
        ' * @author <%= pkg.author %>\n' +
        ' * @version <%= pkg.version %>\n' +
        ' * Copyright <%= pkg.author %>. <%= pkg.license %> licensed.\n' +
        ' * Made using Phaser Blank <https://github.com/lukewilde/phaser-blank/>\n' +
        ' */\n'
      }

    , express:
      { server:
        { options:
          { port: '<%= project.port %>'
          , bases: ['./build']
          , server: productionBuild ? false : './editor/util/server.js'
          }
        }
      }

    , jshint:
      { files:
        [ 'gruntfile.js'
        , '<%= project.js %>'
        ]
      , options:
        { jshintrc: '.jshintrc'
        }
      }

    , 'ftp-deploy':
      { build:
        { auth:
          { host: 'games-ftp.cloud.bbc.co.uk'
          , port: 21
          , authKey: 'key1'
          }
        , src: 'build'
        , dest: 'shared/cbeebies/hey-duggee/hey-duggee/html5/v1'
        , exclusions: ['']
        , keep: ['']
        , simple: true
        }
      }

    , watch:
      { options:
        { livereload: productionBuild ? false : properties.liveReloadPort
        }
      , js:
        { files: '<%= project.dest %>/**/*.js'
        , tasks: ['jade:game']
        }
      , editorJs:
        { files: 'editor/build/js/app.min.js'
        }
      , editorStylus:
        { files: 'editor/src/style/*.styl'
        , tasks: ['stylus:editor']
        }
      , gameStylus:
        { files: 'src/style/*.styl'
        , tasks: ['stylus:game']
        }
      , editorJade:
        { files: 'editor/src/templates/**/*'
        , tasks: ['jade:editor', 'browserify:editor']
        }
      , gameJade:
        { files: 'src/templates/*.jade'
        , tasks: ['jade:game']
        }
      , images:
        { files: 'src/images/**/*'
        , tasks: ['copy:images']
        }
      , editorImages:
        { files: 'editor/src/images/**/*'
        , tasks: ['copy:editorImages']
        }
      , audio:
        { files: 'src/audio/**/*'
        , tasks: ['copy:audio']
        }
      , server:
        { files: 'editor/util/*'
        , tasks: ['express']
        }
      }

    , browserify:
      { game:
        { src: ['<%= project.src %>/game/startup.js']
        , dest: '<%= project.bundle %>'
        , options:
          { transform: ['browserify-shim', 'jadeify']
          , watch: true
          , browserifyOptions:
            { debug: !productionBuild
            }
          }
        }
      , editor:
        { src: ['editor/src/js/app/startup.js']
        , dest: 'editor/build/main.js'
        , options:
          { transform: ['browserify-shim', 'jadeify']
          , watch: true
          , require: moduleData
          , browserifyOptions:
            { debug: true
            }
          }
        }
      }

    , open:
      { server:
        { path: 'http://localhost:<%= project.port %>'
        }
      }

    , cacheBust:
      { options:
        { encoding: 'utf8'
        , algorithm: 'md5'
        , length: 8
        }
      , assets:
        { files:
          [ { src:
              [ 'build/index.html'
              , '<%= project.bundle %>'
              ]
            }
          ]
        }
      }

    , jade:
      { editor:
        { options:
          { data:
            { properties: editorProperties
            , modules: JSON.stringify(moduleData)
            , liveReloadPort: properties.liveReloadPort
            , productionBuild: productionBuild
            }
          }
        , files:
          { 'editor/build/index.html': ['editor/src/templates/index.jade']
          }
        }
      , game:
        { options:
          { data:
            { properties: properties
            , productionBuild: productionBuild
            }
          }
        , files:
          { 'build/index.html': ['src/templates/index.jade']
          }
        }
      }

    , stylus:
      { game:
        { files:
          { 'build/style/index.css': ['src/style/index.styl'] }
        , options:
          { sourcemaps: !productionBuild
          }
        }
      , editor:
        { files:
          { 'editor/build/style/index.css': ['editor/src/style/index.styl'] }
        , options:
          { sourcemaps: !productionBuild
          }
        }
      }

    , clean: ['./build/']

    , pngmin:
      { options:
        { ext: '.png'
        , force: true
        }
      , compile:
        { files:
            [ { src: 'src/images/*.png'
              , dest: 'src/images/'
              }
            ]
          }
        }

    , copy:
      { images:
        { files:
          [ { expand: true, cwd: 'src/images/', src: ['**'], dest: 'build/images/' }
          ]
        }
      , editorImages:
        { files:
          [ { expand: true, cwd: 'editor/src/images/', src: ['**'], dest: 'editor/build/images/' }
          ]
        }
      , audio:
        { files:
          [ { expand: true, cwd: 'src/audio/', src: ['**'], dest: 'build/audio/' }
          ]
        }
      }

    , mkdir:
      { levelPath:
        { options:
          { create: ['./src/js/game/levels'] }
        }
      }

    , uglify:
      { options:
        { banner: '<%= project.banner %>'
        }
      , dist:
        { files:
          { '<%= project.bundle %>': '<%= project.bundle %>'
          }
        }
      }

    , compress:
      { options:
        { archive: 'build/build.zip'
        }
      , main:
        { files: [ { expand: true, cwd: 'build/', src: ['**/*'], dest: 'build/' } ]
        }
      }
    }
  )

  grunt.registerTask('default',
    [ 'clean'
    , 'browserify'
    , 'jade'
    , 'stylus'
    , 'copy'
    , 'mkdir:levelPath'
    , 'express'
    , 'open'
    , 'watch'
    ]
  )

  grunt.registerTask('build',
    [ /*'jshint'
    , */'clean'
    , 'browserify:game'
    , 'jade:game'
    , 'stylus:game'
    , 'uglify'
    , 'copy:images'
    , 'copy:audio'
    //, 'cacheBust'
    //, 'express'
    //, 'open'
    //, 'watch'
    ]
  )

  grunt.registerTask('deploy',
    [ /*'jshint'
    , */'clean'
    , 'browserify:game'
    , 'jade:game'
    , 'stylus:game'
    , 'uglify'
    , 'copy:images'
    , 'copy:audio'
    ]
  )

  grunt.registerTask('optimise', ['pngmin', 'copy:images'])
}
