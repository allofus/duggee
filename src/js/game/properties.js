var npmProperties = require('../../../package.json')

module.exports =
  { title: 'Hey Duggee: Come and Play!'
  , description: npmProperties.description
  , serverURL: 'http://localhost'
  , port: 3020
  , liveReloadPort: 3021
  , analyticsId: 'UA-53585230-1'

  , cdnAddress: ''//'http://play.test.bbc.co.uk/play/game/cbeebies/hey-duggee/hey-duggee/html5/'
  , exitButtonURL: 'http://www.bbc.co.uk/cbeebies/games/hey-duggee-come-and-play'
  , grownUpsURL: 'http://www.bbc.co.uk/cbeebies/grownups/faqs'
  , moreFromThisShowURL: 'http://www.bbc.co.uk/cbeebies/shows/hey-duggee'
  , otherCbeebiesGamesURL: 'http://www.bbc.co.uk/cbeebies/games'

  // debug settings.
  , showStats: false
  , safeAreaPadding: 24
  , buttonPadding: 16
  , margin: 16
  , buttonSize: 64
  , autoMute: false
  }
