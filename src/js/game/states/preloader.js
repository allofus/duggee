var LoaderInfo = require('../../lib/loader')
  , properties = require('properties')
  , _ = require('lodash')
  , GameData = require('GameData')
  , ScreenProperties = require('Screen-Properties')
  , RotateOverlay = require('../../lib/rotate-overlay')
  , loaderInfo
  , preloader = {}


  , rO

preloader.preload = function () {
  this.game.load.crossOrigin = 'anonymous'

  //texture atlas's
  this.game.load.atlasJSONHash('atlas', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/atlas.png', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/atlas.json')
  this.game.load.atlasJSONHash('atlas2', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/atlas2.png', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/atlas2.json')
  this.game.load.atlasJSONHash('duggee', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/duggee.png', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/duggee.json')

  //markers
  this.game.load.image('barrel-badge', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/BARREL_BADGE.png')
  this.game.load.image('sheep-badge', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/SHEEP_BADGE.png')
  this.game.load.image('hide-badge', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/HIDE_SEEK_BADGE.png')
  this.game.load.image('chicken-badge', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/BADGE_CHICKEN.png')
  this.game.load.image('rabbit-badge', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/BADGE_RABBIT.png')

  //static images
  this.game.load.image('logo', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/logo.png')

  //backgrounds
  this.game.load.image('pause-bg', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/paused_bg.jpg')
  this.game.load.image('background', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/background.jpg')
  this.game.load.image('house-main-menu', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/house-main-menu.jpg')
  this.game.load.image('cloud-main-menu', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/cloud-main-menu.jpg')
  this.game.load.image('monkey-instructions', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/monkey-instructions.png')
  this.game.load.image('rabbit-instructions', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/rabbit-instructions.png')
  this.game.load.image('sheep-instructions', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/sheep-instructions.png')
  this.game.load.image('frog-instructions', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/frog-instructions.png')
  this.game.load.image('chicken-instructions', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/chicken-instructions.png')
  this.game.load.image('settings-bg', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/settings-bg.jpg')

  this.game.load.image('barrel-tree', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/barrel-tree.jpg')
  this.game.load.image('sheep-tree-left', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/sheep-tree-left.jpg')
  this.game.load.image('sheep-tree', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/sheep-tree.png')


  this.game.load.image('bush-1', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/BUSH_A.png')
  this.game.load.image('bush-2', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/BUSH_B.png')
  this.game.load.image('treehouse', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/TREEHOUSE.png')
  this.game.load.atlasJSONHash('buttons', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/buttons.png', properties.cdnAddress+ 'images/'+ScreenProperties.screen+'/buttons.json')


  //game music
  if(this.game.sound.usingWebAudio) {
    this.game.load.audio('a', [properties.cdnAddress+'audio/piano-a.mp3', properties.cdnAddress+'audio/piano-a.ogg'])
    this.game.load.audio('b', [properties.cdnAddress+'audio/piano-b.mp3', properties.cdnAddress+'audio/piano-b.ogg'])
    this.game.load.audio('bb', [properties.cdnAddress+'audio/piano-bb.mp3', properties.cdnAddress+'audio/piano-bb.ogg'])
    this.game.load.audio('c', [properties.cdnAddress+'audio/piano-c.mp3', properties.cdnAddress+'audio/piano-c.ogg'])
    this.game.load.audio('d', [properties.cdnAddress+'audio/piano-d.mp3', properties.cdnAddress+'audio/piano-d.ogg'])

    this.game.load.audio('pop1', [properties.cdnAddress+'audio/pop1.mp3', properties.cdnAddress+'audio/pop1.ogg'])
    this.game.load.audio('pop2', [properties.cdnAddress+'audio/pop2.mp3', properties.cdnAddress+'audio/pop2.ogg'])
    this.game.load.audio('pop3', [properties.cdnAddress+'audio/pop3.mp3', properties.cdnAddress+'audio/pop3.ogg'])
    this.game.load.audio('pop4', [properties.cdnAddress+'audio/pop4.mp3', properties.cdnAddress+'audio/pop4.ogg'])
    this.game.load.audio('pop5', [properties.cdnAddress+'audio/pop5.mp3', properties.cdnAddress+'audio/pop5.ogg'])

    this.game.load.audio('flies-plane', [properties.cdnAddress+'audio/flies-plane.mp3', properties.cdnAddress+'audio/flies-plane.ogg'])
    this.game.load.audio('monkey1', [properties.cdnAddress+'audio/monkey1.mp3', properties.cdnAddress+'audio/monkey1.ogg'])
    this.game.load.audio('monkey2', [properties.cdnAddress+'audio/monkey2.mp3', properties.cdnAddress+'audio/monkey2.ogg'])
    this.game.load.audio('monkey3', [properties.cdnAddress+'audio/monkey3.mp3', properties.cdnAddress+'audio/monkey3.ogg'])
    this.game.load.audio('monkey4', [properties.cdnAddress+'audio/monkey4.mp3', properties.cdnAddress+'audio/monkey4.ogg'])
    this.game.load.audio('ribbit', [properties.cdnAddress+'audio/ribbit.mp3', properties.cdnAddress+'audio/ribbit.ogg'])
    this.game.load.audio('spinning-top', [properties.cdnAddress+'audio/spinning-top.mp3', properties.cdnAddress+'audio/spinning-top.ogg'])
    this.game.load.audio('train', [properties.cdnAddress+'audio/train.mp3', properties.cdnAddress+'audio/train.ogg'])

    this.game.load.audio('barrel-game-correct1', [properties.cdnAddress+'audio/barrel-game-correct1.mp3', properties.cdnAddress+'audio/barrel-game-correct1.ogg'])
    this.game.load.audio('barrel-game-correct2', [properties.cdnAddress+'audio/barrel-game-correct2.mp3', properties.cdnAddress+'audio/barrel-game-correct2.ogg'])
    this.game.load.audio('barrel-game-correct3', [properties.cdnAddress+'audio/barrel-game-correct3.mp3', properties.cdnAddress+'audio/barrel-game-correct3.ogg'])

    this.game.load.audio('barrel-game-wrong1', [properties.cdnAddress+'audio/barrel-game-wrong1.mp3', properties.cdnAddress+'audio/barrel-game-wrong1.ogg'])
    this.game.load.audio('barrel-game-wrong2', [properties.cdnAddress+'audio/barrel-game-wrong2.mp3', properties.cdnAddress+'audio/barrel-game-wrong2.ogg'])

    this.game.load.audio('scrape-1', [properties.cdnAddress+'audio/scrape1.mp3', properties.cdnAddress+'audio/scrape1.ogg'])
    this.game.load.audio('scrape-2', [properties.cdnAddress+'audio/scrape2.mp3', properties.cdnAddress+'audio/scrape2.ogg'])
    this.game.load.audio('scrape-3', [properties.cdnAddress+'audio/scrape3.mp3', properties.cdnAddress+'audio/scrape3.ogg'])

    this.game.load.audio('monkey-cry', [properties.cdnAddress+'audio/monkey-cry.mp3', properties.cdnAddress+'audio/monkey-cry.ogg'])
    this.game.load.audio('award-badge', [properties.cdnAddress+'audio/award-badge.mp3', properties.cdnAddress+'audio/award-badge.ogg'])
    this.game.load.audio('woof1', [properties.cdnAddress+'audio/woof-1.mp3', properties.cdnAddress+'audio/woof-1.ogg'])
    this.game.load.audio('woof2', [properties.cdnAddress+'audio/woof-2.mp3', properties.cdnAddress+'audio/woof-2.ogg'])
    this.game.load.audio('music', [properties.cdnAddress+'audio/music.mp3', properties.cdnAddress+'audio/music.ogg'])
    this.game.load.audio('spider-in', [properties.cdnAddress+'audio/spider-in.mp3', properties.cdnAddress+'audio/spider-in.ogg'])
    this.game.load.audio('spider-out', [properties.cdnAddress+'audio/spider-out.mp3', properties.cdnAddress+'audio/spider-out.ogg'])
    this.game.load.audio('roli-shout-1', [properties.cdnAddress+'audio/roli-shout-1.mp3', properties.cdnAddress+'audio/roli-shout-1.ogg'])
    this.game.load.audio('roli-shout-2', [properties.cdnAddress+'audio/roli-shout-2.mp3', properties.cdnAddress+'audio/roli-shout-2.ogg'])
    this.game.load.audio('beatbox', [properties.cdnAddress+'audio/beatbox.mp3', properties.cdnAddress+'audio/beatbox.ogg'])
    this.game.load.audio('betty-face', [properties.cdnAddress+'audio/betty-face.mp3', properties.cdnAddress+'audio/betty-face.ogg'])
    this.game.load.audio('betty-read', [properties.cdnAddress+'audio/betty-read.mp3', properties.cdnAddress+'audio/betty-read.ogg'])
    this.game.load.audio('norrie-giggle-1', [properties.cdnAddress+'audio/norrie-giggle-1.mp3', properties.cdnAddress+'audio/norrie-giggle-1.ogg'])
    this.game.load.audio('norrie-giggle-2', [properties.cdnAddress+'audio/norrie-giggle-2.mp3', properties.cdnAddress+'audio/norrie-giggle-2.ogg'])
    this.game.load.audio('tag-giggle', [properties.cdnAddress+'audio/tag-giggle.mp3', properties.cdnAddress+'audio/tag-giggle.ogg'])

    //vo's
    this.game.load.audio('vo-title', [properties.cdnAddress+'audio/vo/hey_duggee_come_play.mp3', properties.cdnAddress+'audio/vo/hey_duggee_come_play.ogg'])
    this.game.load.audio('vo-home', [properties.cdnAddress+'audio/vo/home.mp3', properties.cdnAddress+'audio/vo/home.ogg'])
    this.game.load.audio('vo-back', [properties.cdnAddress+'audio/vo/back.mp3', properties.cdnAddress+'audio/vo/back.ogg'])
    this.game.load.audio('vo-help', [properties.cdnAddress+'audio/vo/help.mp3', properties.cdnAddress+'audio/vo/help.ogg'])
    this.game.load.audio('vo-mute', [properties.cdnAddress+'audio/vo/mute.mp3', properties.cdnAddress+'audio/vo/mute.ogg'])
    this.game.load.audio('vo-play', [properties.cdnAddress+'audio/vo/play.mp3', properties.cdnAddress+'audio/vo/play.ogg'])
    this.game.load.audio('vo-close', [properties.cdnAddress+'audio/vo/close.mp3', properties.cdnAddress+'audio/vo/close.ogg'])
    this.game.load.audio('vo-are-you-ready', [properties.cdnAddress+'audio/vo/are_you_ready.mp3', properties.cdnAddress+'audio/vo/are_you_ready.ogg'])
    this.game.load.audio('vo-replay', [properties.cdnAddress+'audio/vo/replay.mp3', properties.cdnAddress+'audio/vo/replay.ogg'])
    this.game.load.audio('vo-play-again', [properties.cdnAddress+'audio/vo/play_again.mp3', properties.cdnAddress+'audio/vo/play_again.ogg'])
    this.game.load.audio('vo-pause', [properties.cdnAddress+'audio/vo/pause.mp3', properties.cdnAddress+'audio/vo/pause.ogg'])

    this.game.load.audio('vo-sheepBadge', [properties.cdnAddress+'audio/vo/well_done_sheep_badge_FAST.mp3', properties.cdnAddress+'audio/vo/well_done_sheep_badge_FAST.ogg'])
    this.game.load.audio('vo-monkeyBadge', [properties.cdnAddress+'audio/vo/well_done_monkey_badge_FAST.mp3', properties.cdnAddress+'audio/vo/well_done_monkey_badge_FAST.ogg'])
    this.game.load.audio('vo-chickenBadge', [properties.cdnAddress+'audio/vo/well_done_chicken_badge_FAST.mp3', properties.cdnAddress+'audio/vo/well_done_chicken_badge_FAST.ogg'])
    this.game.load.audio('vo-rabbitBadge', [properties.cdnAddress+'audio/vo/well_done_rabbit_badge_FAST.mp3', properties.cdnAddress+'audio/vo/well_done_rabbit_badge_FAST.ogg'])
    this.game.load.audio('vo-frogBadge', [properties.cdnAddress+'audio/vo/well_done_frog_badge_FAST.mp3', properties.cdnAddress+'audio/vo/well_done_frog_badge_FAST.ogg'])

    this.game.load.audio('vo-busy-squirrel-club', [properties.cdnAddress+'audio/vo/busy_squirrel_club.mp3', properties.cdnAddress+'audio/vo/busy_squirrel_club.ogg'])
    this.game.load.audio('vo-found-music-play-sheep', [properties.cdnAddress+'audio/vo/found_music_play_sheep.mp3', properties.cdnAddress+'audio/vo/found_music_play_sheep.ogg'])
    this.game.load.audio('vo-found-finding-game', [properties.cdnAddress+'audio/vo/well_done_you_found_finding_game.mp3', properties.cdnAddress+'audio/vo/well_done_you_found_finding_game.ogg'])
    this.game.load.audio('vo-found-barrel-game', [properties.cdnAddress+'audio/vo/ah_you_found_barrell_game.mp3', properties.cdnAddress+'audio/vo/ah_you_found_barrell_game.ogg'])
    this.game.load.audio('vo-well-done-right-barrel', [properties.cdnAddress+'audio/vo/well_done_right_barrel.mp3', properties.cdnAddress+'audio/vo/well_done_right_barrel.ogg'])
    this.game.load.audio('vo-incorrect-barrel', [properties.cdnAddress+'audio/vo/oh_right_barrel.mp3', properties.cdnAddress+'audio/vo/oh_right_barrel.ogg'])
    this.game.load.audio('vo-incorrect-barrel-2', [properties.cdnAddress+'audio/vo/almost_try_again.mp3', properties.cdnAddress+'audio/vo/almost_try_again.ogg'])

    this.game.load.audio('vo-click-tap-monkey-in', [properties.cdnAddress+'audio/vo/click_tap_monkey_in.mp3', properties.cdnAddress+'audio/vo/click_tap_monkey_in.ogg'])
    this.game.load.audio('vo-click-tap-rabbit-in', [properties.cdnAddress+'audio/vo/click_tap_rabbit_in.mp3', properties.cdnAddress+'audio/vo/click_tap_rabbit_in.ogg'])
  }else {
    this.game.load.audio('sfx', [properties.cdnAddress+'audio/sfx.mp3', properties.cdnAddress+'audio/sfx.ogg' ])
  }

  ScreenProperties.calculateSize()
  //draw pink
  // set a fill and line style
  var graphics = this.game.add.graphics(0, 0)
  graphics.beginFill(0xffa1bb)
  graphics.drawRect(0, 0, this.game.width, this.game.height)
  graphics.endFill()

  //floor
  var floor = this.game.add.graphics(0, 385 * ScreenProperties.gameScale)
  floor.beginFill(0xe1af2c)
  floor.drawRect(0, 0, this.game.width, this.game.height -floor.y)
  floor.endFill()

  var bg = this.game.add.image(this.game.width/2, this.game.height/2, 'preloader')
  bg.scale.setTo(ScreenProperties.gameScale)
  bg.anchor.setTo(.5)

  loaderInfo = new LoaderInfo()
  loaderInfo.setupLoader(this.game)

  var cLogo = this.game.add.image(this.game.width/2, 455 * ScreenProperties.gameScale, 'c-beebies-logo')
  cLogo.scale.setTo(.8 * ScreenProperties.gameScale)
  cLogo.anchor.setTo(.5)

  rO = new RotateOverlay(this.game)

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
    rO.open()
  }
}

preloader.create = function () {
  this.game.audioStore = {}

  if(this.game.sound.usingWebAudio) {

    this.game.audioStore.a = this.game.add.audio('a')
    this.game.audioStore.b = this.game.add.audio('b')
    this.game.audioStore.bb = this.game.add.audio('bb')
    this.game.audioStore.c = this.game.add.audio('c')
    this.game.audioStore.d = this.game.add.audio('d')

    this.game.audioStore.fliesPlane = this.game.add.audio('flies-plane')
    this.game.audioStore.monkey1 = this.game.add.audio('monkey1')
    this.game.audioStore.monkey2 = this.game.add.audio('monkey2')
    this.game.audioStore.monkey3 = this.game.add.audio('monkey3')
    this.game.audioStore.monkey4 = this.game.add.audio('monkey4')

    this.game.audioStore.scrape1 = this.game.add.audio('scrape-1')
    this.game.audioStore.scrape2 = this.game.add.audio('scrape-2')
    this.game.audioStore.scrape3 = this.game.add.audio('scrape-3')

    this.game.audioStore.pop1 = this.game.add.audio('pop1', .4)
    this.game.audioStore.pop2 = this.game.add.audio('pop2', .4)
    this.game.audioStore.pop3 = this.game.add.audio('pop3', .4)
    this.game.audioStore.pop4 = this.game.add.audio('pop4', .4)
    this.game.audioStore.pop5 = this.game.add.audio('pop5', .4)

    this.game.audioStore.ribbit = this.game.add.audio('ribbit')
    this.game.audioStore.spinningTop = this.game.add.audio('spinning-top')
    this.game.audioStore.train = this.game.add.audio('train')

    this.game.audioStore.barrelGameCorrect1 = this.game.add.audio('barrel-game-correct1',.15)
    this.game.audioStore.barrelGameCorrect2 = this.game.add.audio('barrel-game-correct2', .15)
    this.game.audioStore.barrelGameCorrect3 = this.game.add.audio('barrel-game-correct3', .15)

    this.game.audioStore.barrelGameWrong1 = this.game.add.audio('barrel-game-wrong1')
    this.game.audioStore.barrelGameWrong2 = this.game.add.audio('barrel-game-wrong2')

    this.game.audioStore.monkeyCry = this.game.add.audio('monkey-cry')
    this.game.audioStore.awardBadge = this.game.add.audio('award-badge')
    this.game.audioStore.woof1 = this.game.add.audio('woof1')
    this.game.audioStore.woof2 = this.game.add.audio('woof2')
    this.game.audioStore.music = this.game.add.audio('music', .2)
    this.game.audioStore.spiderIn = this.game.add.audio('spider-in')
    this.game.audioStore.spiderOut = this.game.add.audio('spider-out')
    this.game.audioStore.roliShout1 = this.game.add.audio('roli-shout-1')
    this.game.audioStore.roliShout2 = this.game.add.audio('roli-shout-2')
    this.game.audioStore.beatbox = this.game.add.audio('beatbox')
    this.game.audioStore.bettyFace = this.game.add.audio('betty-face')
    this.game.audioStore.bettyRead = this.game.add.audio('betty-read')
    this.game.audioStore.norrieGiggle1 = this.game.add.audio('norrie-giggle-1')
    this.game.audioStore.norrieGiggle2 = this.game.add.audio('norrie-giggle-2')
    this.game.audioStore.tagGiggle = this.game.add.audio('tag-giggle')

    //vo
    var hudItemVolume = .5
    this.game.audioStore.voTitle = this.game.add.audio('vo-title')
    this.game.audioStore.voHome = this.game.add.audio('vo-home', hudItemVolume)
    this.game.audioStore.voBack = this.game.add.audio('vo-back', hudItemVolume)
    this.game.audioStore.voHelp = this.game.add.audio('vo-help', hudItemVolume)
    this.game.audioStore.voMute = this.game.add.audio('vo-mute', hudItemVolume)
    this.game.audioStore.voPlay = this.game.add.audio('vo-play', hudItemVolume)
    this.game.audioStore.voPause = this.game.add.audio('vo-pause', hudItemVolume + .2)
    this.game.audioStore.voClose = this.game.add.audio('vo-close', hudItemVolume)
    this.game.audioStore.voAreYouReady = this.game.add.audio('vo-are-you-ready', hudItemVolume)
    this.game.audioStore.voReplay = this.game.add.audio('vo-replay', hudItemVolume)
    this.game.audioStore.voPlayAgain = this.game.add.audio('vo-play-again', hudItemVolume)
    this.game.audioStore.voSheepBadge = this.game.add.audio('vo-sheepBadge')
    this.game.audioStore.voMonkeyBadge = this.game.add.audio('vo-monkeyBadge')
    this.game.audioStore.voChickenBadge = this.game.add.audio('vo-chickenBadge')
    this.game.audioStore.voRabbitBadge = this.game.add.audio('vo-rabbitBadge')
    this.game.audioStore.voFrogBadge = this.game.add.audio('vo-frogBadge')

    this.game.audioStore.voBusySquirrelClub = this.game.add.audio('vo-busy-squirrel-club')
    this.game.audioStore.voFoundMusicPlaySheep = this.game.add.audio('vo-found-music-play-sheep')
    this.game.audioStore.voFoundFindingGame = this.game.add.audio('vo-found-finding-game')
    this.game.audioStore.voFoundBarrelGame = this.game.add.audio('vo-found-barrel-game')
    this.game.audioStore.voRightBarrel = this.game.add.audio('vo-well-done-right-barrel')
    this.game.audioStore.voIncorrectBarrel = this.game.add.audio('vo-incorrect-barrel')
    this.game.audioStore.voIncorrectBarrel2 = this.game.add.audio('vo-incorrect-barrel-2')

    this.game.audioStore.voTapMonkey = this.game.add.audio('vo-click-tap-monkey-in')
    this.game.audioStore.voTapRabbit = this.game.add.audio('vo-click-tap-rabbit-in')
  }else {
    this.game.audioStore.fx = this.game.add.audio('sfx')
    this.game.audioStore.fx.addMarker('award-badge', 0, 1.724)
    this.game.audioStore.fx.addMarker('barrel-game-correct-1', 1.839, 1.464)
    this.game.audioStore.fx.addMarker('barrel-game-correct-2', 3.457, 1.92)
    this.game.audioStore.fx.addMarker('barrel-game-correct-3', 5.486, 1.68)
    this.game.audioStore.fx.addMarker('barrel-game-wrong-1', 7.267, 1.944)
    this.game.audioStore.fx.addMarker('barrel-game-wrong-2', 9.299, 2.544)
    this.game.audioStore.fx.addMarker('beatbox', 11.941, 3.056)
    this.game.audioStore.fx.addMarker('betty-face', 15.188, 2.847)
    this.game.audioStore.fx.addMarker('betty-read', 18.15, 1.332)
    this.game.audioStore.fx.addMarker('flies-plane', 19.61, 3.624)
    this.game.audioStore.fx.addMarker('monkey-cry', 23.362, 2.016)
    this.game.audioStore.fx.addMarker('monkey-1', 25.475, .912)
    this.game.audioStore.fx.addMarker('monkey-2', 26.517, 1.512)
    this.game.audioStore.fx.addMarker('monkey-3', 28.119, 1.632)
    this.game.audioStore.fx.addMarker('monkey-4', 29.878, .864)
    this.game.audioStore.fx.addMarker('norrie-giggle-1', 30.813, .731)
    this.game.audioStore.fx.addMarker('norrie-giggle-2', 31.668, .862)
    this.game.audioStore.fx.addMarker('a', 32.698, 1.032)
    this.game.audioStore.fx.addMarker('b', 33.833, .864)
    this.game.audioStore.fx.addMarker('bb', 34.811, .864)
    this.game.audioStore.fx.addMarker('c', 35.783, .792)
    this.game.audioStore.fx.addMarker('d', 36.671, .840)
    this.game.audioStore.fx.addMarker('pop-1', 37.612, .432)
    this.game.audioStore.fx.addMarker('pop-2', 38.123, .240)
    this.game.audioStore.fx.addMarker('pop-3', 38.418, .360)
    this.game.audioStore.fx.addMarker('pop-4', 38.846, .288)
    this.game.audioStore.fx.addMarker('pop-5', 39.242, .324)
    this.game.audioStore.fx.addMarker('ribbit', 39.654, 1.224)
    this.game.audioStore.fx.addMarker('roli-shout-1', 41.001, 2.247)
    this.game.audioStore.fx.addMarker('roli-shout-2', 43.36, 2.247)
    this.game.audioStore.fx.addMarker('scrape-1', 45.719, 1.26)
    this.game.audioStore.fx.addMarker('scrape-2', 47.093, 1.188)
    this.game.audioStore.fx.addMarker('scrape-3', 48.391, 1.008)
    this.game.audioStore.fx.addMarker('spider-in', 49.529, 1.008)
    this.game.audioStore.fx.addMarker('spider-out', 50.691, .936)
    this.game.audioStore.fx.addMarker('spinning-top', 51.737, 2.64)
    this.game.audioStore.fx.addMarker('tag-giggle', 54.501, 4.023)
    this.game.audioStore.fx.addMarker('train', 58.69, 2.952)
    this.game.audioStore.fx.addMarker('woof-1', 61.786, 1.097)
    this.game.audioStore.fx.addMarker('woof-2', 63.05, 1.019)
    this.game.audioStore.fx.addMarker('vo-found-barrel-game', 64.182, 2.88)
    this.game.audioStore.fx.addMarker('vo-incorrect-barrel-2', 67.22, 2.712)
    this.game.audioStore.fx.addMarker('vo-are-you-ready', 70, 1.584)
    this.game.audioStore.fx.addMarker('vo-back', 71.695, .840)
    this.game.audioStore.fx.addMarker('vo-busy-squirrel-club', 72.645, 5.4)
    this.game.audioStore.fx.addMarker('vo-click-tap-monkey-in', 78.165, 3.24)
    this.game.audioStore.fx.addMarker('vo-click-tap-rabbit-in', 81.569, 3.36)
    this.game.audioStore.fx.addMarker('vo-close', 85.084, .984)
    this.game.audioStore.fx.addMarker('vo-found-music-play-sheep', 86.292, 4.8)
    this.game.audioStore.fx.addMarker('vo-help', 91.280, .912)
    this.game.audioStore.fx.addMarker('vo-title', 92.352, 2.904)
    this.game.audioStore.fx.addMarker('vo-home', 95.429, .624)
    this.game.audioStore.fx.addMarker('vo-mute', 96.234, .912)
    this.game.audioStore.fx.addMarker('vo-incorrect-barrel', 97.289, 2.712)
    this.game.audioStore.fx.addMarker('vo-pause', 100.149, .888)
    this.game.audioStore.fx.addMarker('vo-play', 101.149, .912)
    this.game.audioStore.fx.addMarker('vo-play-again', 102.222, 1.2)
    this.game.audioStore.fx.addMarker('vo-replay', 103.572, .984)
    this.game.audioStore.fx.addMarker('vo-chicken-badge', 104.685, 3.072)
    this.game.audioStore.fx.addMarker('vo-frog-badge', 107.914, 2.88)
    this.game.audioStore.fx.addMarker('vo-monkey-badge', 110.928, 2.88)
    this.game.audioStore.fx.addMarker('vo-rabbit-badge', 113.91, 3.024)
    this.game.audioStore.fx.addMarker('vo-well-done-right-barrel', 117.47, 3.480)
    this.game.audioStore.fx.addMarker('vo-sheep-badge', 120.636, 2.784)
    this.game.audioStore.fx.addMarker('vo-found-finding-game', 123.548, 3.912)

    this.game.audioStore.fx.mute = GameData.isMuted
  }
}

preloader.update = function () {
  if (loaderInfo.loaded && areAllSoundsDecoded(this.game)) {
    setTimeout(_.bind(begin, this), 300)
  }
}

function areAllSoundsDecoded(game) {

  var allSoundsDecoded = true

  _.each(game.cache._sounds, function(sound) {
    if (!sound.decoded) {
      allSoundsDecoded = false
    }
  })

  return allSoundsDecoded
}

preloader.destroy = function () {
  this.game.cache.removeImage('preloader-bar')
  this.game.cache.removeImage('preloader')

  loaderInfo.destroy()
  loaderInfo = null
}

function begin() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
    setTimeout(_.bind(begin, this), 300)
    return
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_loaded", true);

  var div = document.getElementById('og-game-holder')
  , w = div.offsetWidth * window.devicePixelRatio
  , h = div.offsetHeight * window.devicePixelRatio

  this.game.scale.setGameSize((h > w) ? h : w, (h > w) ? w : h)
  ScreenProperties.calculateSize()

  this.game.sound.mute = properties.autoMute

  this.game.state.start('mainMenu')
}

module.exports = preloader
