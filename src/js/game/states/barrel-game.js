var AbstractState = require('./abstract-state')
  , MathUtils = require('MathUtils')
  , ScreenProperties = require('../../lib/screen-properties')
  , Phaser = require('Phaser')
  , HUD = require('../../lib/hud')
  , GameData = require('GameData')
  , properties = require('properties')
  , _ = require('lodash')

  , barrels = []
  , currentBarrel
  , monkey
  , currentMonkeyIdx
  , introCtr
  , introWaitTime
  , difficulty

  , waitToPlayAgainTimeout = null
  , isAnimating
  , gameGroup

  , playButton
  , winCount

  , winGroup
  , badge

  , hasReadInstructions = false

  , tweens = []
  , paused = false

var BarrelGame = function (game) {
  this.game = game
  AbstractState.call(game)
}

BarrelGame.prototype = Object.create(AbstractState.prototype)

BarrelGame.prototype.create = function() {

  var div = document.getElementById('og-game-holder')
    , w = div.offsetWidth * window.devicePixelRatio
    , h = div.clientHeight * window.devicePixelRatio

  this.game.scale.setGameSize((h > w) ? h : w, (h > w) ? w : h)
  ScreenProperties.calculateSize()

  this.name = 'barrel-game'

	barrels = []
  currentBarrel = null
  monkey = null
  currentMonkeyIdx = null
  introCtr = null
  difficulty = 0

	var graphics = this.game.add.graphics(0, 0);
  graphics.beginFill(0xb7edff)
  graphics.drawRect(0,0,this.game.width,this.game.height)
  graphics.endFill()

  var floor = this.game.add.graphics(0, 350 * ScreenProperties.gameScale)
  floor.beginFill(0xb9d366)
  floor.drawRect(0, 0, this.game.width, this.game.height -floor.y)
  floor.endFill()

  var tree = this.game.add.image(this.game.width, 0, 'barrel-tree')
  tree.scale.setTo(.5 * ScreenProperties.gameScale)
  tree.x -= tree.width

  var bgCloud = this.game.add.image(115 * ScreenProperties.gameScale, 80 * ScreenProperties.gameScale, 'cloud-main-menu')
  bgCloud.scale.setTo(ScreenProperties.gameScale)

  var img = this.game.add.image( this.game.width, this.game.height, 'atlas', 'hide-and-seek/flowers2.png')
  img.scale.setTo(ScreenProperties.gameScale)
  img.x-=img.width
  img.y-=img.height

  img = this.game.add.image( 0, this.game.height, 'atlas', 'hide-and-seek/flowers1.png')
  img.scale.setTo(ScreenProperties.gameScale)
  img.y-=img.height

  gameGroup = this.game.add.group()

	if(GameData.showRabbitOnBarrelGame) {
    this.addRabbit()
  }else {
    this.addMonkey()
  }

  this.addBarrel((this.game.width/2) - (270 * ScreenProperties.gameScale), 235 * ScreenProperties.gameScale, 0)
  currentBarrel = this.addBarrel((this.game.width/2)-80 * ScreenProperties.gameScale, 235 * ScreenProperties.gameScale, 1)
  this.addBarrel((this.game.width/2) + (110 * ScreenProperties.gameScale), 235 * ScreenProperties.gameScale, 2)

  //setup hud
  new HUD(this.game, true, false, true, false, pauseCallback)

  playButton = this.game.add.button(this.game.width/2, (318 * ScreenProperties.gameScale), 'atlas', begin, this, 'buttons/play-hover.png', 'buttons/play.png', 'buttons/play-hover.png', 'buttons/play-hover.png')
  playButton.anchor.setTo(.5)
  playButton.input.useHandCursor = true

  playButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voPlay.play()
    }
  }, this)

  winCount = 0

  if(!GameData.hasFoundBarrelGame) {
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voFoundBarrelGame.play()
    }else {
      this.game.audioStore.fx.play('vo-found-barrel-game')
    }

    GameData.hasFoundBarrelGame = true
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'selected', {'game_level_name':'barrels'});
}

BarrelGame.prototype.addRabbit = function() {
  monkey = this.game.add.sprite((this.game.width/2)-(50 * ScreenProperties.gameScale), 59 * ScreenProperties.gameScale, 'atlas2', 'RABBIT_BARRELGAME/Rabbit_master0035.png')

  monkey.animations.add('idle', ['RABBIT_BARRELGAME/Rabbit_master0035.png'], 25, true)
  monkey.animations.add('success', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 1, 47, '.png', 4), 25, false)
  monkey.animations.add('down', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 65, 71, '.png', 4), 25, false)
  monkey.animations.add('cry', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 72, 133, '.png', 4), 25, false)

  monkey.animations.getAnimation('cry').onComplete.add(function() {
    monkey.alpha = 0
    this.playGame()
  }, this)

  monkey.animations.getAnimation('success').onComplete.add(function() {
    monkey.animations.play('idle')
    monkey.alpha = 0
    if(winCount < 5 ) {
      this.playGame()
    }
  }, this)

  var loopCount = 20
  monkey.alpha = 1

  monkey.animations.getAnimation('down').onComplete.add(function() {
    monkey.alpha = 0
    this.playGame()
  }, this)

  monkey.scale.setTo(ScreenProperties.gameScale)
  gameGroup.add(monkey)

  monkey.animations.play('idle')
}

function pauseCallback() {

  paused = !paused

  _.each(tweens, function(tween) {
    if(paused) {
      tween.pause()
    }else {
      tween.resume()
    }
  });
}

BarrelGame.prototype.addMonkey = function() {
  //add the monkey!
  monkey = this.game.add.sprite((this.game.width/2)-(50 * ScreenProperties.gameScale), 70 * ScreenProperties.gameScale, 'atlas2', 'barrel-game/MONKEY_0001.png')

  monkey.animations.add('idle', ['barrel-game/MONKEY_0001.png'], 25, true)
  monkey.animations.add('blink', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 2, 6, '.png', 4), 25, false)
  monkey.animations.add('calebrate-1', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 61, 107, '.png', 4), 25, false)
  monkey.animations.add('calebrate-2', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 108, 160, '.png', 4), 25, false)
  monkey.animations.add('calebrate-3', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 161, 207, '.png', 4), 25, false)
  monkey.animations.add('cry', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 208, 289, '.png', 4), 25, false)
  monkey.animations.add('intro', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 38, 60, '.png', 4), 25, false)
  monkey.animations.add('down', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 51, 60, '.png', 4), 25, false)

  monkey.animations.getAnimation('down').onComplete.add(function() {
    monkey.animations.play('idle')
    this.playGame()
  }, this)

  monkey.animations.getAnimation('intro').onComplete.add(function() {
    monkey.animations.play('idle')
    this.playGame()
  }, this)

  monkey.animations.play('idle')

  monkey.animations.getAnimation('calebrate-1').onComplete.add(function() {
    monkey.animations.play('idle')
    if(winCount < 5 ) {
      this.playGame()
    }
  }, this)

  monkey.animations.getAnimation('calebrate-2').onComplete.add(function() {
    monkey.animations.play('idle')
    if(winCount < 5 ) {
      this.playGame()
    }
  }, this)

  monkey.animations.getAnimation('calebrate-3').onComplete.add(function() {
    monkey.animations.play('idle')
    if(winCount < 5 ) {
      this.playGame()
    }
  }, this)

  monkey.animations.getAnimation('blink').onComplete.add(function() {
    monkey.animations.play('idle')
  }, this)

  monkey.animations.getAnimation('idle').onLoop.add(function() {
    if(monkey.animations.currentAnim.loopCount >= 30) {
      monkey.animations.play('blink')
    }
  }, this)

  monkey.animations.getAnimation('cry').onComplete.add(function() {
    this.playGame()
  }, this)

  monkey.scale.setTo(ScreenProperties.gameScale)
  gameGroup.add(monkey)
}

function begin() {

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'started', {'game_level_name':'barrels'});

  playButton.destroy()
  playButton = null

  //begin the game...
  isAnimating = true
  currentMonkeyIdx = 1

  monkey.animations.play('down')
}

BarrelGame.prototype.shutdown = function() {

  this.game.tweens.pauseAll()
  this.game.sound.stopAll()

	clearTimeout(waitToPlayAgainTimeout)
	waitToPlayAgainTimeout = null
}

BarrelGame.prototype.playGame = function() {
	resetMonkey(this.game)
	this.moveMonkey()

	monkey.animations.play('idle')
}

function resetMonkey(game){

	barrels.sort(function(a, b) {
	  return a.x > b.x
	})

	introCtr = 5 + difficulty
	introWaitTime = 5 + difficulty

	if( introCtr > 20 ) {
		introCtr = 20
	}

	monkey.visible = false
	currentBarrel = barrels[currentMonkeyIdx]
}

BarrelGame.prototype.moveMonkey = function() {

	introCtr --

	if( introCtr <= 0 ) {
    if(GameData.showRabbitOnBarrelGame) {
      monkey.x = currentBarrel.x + (33 * ScreenProperties.gameScale)
    }else {
      monkey.x = currentBarrel.x + (20 * ScreenProperties.gameScale)
    }

    if(!hasReadInstructions) {
      hasReadInstructions = true

      if(this.game.sound.usingWebAudio) {
        if(GameData.showRabbitOnBarrelGame) {
          this.game.audioStore.voTapRabbit.play()
        }else {
          this.game.audioStore.voTapMonkey.play()
        }
      }else {
        if(GameData.showRabbitOnBarrelGame) {
          this.game.audioStore.fx.play('vo-click-tap-rabbit-in')
        }else {
          this.game.audioStore.fx.play('vo-click-tap-monkey-in')
        }
      }
    }

    isAnimating = false
		return
	}

	barrels.sort(function(a, b) {
	  return a.x > b.x
	})

	var targetIndex = chooseRandomBarrel()
		, targetBarrel = barrels[targetIndex]
		, time = 1000 - ( (introWaitTime-introCtr) / introWaitTime * 800)

  tweens = []
	//tween the current barrel to its destination
	tweens.push(this.game.add.tween(barrels[currentMonkeyIdx]).to( { x:targetBarrel.x }, time, Phaser.Easing.Quadratic.InOut, true))
	var tween = this.game.add.tween(targetBarrel).to( { x:barrels[currentMonkeyIdx].x }, time, Phaser.Easing.Quadratic.InOut, true)
	tween.onComplete.addOnce(this.moveMonkey, this)
  tweens.push(tween)

	//tween scale
	tweens.push(this.game.add.tween(barrels[currentMonkeyIdx].scale).to({x: barrels[currentMonkeyIdx].scale.x - (.05  * ScreenProperties.gameScale), y: barrels[currentMonkeyIdx].scale.y - (.05 * ScreenProperties.gameScale)} , time * .45, Phaser.Easing.Linear.In, true, 0, 0, true))
	tweens.push(this.game.add.tween(targetBarrel.scale).to( { x:targetBarrel.scale.x + (.05  * ScreenProperties.gameScale), y: targetBarrel.scale.y + (.05 * ScreenProperties.gameScale)} , time * .45, Phaser.Easing.Linear.In, true, 0, 0, true))
	targetBarrel.bringToTop()

  var n = MathUtils.getRandomInt(1,3)

  if(this.game.sound.usingWebAudio) {
    if(n === 1) {
      this.game.audioStore.scrape1.play()
    }else if(n === 2) {
      this.game.audioStore.scrape2.play()
    }else if(n === 3) {
      this.game.audioStore.scrape3.play()
    }
  }

	currentMonkeyIdx = targetIndex
}

function chooseRandomBarrel() {

	var targetIndex

	if(currentMonkeyIdx === 0 || currentMonkeyIdx === 2) {
		targetIndex = 1
	}else {
		if(MathUtils.getRandomBoolean()) {
			targetIndex = 0
		}else{
			targetIndex = 2
		}
	}

	return targetIndex
}

BarrelGame.prototype.addBarrel = function(x,y,channel) {
	var newBarrel = this.game.add.image(x,y,'atlas', 'barrel.png')
	newBarrel.inputEnabled = true
	newBarrel.input.useHandCursor = true
	newBarrel.channel = channel
	newBarrel.scale.setTo(ScreenProperties.gameScale, ScreenProperties.gameScale)

  gameGroup.add(newBarrel)

	newBarrel.events.onInputDown.add(function(){

    if(isAnimating || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }

		if(introCtr === 0){

      isAnimating = true

			if(currentBarrel === newBarrel){
				this.completedTurn()
			}else{
				this.failedTurn()
			}

			monkey.visible = true
		}
	}, this)

	barrels.push(newBarrel)

	return newBarrel
}

BarrelGame.prototype.completedTurn = function() {
	difficulty += 3

  this.game.sound.stopAll()

  var n = MathUtils.getRandomInt(1,3)

  if(GameData.showRabbitOnBarrelGame) {
    monkey.animations.play('success')
    monkey.alpha = 1
  }else {
    if(n === 1) {
      monkey.animations.play('calebrate-1')
    }else if(n === 2){
      monkey.animations.play('calebrate-2')
    }else {
      monkey.animations.play('calebrate-3')
    }

    n = MathUtils.getRandomInt(1,4)
    if(n === 1) {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.monkey1.play()
      }
    }else if(n === 2) {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.monkey2.play()
      }
    }else if(n === 3) {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.monkey3.play()
      }
    }else {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.monkey4.play()
      }
    }
  }

	winCount ++

	if( winCount === 5) {
		this.showBadgeAnimation()
	}else{
		this.waitToPlayAgain()
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voRightBarrel.play()
    }else {
      this.game.audioStore.fx.play('vo-well-done-right-barrel')
    }
	}
}

BarrelGame.prototype.showBadgeAnimation = function() {

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'complete', {'game_level_name':'barrels'});

  winGroup = this.game.add.group()

  GameData.disableHud = true

  var graphics = this.game.add.graphics(0,0)
  this.game.add.graphics(this.game.width/2, this.game.height/2)
  graphics.beginFill(0xc1d871)
  graphics.drawRect(0, 0, this.game.width, this.game.height)
  graphics.endFill()
  graphics.fixedToCamera = true
  graphics.alpha = 0
  this.game.add.tween(graphics).to({alpha: .9} , 800, Phaser.Easing.Linear.InOut, true)
  winGroup.add(graphics)

  if(this.game.sound.usingWebAudio) {
    this.game.audioStore.awardBadge.play()

    if(GameData.showRabbitOnBarrelGame) {
      this.game.audioStore.voRabbitBadge.play()
    }else {
      this.game.audioStore.voMonkeyBadge.play()
    }
  }else {
    if(GameData.showRabbitOnBarrelGame) {
      this.game.audioStore.fx.play('vo-rabbit-badge')
    }else {
      this.game.audioStore.fx.play('vo-monkey-badge')
    }
  }

  if(GameData.showRabbitOnBarrelGame) {
    badge = this.game.add.image(this.game.width/2, 0, 'rabbit-badge')
  }else {
    badge = this.game.add.image(this.game.width/2, 0, 'barrel-badge')
  }

  badge.anchor.setTo(.5,.5)
  badge.scale.setTo(.8 * ScreenProperties.gameScale)

  badge.y = -badge.height
  var tween1 = this.game.add.tween(badge).to({y:this.game.height/2} , 1000, Phaser.Easing.Back.InOut, false, 500)
  var tween3 = this.game.add.tween(badge).to({y: -badge.height}, 600, Phaser.Easing.Back.InOut, false, 1800)
  tween1.chain(tween3)

  tween1.start()

  this.game.add.tween(badge.scale).to({x: badge.scale.x * .3, y: badge.scale.y * .3} , 800, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(badge.scale).to({x: badge.scale.x * 1.4, y: badge.scale.y * 1.4} , 800, Phaser.Easing.Back.InOut, true, 1450, 0, true)

  var playButton = this.game.add.button(0, this.game.height/2, 'buttons', playAgain, this, 'replay_over_192.png', 'replay_off_192.png', 'replay_over_192.png', 'replay_over_192.png')
  playButton.scale.setTo(ScreenProperties.gameScale)
  playButton.anchor.setTo(.5)
  playButton.input.useHandCursor = true
  playButton.x -= playButton.width
  winGroup.add(playButton)

  var homeButton = this.game.add.button(this.game.width, this.game.height/2, 'atlas', handleHome, this, 'buttons/homebtn_over_192.png', 'buttons/homebtn_off_192.png', 'buttons/homebtn_over_192.png', 'buttons/homebtn_over_192.png')
  homeButton.scale.setTo(ScreenProperties.gameScale)
  homeButton.anchor.setTo(.5)
  homeButton.input.useHandCursor = true
  homeButton.x += homeButton.width

  homeButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voHome.play()
    }
  }, this)

  playButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voPlay.play()
    }
  }, this)

  this.game.add.tween(homeButton).to({x: this.game.width * .33} , 1000, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(playButton).to({x: this.game.width * .66} , 1000, Phaser.Easing.Back.InOut, true, 3100)

  winGroup.add(homeButton)
}

function playAgain() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'playagain', {'game_level_name':'barrels'});

  GameData.disableHud = false

  winGroup.destroy()
  winGroup = null

  isAnimating = true
  currentMonkeyIdx = 1
  winCount = 0
  difficulty = 0

  this.playGame()
}

function handleHome() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  GameData.disableHud = false

  this.game.sound.stopAll()
  this.game.state.start('lobby')
}

BarrelGame.prototype.failedTurn = function() {

  this.game.sound.stopAll()

  if(!GameData.showRabbitOnBarrelGame) {
    var soundId = MathUtils.getRandomInt(1,3)
    if(soundId === 1) {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.voIncorrectBarrel.play()
      }else {
        this.game.audioStore.fx.play('barrel-game-wrong-1')
      }
    } else if(soundId === 2) {
      if(this.game.sound.usingWebAudio) {
        this.game.audioStore.voIncorrectBarrel2.play()
      }else {
        this.game.audioStore.fx.play('barrel-game-wrong-2')
      }
    }
  }

  if(this.game.sound.usingWebAudio && !GameData.showRabbitOnBarrelGame) {
    this.game.audioStore.monkeyCry.play()
  }

  monkey.alpha = 1
  monkey.animations.play('cry')

	this.waitToPlayAgain()
}

BarrelGame.prototype.waitToPlayAgain = function() {

}

module.exports = BarrelGame
