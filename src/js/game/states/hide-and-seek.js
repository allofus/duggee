var AbstractState = require('./abstract-state')
	, ScreenProperties = require('../../lib/screen-properties')
	, HUD = require('../../lib/hud')
	, MathUtils = require('MathUtils')
  , Phaser = require('Phaser')
  , GameData = require('GameData')
  , properties = require('properties')

  , winCount = 0
  , previousIndex = -1

  , frog
  , emitter
  , winGroup
  , badge

  , nextMoveTimeout
  , group

  , gameStarted = false

  , locationList =
  [ {x:620, y:110}
  , {x:620, y:310}
  , {x:744, y:305}
  , {x:365, y:160}
  , {x:122, y:310}
  , {x:437, y:353} //
  ]

  //rock
  //cow
  //cow
  //teepee
  //sheep
  //straw
  , chickenLocationList =
  [ {x:620, y:110}
  , {x:620, y:330}
  , {x:744, y:305}
  , {x:365, y:160}
  , {x:130, y:315}
  , {x:437, y:353} //
  ]

var HideAndSeek = function (game) {
  this.game = game
  AbstractState.call(game)
}

HideAndSeek.prototype = Object.create(AbstractState.prototype)

HideAndSeek.prototype.shutdown = function() {
  this.game.sound.stopAll()
}

HideAndSeek.prototype.create = function() {

  var div = document.getElementById('og-game-holder')
    , w = div.offsetWidth * window.devicePixelRatio
    , h = div.clientHeight * window.devicePixelRatio

  this.game.scale.setGameSize((h > w) ? h : w, (h > w) ? w : h)
  ScreenProperties.calculateSize()

  this.name = 'hide-and-seek'

	this.game.world.setBounds(0, 0, this.game.width, this.game.height)
	this.game.camera.setBoundsToWorld()

	var graphics = this.game.add.graphics(0, 0)
	graphics.beginFill(0xb9d366)
	graphics.drawRect(0,0,this.game.width, this.game.height)
	graphics.endFill()
	graphics.fixedToCamera = true

  winCount = 0

  var img = this.game.add.image( 0, 0, 'treehouse')
  img.scale.setTo(ScreenProperties.gameScale)

  group = this.game.add.group()

  if(GameData.showChickenOnHideAndSeekGame) {
    addChicken(this.game)
  }else {
    addFrog(this.game)
  }

  frog.events.onInputDown.add(clickFrog, this)
  group.add(frog)

  //rock2
  img = this.game.add.image( 530 * ScreenProperties.gameScale, 100 * ScreenProperties.gameScale, 'duggee', 'hide-and-seek/rocks.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 190 * ScreenProperties.gameScale, 100 * ScreenProperties.gameScale, 'duggee', 'hide-and-seek/teepee.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 360 * ScreenProperties.gameScale, 200 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/barrel.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 400 * ScreenProperties.gameScale, 200 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/barrel.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 560 * ScreenProperties.gameScale, 320 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/cows.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 200 * ScreenProperties.gameScale, 320 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/hay.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)
  img = this.game.add.image( 280 * ScreenProperties.gameScale, 360 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/hay.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  img = this.game.add.image( 210 * ScreenProperties.gameScale, 430 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/mushroom2.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  img = this.game.add.image( 290 * ScreenProperties.gameScale, 440 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/toadstool.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  img = this.game.add.image( 600 * ScreenProperties.gameScale, 450 * ScreenProperties.gameScale, 'atlas', 'hide-and-seek/toadstool.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  img = this.game.add.image( 240 * ScreenProperties.gameScale, 240 * ScreenProperties.gameScale, 'duggee', 'hide-and-seek/pond.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  img = this.game.add.image( 125 * ScreenProperties.gameScale, 384 * ScreenProperties.gameScale, 'atlas', 'sheepshadow.png')
  img.scale.setTo(ScreenProperties.gameScale)
  group.add(img)

  var sheep = this.game.add.sprite( 260 * ScreenProperties.gameScale, 280 * ScreenProperties.gameScale, 'duggee', 'sheep_side/SHEEP_HOMEPAGE0026.png')
  sheep.animations.add('idle', Phaser.Animation.generateFrameNames('sheep_side/SHEEP_HOMEPAGE', 26, 39, '.png', 4), 25, true)
  sheep.animations.play('idle')
  sheep.scale.setTo(ScreenProperties.gameScale)
  sheep.scale.x *= -1
  group.add(sheep)

  img = this.game.add.image( 0, this.game.height, 'bush-2')
  img.scale.setTo(ScreenProperties.gameScale)
  img.y -= img.height

  img = this.game.add.image( this.game.width, this.game.height, 'bush-1')
  img.scale.setTo(ScreenProperties.gameScale)

  img.y -= img.height
  img.x -= img.width

  previousIndex = -1
  hideFrog(false)

  winCount = 0

  if(!GameData.hasFoundHideGame) {
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voFoundFindingGame.play()
    }else {
      this.game.audioStore.fx.play('vo-found-finding-game')
    }

    GameData.hasFoundHideGame = true
  }

  emitter = this.game.add.emitter(0, 0, 100)
  emitter.makeParticles('atlas', ['star2.png'])
  group.add(emitter)

  group.x = (this.game.width/2) - ((ScreenProperties.logicWidth * ScreenProperties.gameScale) /2 )
  //setup hud
  new HUD(this.game, true, false, true)

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'selected', {'game_level_name':'hide'});
}

function addFrog(game) {
  frog = game.add.sprite(505 * ScreenProperties.gameScale, 190 * ScreenProperties.gameScale,'atlas', 'FROG_IDLE.png')
  frog.scale.setTo(ScreenProperties.gameScale * .5)
  frog.anchor.setTo(.5)
  frog.inputEnabled = true
  frog.input.useHandCursor = true
  frog.animations.add('idle', ['FROG_IDLE.png'], 25, true)
  frog.animations.add('blink', Phaser.Animation.generateFrameNames('FROG_IDLEBLINK_', 1, 5, '.png', 4), 25)
  frog.animations.play('idle')

  frog.animations.getAnimation('idle').onLoop.add(function() {
    if(frog.animations.currentAnim.loopCount >= 60) {
      frog.animations.play('blink')
    }
  })

  var shouldRibbit = true
  frog.animations.getAnimation('blink').onComplete.add(function() {
    frog.animations.play('idle')

    if(game.sound.usingWebAudio) {
      game.audioStore.ribbit.play()
    }else {
      game.audioStore.fx.play('ribbit')
    }

    shouldRibbit = !shouldRibbit
  }, this)
}

function addChicken(game) {
  frog = game.add.sprite(505 * ScreenProperties.gameScale, 190 * ScreenProperties.gameScale,'duggee', 'CHICKEN1_NEUTRALLEFT/CHICKEN_NEUTRALLEFT.png')
  frog.scale.setTo(ScreenProperties.gameScale * .5)
  frog.anchor.setTo(.5)
  frog.inputEnabled = true
  frog.input.useHandCursor = true
}

function hideFrog(showParticles) {

  clearTimeout(nextMoveTimeout)

  if(showParticles) {
    emitParticles()

    if(!gameStarted) {
      gameStarted = true
      //TOTO ECHO STATS
      //gamesGrid.iStatsActionEvent ("game_level", 'started', {'game_level_name':'hide'});
    }
  }

  var locationIdx = previousIndex
	//remove the last chosen location from the list of locations
  while(locationIdx === previousIndex) {
    locationIdx = MathUtils.getRandomInt(0, locationList.length-1)
  }

	//save the chosen previous index so we do not choose it again next time.
	previousIndex = locationIdx

  if(GameData.showChickenOnHideAndSeekGame) {
    //set the frog position
    frog.x = chickenLocationList[locationIdx].x * ScreenProperties.gameScale
    frog.y = chickenLocationList[locationIdx].y * ScreenProperties.gameScale
  }else {
    //set the frog position
    frog.x = locationList[locationIdx].x * ScreenProperties.gameScale
    frog.y = locationList[locationIdx].y * ScreenProperties.gameScale
  }

  nextMoveTimeout = setTimeout(hideFrog, 3000)
}

function emitParticles() {
  emitter.x = frog.x
  emitter.y = frog.y

  emitter.setScale(.2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, .2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, 800)
  emitter.setAlpha(1, 0, 1200)

  emitter.particleDrag.x = 400 * ScreenProperties.gameScale
  emitter.particleDrag.y = 400 * ScreenProperties.gameScale

  emitter.setXSpeed(-300 * ScreenProperties.gameScale, 300 * ScreenProperties.gameScale)
  emitter.setYSpeed(-300 * ScreenProperties.gameScale, 300 * ScreenProperties.gameScale)

  emitter.start(true, 2000, null, 14)
}

function clickFrog() {

  if(GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
    return
  }

  clearTimeout(nextMoveTimeout)

	hideFrog(true)

	winCount ++

	if(winCount === 10) {
		this.showBadgeAnimation()
    frog.inputEnabled = false
	}

  if(this.game.sound.usingWebAudio) {
    switch(MathUtils.getRandomInt(1,5)) {
      case 1:
      this.game.audioStore.pop1.play()
      break

      case 2:
      this.game.audioStore.pop2.play()
      break

      case 3:
      this.game.audioStore.pop3.play()
      break

      case 4:
      this.game.audioStore.pop4.play()
      break

      case 5:
      this.game.audioStore.pop5.play()
      break
    }
  }else {
    switch(MathUtils.getRandomInt(1,5)) {
      case 1:
      this.game.audioStore.fx.play('pop-1')
      break

      case 2:
      this.game.audioStore.fx.play('pop-2')
      break

      case 3:
      this.game.audioStore.fx.play('pop-3')
      break

      case 4:
      this.game.audioStore.fx.play('pop-4')
      break

      case 5:
      this.game.audioStore.fx.play('pop-5')
      break
    }
  }
}

HideAndSeek.prototype.destroy = function() {
  clearTimeout(nextMoveTimeout)
}

HideAndSeek.prototype.showBadgeAnimation = function() {

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'complete', {'game_level_name':'hide'});

  GameData.disableHud = true

  winGroup = this.game.add.group()

  var graphics = this.game.add.graphics(0,0)
  this.game.add.graphics(this.game.width/2, this.game.height/2)
  graphics.beginFill(0xc1d871)
  graphics.drawRect(0, 0, this.game.width, this.game.height)
  graphics.endFill()
  graphics.alpha = 0
  this.game.add.tween(graphics).to({alpha: .9} , 800, Phaser.Easing.Linear.InOut, true)

  winGroup.add(graphics)

  if(GameData.showChickenOnHideAndSeekGame) {
    badge = this.game.add.image(this.game.width/2, 0,'chicken-badge')
  }else {
    badge = this.game.add.image(this.game.width/2, 0, 'hide-badge')
  }

  badge.anchor.setTo(.5,.5)
  badge.scale.setTo(.8 * ScreenProperties.gameScale)

  badge.y = -badge.height
  var tween1 = this.game.add.tween(badge).to({y:this.game.height/2} , 1000, Phaser.Easing.Back.InOut, false, 500)
  var tween3 = this.game.add.tween(badge).to({y: -badge.height}, 600, Phaser.Easing.Back.InOut, false, 1800)
  tween1.chain(tween3)

  tween1.start()

  this.game.add.tween(badge.scale).to({x: badge.scale.x * .3, y: badge.scale.y * .3} , 800, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(badge.scale).to({x: badge.scale.x * 1.4, y: badge.scale.y * 1.4} , 800, Phaser.Easing.Back.InOut, true, 1450, 0, true)

  var playButton = this.game.add.button(0, this.game.height/2, 'buttons', playAgain, this, 'replay_over_192.png', 'replay_off_192.png', 'replay_over_192.png', 'replay_over_192.png')
  playButton.scale.setTo(ScreenProperties.gameScale)
  playButton.anchor.setTo(.5)
  playButton.input.useHandCursor = true
  playButton.x -= playButton.width
  winGroup.add(playButton)

  var homeButton = this.game.add.button(this.game.width, this.game.height/2, 'atlas', handleHome, this, 'buttons/homebtn_over_192.png', 'buttons/homebtn_off_192.png', 'buttons/homebtn_over_192.png', 'buttons/homebtn_over_192.png')
  homeButton.scale.setTo(ScreenProperties.gameScale)
  homeButton.anchor.setTo(.5)
  homeButton.input.useHandCursor = true
  homeButton.x += homeButton.width
  winGroup.add(homeButton)

  this.game.add.tween(playButton).to({x: this.game.width * .66} , 1000, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(homeButton).to({x: this.game.width * .33} , 1000, Phaser.Easing.Back.InOut, true, 3100)

   if(this.game.sound.usingWebAudio) {
    this.game.audioStore.awardBadge.play()

    if(GameData.showChickenOnHideAndSeekGame) {
      this.game.audioStore.voChickenBadge.play()
    }else {
      this.game.audioStore.voFrogBadge.play()
    }
  }else {
    if(GameData.showChickenOnHideAndSeekGame) {
      this.game.audioStore.fx.play('vo-chicken-badge')
    }else {
      this.game.audioStore.fx.play('vo-frog-badge')
    }
  }

  homeButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voHome.play()
    }
  }, this)

  playButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voPlay.play()
    }
  }, this)
}

function playAgain() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'playagain', {'game_level_name':'hide'});

  GameData.disableHud = false

  winGroup.destroy()
  winGroup = null

  previousIndex = -1
  winCount = 0
  frog.inputEnabled = true
  hideFrog(false)
}

function handleHome() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  GameData.disableHud = false

  this.game.sound.stopAll()
  this.game.state.start('lobby')
}

module.exports = HideAndSeek
