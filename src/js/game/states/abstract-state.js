var Phaser = require('Phaser')
  , AbstractState = function(game) {
      this.game = game
    }


AbstractState.prototype = Object.create(Phaser.State.prototype)

AbstractState.prototype.create = function() {

}

AbstractState.prototype.init = function() {
	//you can call this method after all entities have been instantiated. Useful when referencing other level entities that require linkage.
}

AbstractState.prototype.update = function() {

}

module.exports = AbstractState
