var Stats = require('Stats')
  , properties = require('properties')
  , ScreenProperties = require('../../lib/screen-properties')
  , Phaser = require('Phaser')
  , RotateOverlay = require('../../lib/rotate-overlay')
  , boot = {}

  , rO

boot.create = function () {

  this.game.load.crossOrigin = 'anonymous'
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
  this.game.input.maxPointers = 1

  properties.buttonSize *= window.devicePixelRatio

  this.scale.pageAlignHorizontally = true
  this.scale.pageAlignVertically = true

  if (properties.showStats) {
    addStats()
  }

  rO = new RotateOverlay(this.game)

  //if the game is in the wrong aspect ratio...
  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
    rO.open()
    this.game.scale.onOrientationChange.addOnce(this.continueGame, this)
  }else {
    this.continueGame()
  }
}

boot.continueGame = function() {
  //set the size of the game
  rO.destroy()
  ScreenProperties.calculateSize()
  this.game.state.start('preloader')
}

boot.preload = function () {
  this.game.load.crossOrigin = 'anonymous'
  this.game.load.image('preloader', properties.cdnAddress+'images/'+ScreenProperties.screen+'/preloader.jpg')
  this.game.load.image('preloader-bar', properties.cdnAddress+'images/'+ScreenProperties.screen+'/preloader-bar.png')
  this.game.load.image('c-beebies-logo', properties.cdnAddress+'images/'+ScreenProperties.screen+'/c-beebies-logo.png')
  this.game.load.bitmapFont('Berlin-Sans-40pt', properties.cdnAddress+'images/'+ScreenProperties.screen+'/font.png', properties.cdnAddress+'images/'+ScreenProperties.screen+'/font.xml')
}

function addStats() {
  var stats = new Stats()

  stats.setMode(0)

  stats.domElement.style.position = 'absolute'
  stats.domElement.style.left = '0px'
  stats.domElement.style.top = '0px'

  document.body.appendChild(stats.domElement)

  setInterval(function () {
    stats.begin()
    stats.end()
  }, 1000 / 60)
}

module.exports = boot
