var AbstractState = require('./abstract-state')
	, ScreenProperties = require('../../lib/screen-properties')
	, HUD = require('../../lib/hud')
	, Duggee = require('../entities/idle-characters/duggee')
	, Phaser = require('Phaser')
  , properties = require('properties')
  , _ = require('lodash')
  , GameData = require('GameData')

	, playButton

  , hud

var MainMenu = function (game) {
  this.game = game
  AbstractState.call(game)
}

MainMenu.prototype = Object.create(AbstractState.prototype)

MainMenu.prototype.create = function() {

  var graphics = this.game.add.graphics(0, 0);

  // set a fill and line style
  graphics.beginFill(0xb7edff)
  graphics.drawRect(0,0,this.game.width,this.game.height)
  graphics.endFill()

  this.name = 'main menu'

  var floor = this.game.add.graphics(0, 480 * ScreenProperties.gameScale)
  floor.beginFill(0xb9d366)
  floor.drawRect(0, 0, this.game.width, this.game.height -floor.y)
  floor.endFill()

  var bgHouse = this.game.add.image(0, floor.y - (this.game.cache.getImage('house-main-menu').height * ScreenProperties.gameScale), 'house-main-menu')
  bgHouse.scale.setTo(ScreenProperties.gameScale)

  var bgCloud = this.game.add.image(this.game.width, 20 * ScreenProperties.gameScale, 'cloud-main-menu')
  bgCloud.scale.setTo(ScreenProperties.gameScale)
  bgCloud.x -= bgCloud.width

  var logo = this.game.add.image(this.game.width/2, 0, 'logo')
  logo.anchor.setTo(.5)
  logo.y -= logo.height/2
  logo.scale.setTo(ScreenProperties.gameScale)
	this.game.add.tween(logo).to( { y:(100 * ScreenProperties.gameScale) }, 800, Phaser.Easing.Back.InOut, true, 100)

  var cLogo = this.game.add.image(this.game.width/2, 455 * ScreenProperties.gameScale, 'c-beebies-logo')
  cLogo.scale.setTo(.8 * ScreenProperties.gameScale)
  cLogo.anchor.setTo(.5)

  playButton = this.game.add.button(this.game.width/2, this.game.height, 'atlas', openLobby, this, 'buttons/play-hover.png', 'buttons/play.png', 'buttons/play-hover.png', 'buttons/play-hover.png')
  playButton.scale.setTo(ScreenProperties.gameScale)
	playButton.anchor.setTo(.5)
	playButton.input.useHandCursor = true
	playButton.y += playButton.height/2

  playButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voPlay.play()
    }
  }, this)

	this.game.add.tween(playButton).to( { y:(290 * ScreenProperties.gameScale) }, 800, Phaser.Easing.Back.InOut, true, 200)

	new Duggee(this.game, this.game.width * .84, 330 * ScreenProperties.gameScale, true)
	new HUD(this.game, false, false, false, true)

  if(!this.game.device.desktop) {
    var homeButton = this.game.add.button(properties.margin, properties.safeAreaPadding, 'atlas', handleHome, this, 'buttons/Exit1_off_48.png', 'buttons/Exit1_over_48.png', 'buttons/Exit1_off_48.png', 'buttons/Exit1_off_48.png')
    homeButton.input.useHandCursor = true
    homeButton.scale.setTo(window.devicePixelRatio)
  }


  if(this.game.sound.usingWebAudio) {
    this.game.audioStore.music.play()
  }

  if(this.game.device.desktop) {
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voTitle.play()
    }else {
      this.game.audioStore.fx.play('vo-title')
    }
  }

  this.input.onDown.add(function() {
    console.log('burp')
  });
}

MainMenu.prototype.shutdown = function() {
  this.game.sound.stopAll()
}

function handleHome() {
  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
    return
  }

  if(GameData.isFirstClick) {
    //TOTO ECHO STATS
    //gamesGrid.iStatsActionEvent ("game_first_click", 'exit', {'game_level':'start'});
    GameData.isFirstClick = false
  }

  //go to gome
  window.location.href = properties.exitButtonURL
}

function openLobby() {
  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
    return
  }

  if(GameData.isFirstClick) {
    //TOTO ECHO STATS
    //gamesGrid.iStatsActionEvent ("game_first_click", 'play', {'game_level':'start'});
    GameData.isFirstClick = false
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_click", 'play', {'game_level':'start'});

	playButton.inputEnabled = false
	this.game.state.start('lobby')
}

module.exports = MainMenu
