var AbstractState = require('./abstract-state')
  , GameData = require('GameData')
  , Phaser = require('Phaser')
  , MathUtils = require('MathUtils')

  , Roli = require('../entities/idle-characters/roli')
  , Duggee = require('../entities/idle-characters/duggee')
  , Tag = require('../entities/idle-characters/tag')
  , Norrie = require('../entities/idle-characters/norrie')
  , Betty = require('../entities/idle-characters/betty')
  , Happy = require('../entities/idle-characters/happy')
  , Tag = require('../entities/idle-characters/tag')

  , Spider = require('../entities/behaviours/spider')
  , HUD = require('../../lib/hud')
  , ScreenProperties = require('../../lib/screen-properties')
  , properties = require('properties')

  , monkey
  , lobbyGroup

var Lobby = function (game) {
  this.game = game
  AbstractState.call(game)
}

Lobby.prototype = Object.create(AbstractState.prototype)

Lobby.prototype.create = function() {

  var div = document.getElementById('og-game-holder')
    , w = div.offsetWidth * window.devicePixelRatio
    , h = div.clientHeight * window.devicePixelRatio

  this.game.scale.setGameSize((h > w) ? h : w, (h > w) ? w : h)
  ScreenProperties.calculateSize()

  this.name = 'lobby'

  //decide on what mini game versions to use
  GameData.showRabbitOnBarrelGame = MathUtils.getRandomBoolean()
  GameData.showChickenOnHideAndSeekGame = MathUtils.getRandomBoolean()

	//setup camera
	createBackground(this.game)

  lobbyGroup = this.game.add.group()

  var wallScratch2 = this.game.add.image(20 * ScreenProperties.gameScale, 120 * ScreenProperties.gameScale, 'atlas2', 'scratches/WALL_TEXTURE_E.png')
  wallScratch2.scale.setTo(ScreenProperties.gameScale)
  lobbyGroup.add(wallScratch2)

  wallScratch2 = this.game.add.image(140 * ScreenProperties.gameScale, 170 * ScreenProperties.gameScale, 'atlas2', 'scratches/WALL_TEXTURE_F.png')
  wallScratch2.scale.setTo(ScreenProperties.gameScale)
  lobbyGroup.add(wallScratch2)

	//add window
	var window1 = this.game.add.image(280 * ScreenProperties.gameScale, 90 * ScreenProperties.gameScale, 'duggee', 'KITCHEN_WINDOW.png')
	window1.scale.setTo(ScreenProperties.gameScale)
  lobbyGroup.add(window1)

  //add barrel game button
  var barrelGameButton

  if(GameData.showRabbitOnBarrelGame) {
    barrelGameButton = addRabbit(this.game)
  }else {
    barrelGameButton = addMonkey(this.game)
  }

  barrelGameButton.scale.setTo(ScreenProperties.gameScale)
  barrelGameButton.inputEnabled = true
  barrelGameButton.input.useHandCursor = true
  barrelGameButton.events.onInputDown.add(function(){
    if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }
    barrelGameButton.inputEnabled = false
    this.game.state.start('barrelGame')
  }, this)

  lobbyGroup.add(barrelGameButton)

  var hideAndSeekGameButton

  if(GameData.showChickenOnHideAndSeekGame) {
    hideAndSeekGameButton = addChicken(this.game)
  }else {
    hideAndSeekGameButton = addFrog(this.game)
  }

  lobbyGroup.add(hideAndSeekGameButton)
  hideAndSeekGameButton.inputEnabled = true
  hideAndSeekGameButton.input.useHandCursor = true
  hideAndSeekGameButton.events.onInputDown.add(function(){

    if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }

    hideAndSeekGameButton.inputEnabled = false

    this.game.state.start('hideAndSeek')
  }, this)

  var bounceTween = this.game.add.tween(hideAndSeekGameButton).to( { y:hideAndSeekGameButton.y + (20 * ScreenProperties.gameScale)}, 1000, Phaser.Easing.Linear.In, true, 0, Infinity, true)
  bounceTween.onLoop.add(function() {
    hideAndSeekGameButton.emitter.explode(3000, 5)
  })

  var flourTin = this.game.add.image(490 * ScreenProperties.gameScale, 230 * ScreenProperties.gameScale,'atlas', 'FLOUR_TIN.png')
  flourTin.scale.setTo(ScreenProperties.gameScale,ScreenProperties.gameScale)
  lobbyGroup.add(flourTin)
  flourTin.inputEnabled = true
  flourTin.input.useHandCursor = true
  flourTin.events.onInputDown.add(function(){

    if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }

    flourTin.inputEnabled = false

    this.game.state.start('hideAndSeek')
  }, this)

  var draws1 = this.game.add.image(470 * ScreenProperties.gameScale, 307 * ScreenProperties.gameScale,'atlas', 'BBC_IMMERSIVE_SITE_v001_DRAWERS.png')
  draws1.scale.setTo(ScreenProperties.gameScale,ScreenProperties.gameScale)
  lobbyGroup.add(draws1)

  var monkeyMask = this.game.add.graphics(315 * ScreenProperties.gameScale, 62 * ScreenProperties.gameScale)
  monkeyMask.beginFill(0xFF3300)
  monkeyMask.drawRect(0, 0, draws1.width, 192 * ScreenProperties.gameScale)
  monkeyMask.endFill()
  barrelGameButton.mask = monkeyMask

  lobbyGroup.add(monkeyMask)

	var sheep = this.game.add.sprite(285 * ScreenProperties.gameScale, 210 * ScreenProperties.gameScale, 'duggee', 'sheep_side/SHEEP_HOMEPAGE0026.png')
  sheep.animations.add('idle', Phaser.Animation.generateFrameNames('sheep_side/SHEEP_HOMEPAGE', 26, 39, '.png', 4), 25, true)
  sheep.animations.add('bleat', Phaser.Animation.generateFrameNames('sheep_side/SHEEP_HOMEPAGE', 10, 20, '.png', 4), 25, true)
  sheep.animations.play('idle')
	sheep.scale.setTo(ScreenProperties.gameScale * 1.5)
  sheep.scale.x *= -1
	sheep.inputEnabled = true
	sheep.input.useHandCursor = true
	sheep.events.onInputDown.add(function(){
    if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }
		sheep.inputEnabled = false

		this.game.state.start('keyboard')
	}, this)

  lobbyGroup.add(sheep)

  var loopLimit = 12

  sheep.animations.currentAnim.onLoop.add(function(){
    if(sheep.animations.currentAnim.loopCount >= loopLimit) {
      sheep.animations.play('bleat')
      loopLimit = 16
      if(MathUtils.getRandomBoolean()) {
        if(this.game.sound.usingWebAudio) {
          this.game.audioStore.a.play()
        }else if(!this.game.audioStore.fx.isPlaying ){
          this.game.audioStore.fx.play('a')
        }
      }else {
        if(this.game.sound.usingWebAudio) {
          this.game.audioStore.bb.play()
        }else if(!this.game.audioStore.fx.isPlaying ){
          this.game.audioStore.fx.play('bb')
        }
      }

      sheep.animations.currentAnim.onLoop.addOnce(function() {
        sheep.animations.play('idle')
      })

      sheep.emitter.explode(3000, 5)
    }
  }, this)

	//add clock
	var clock = this.game.add.image(540 * ScreenProperties.gameScale, 50 * ScreenProperties.gameScale, 'atlas', 'clock.png')
	clock.scale.setTo(ScreenProperties.gameScale, ScreenProperties.gameScale)
  lobbyGroup.add(clock)

  lobbyGroup.add(new Duggee(this.game, 690 * ScreenProperties.gameScale, 240 * ScreenProperties.gameScale))

  addCharacters(this.game)

	//add spider
	var spider = new Spider(this.game, 30 * ScreenProperties.gameScale, 0)
	spider.scale.setTo(ScreenProperties.gameScale, ScreenProperties.gameScale)
	spider.y = -(spider.height + (100 * ScreenProperties.gameScale))
  lobbyGroup.add(spider)

  //add sparkles to buttons
  sheep.emitter = addEmitterToEntity(this.game, (sheep.x + sheep.width/2) + (ScreenProperties.gameScale * 10), sheep.y + (ScreenProperties.gameScale * 10))
  barrelGameButton.emitter = addEmitterToEntity(this.game, barrelGameButton.x + barrelGameButton.width/2, barrelGameButton.y + barrelGameButton.height/2)
  hideAndSeekGameButton.emitter = addEmitterToEntity(this.game, hideAndSeekGameButton.x + hideAndSeekGameButton.width/2, hideAndSeekGameButton.y + hideAndSeekGameButton.height/2)

  //setup hud
  new HUD(this.game, false, false, true, false)

  var homeButton = this.game.add.button(properties.margin, properties.safeAreaPadding, 'atlas', function() {
    if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }

    this.game.state.start('mainMenu')
  }, this, 'buttons/Back_off_48.png', 'buttons/Back_over_48.png', 'buttons/Back_over_48.png', 'buttons/Back_over_48.png')
  homeButton.input.useHandCursor = true
  homeButton.scale.setTo(window.devicePixelRatio)

  homeButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voBack.play()
    }
  }, this)

  lobbyGroup.x = (this.game.width/2) - ((ScreenProperties.logicWidth * ScreenProperties.gameScale) /2 )

  if(GameData.isFirstLobbyOpen) {
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voBusySquirrelClub.play()
    }
  }

  GameData.isFirstLobbyOpen = false
}

Lobby.prototype.shutdown = function() {
  this.game.sound.stopAll()
}

function addChicken(game) {
  var chicken = game.add.sprite(477 * ScreenProperties.gameScale, 180 * ScreenProperties.gameScale,'duggee', 'CHICKEN1_NEUTRALLEFT/CHICKEN_NEUTRALLEFT.png')
  chicken.scale.setTo(ScreenProperties.gameScale * .5)

  return chicken
}

function addMonkey(game) {
  //add monkey
  monkey = game.add.sprite(315 * ScreenProperties.gameScale, 110 * ScreenProperties.gameScale, 'atlas2', 'barrel-game/MONKEY_0001.png')
  monkey.animations.add('idle', ['barrel-game/MONKEY_0001.png'], 25, true)
  monkey.animations.add('blink', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 2, 7, '.png', 4), 25, false)
  monkey.animations.add('window-jump', Phaser.Animation.generateFrameNames('barrel-game/MONKEY_', 8, 37, '.png', 4), 25, false)

  monkey.animations.getAnimation('window-jump').onComplete.add(function() {
    monkey.animations.play('idle')
  })

  monkey.animations.getAnimation('blink').onComplete.add(function() {
    monkey.animations.play('idle')
  })

  var monkeyBlinkCtr = 200
  var shouldBlink = false
  monkey.animations.getAnimation('idle').onLoop.add(function() {
    if(monkey.animations.currentAnim.loopCount >= monkeyBlinkCtr ) {
      monkeyBlinkCtr = 180
      if(shouldBlink) {
        monkey.animations.play('blink')
      }else {
        monkey.animations.play('window-jump')
        monkey.emitter.explode(3000, 5)

        var n = MathUtils.getRandomInt(1,4)
        if(n === 1) {
          if(game.sound.usingWebAudio) {
            game.audioStore.monkey1.play()
          }else if(!game.audioStore.fx.isPlaying ){
            game.audioStore.fx.play('monkey-1')
          }
        }else if(n === 2) {
          if(game.sound.usingWebAudio) {
            game.audioStore.monkey2.play()
          }else if(!game.audioStore.fx.isPlaying ){
            game.audioStore.fx.play('monkey-2')
          }
        }else if(n === 3) {
          if(game.sound.usingWebAudio) {
            game.audioStore.monkey3.play()
          }else if(!game.audioStore.fx.isPlaying ){
            game.audioStore.fx.play('monkey-3')
          }
        }else {
          if(game.sound.usingWebAudio) {
            game.audioStore.monkey4.play()
          }else if(!game.audioStore.fx.isPlaying ){
            game.audioStore.fx.play('monkey-4')
          }
        }
      }

      shouldBlink = !shouldBlink
    }
  }, this)

  monkey.animations.play('idle')

  return monkey
}

function addFrog(game) {
  //add frog
  var frog = game.add.sprite(495 * ScreenProperties.gameScale, 140 * ScreenProperties.gameScale, 'atlas', 'FROG_IDLE.png')
  frog.animations.add('idle', ['FROG_IDLE.png'], 25, true)
  frog.animations.add('blink', Phaser.Animation.generateFrameNames('FROG_IDLEBLINK_', 1, 5, '.png', 4), 25)
  frog.animations.play('idle')
  frog.scale.setTo(ScreenProperties.gameScale * .5)
  var loopCount = 280
  frog.animations.getAnimation('idle').onLoop.add(function() {
    if(frog.animations.currentAnim.loopCount >= loopCount) {
      frog.animations.play('blink')

      loopCount = 260

      if(game.sound.usingWebAudio) {
        game.audioStore.ribbit.play()
      }else if(!game.audioStore.fx.isPlaying ){
        game.audioStore.fx.play('ribbit')
      }
    }
  }, this)

  frog.animations.getAnimation('blink').onComplete.add(function() {
    frog.animations.play('idle')
  }, this)

  return frog
}

function addRabbit(game) {
  var rabbit = game.add.sprite(330 * ScreenProperties.gameScale, 80 * ScreenProperties.gameScale, 'atlas2', 'RABBIT_BARRELGAME/Rabbit_master0035.png')
  rabbit.animations.add('idle', ['RABBIT_BARRELGAME/Rabbit_master0035.png'], 25, true)

  rabbit.animations.add('success', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 1, 47, '.png', 4), 25, false)
  rabbit.animations.add('window-jump-in', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 48, 56, '.png', 4), 25, false)
  rabbit.animations.add('window-jump-cycle', ['RABBIT_BARRELGAME/Rabbit_master0057.png'], 25, true)
  rabbit.animations.add('window-jump-end', Phaser.Animation.generateFrameNames('RABBIT_BARRELGAME/Rabbit_master', 58, 71, '.png', 4), 25, false)
  rabbit.animations.add('window-pause', ['RABBIT_BARRELGAME/Rabbit_master0057.png'], 25, true)

  rabbit.animations.getAnimation('window-jump-in').onComplete.add(function() {
    rabbit.animations.play('window-jump-cycle')
  }, this)

  var loopCount = 20
  rabbit.alpha = 0

  rabbit.animations.getAnimation('window-pause').onLoop.add(function() {
    if(rabbit.animations.currentAnim.loopCount >= loopCount) {
      loopCount = 40
      rabbit.animations.play('window-jump-in')
      rabbit.alpha = 1
      rabbit.emitter.explode(3000, 5)
    }
  }, this)

  rabbit.animations.getAnimation('window-jump-end').onComplete.add(function() {
    rabbit.animations.play('window-pause')
    rabbit.alpha = 0
  }, this)

  rabbit.animations.getAnimation('window-jump-cycle').onLoop.add(function() {
    if(rabbit.animations.currentAnim.loopCount >= 120) {
      rabbit.animations.play('window-jump-end')
    }

    if(rabbit.animations.currentAnim.loopCount === 90) {
      rabbit.emitter.explode(3000, 5)
    }
  }, this)

  rabbit.scale.setTo(ScreenProperties.gameScale)
  rabbit.animations.play('window-pause')

  return rabbit
}

function addEmitterToEntity(game, x, y) {
  var emitter1 = game.add.emitter(0, 0, 5)
  emitter1.makeParticles('atlas', ['star2.png'])
  emitter1.x = x
  emitter1.y = y
  emitter1.setScale(.2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, .2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, 800)
  emitter1.setAlpha(.8, 0, 1800)
  emitter1.particleDrag.x = 400 * ScreenProperties.gameScale
  emitter1.particleDrag.y = 400 * ScreenProperties.gameScale
  emitter1.setXSpeed(-200 * ScreenProperties.gameScale, 200 * ScreenProperties.gameScale)
  emitter1.setYSpeed(-200 * ScreenProperties.gameScale, 200 * ScreenProperties.gameScale)

  lobbyGroup.add(emitter1)

  return emitter1
}

function addCharacters(game) {

  //push 3 random numbers
  var charIds = [1,2,3,4,5]

  var chosenIds = []

  chosenIds.push(charIds.splice(MathUtils.getRandomInt(1,charIds.length-1),1)[0])
  chosenIds.push(charIds.splice(MathUtils.getRandomInt(0,charIds.length-1),1)[0])
  chosenIds.push(charIds.splice(MathUtils.getRandomInt(0,charIds.length-1),1)[0])

  while(chosenIds.length >= 1) {

    var newChar

    if(chosenIds[0] === 1) {
      newChar = new Happy(game, 400 * ScreenProperties.gameScale, 400 * ScreenProperties.gameScale)
    }

    if(chosenIds[0] === 2) {
      //we need to add the cup image.
      newChar = new Norrie(game, 0 * ScreenProperties.gameScale, 320 * ScreenProperties.gameScale)
    }

    if(chosenIds[0] === 3) {
      newChar = new Betty(game, 660 * ScreenProperties.gameScale, 440 * ScreenProperties.gameScale)
    }

    if(chosenIds[0] === 4) {
      newChar = new Roli(game, 130 * ScreenProperties.gameScale, 220 * ScreenProperties.gameScale)
    }

    if(chosenIds[0] === 5) {
      newChar = new Tag(game, 450 * ScreenProperties.gameScale, 250 * ScreenProperties.gameScale)
    }

    newChar.anchor.setTo(.5,1)
    newChar.y = 475 * ScreenProperties.gameScale

    if(chosenIds.length === 3) {
      newChar.x = 140 * ScreenProperties.gameScale
    }

    if(chosenIds.length === 2) {
      newChar.x = 350 * ScreenProperties.gameScale
      newChar.y = 450 * ScreenProperties.gameScale
    }

    if(chosenIds.length === 1) {
      newChar.x = 560 * ScreenProperties.gameScale
    }

    //add the cup image
    if(chosenIds[0] === 2) {

      newChar.x += 20 * ScreenProperties.gameScale

      var teapot = game.add.image(newChar.x - (150 * ScreenProperties.gameScale), newChar.y - (105 * ScreenProperties.gameScale), 'atlas', 'norrie/NORRIE_FINALCOMPILE_0001.png')
      teapot.scale.setTo(ScreenProperties.gameScale)
    }

    chosenIds.splice(0,1)
  }
}

function createBackgroundTile(game,x) {
	var tile = game.add.sprite(x,0,'background')
	tile.scale.setTo(ScreenProperties.gameScale)

	tile.inputEnabled = true
	tile.input.useHandCursor = true
	tile.ignoreAutoTab = true

  if(tile.width < game.width) {
    tile = game.add.sprite(tile.width-2, 0,'background')
    tile.scale.setTo(ScreenProperties.gameScale)

    tile.inputEnabled = true
    tile.input.useHandCursor = true
    tile.ignoreAutoTab = true
  }

	return tile
}

function createBackground(game) {
	createBackgroundTile(game, 0)
}

module.exports = Lobby
