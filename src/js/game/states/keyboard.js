var AbstractState = require('./abstract-state')
	, ScreenProperties = require('../../lib/screen-properties')
	, MusicalSheep = require('../entities/keyboard/musical-sheep')
	, HUD = require('../../lib/hud')
  , Phaser = require('Phaser')
  , properties = require('properties')
  , GameData = require('GameData')

  , buttonList = null

  , gameStarted = false

  , winCount = 0

  , sheepGroup
  , winGroup
  , badge

var Keyboard = function (game) {
  this.game = game
  AbstractState.call(game)
}

Keyboard.prototype = Object.create(AbstractState.prototype)

Keyboard.prototype.create = function() {

  var div = document.getElementById('og-game-holder')
    , w = div.offsetWidth * window.devicePixelRatio
    , h = div.clientHeight * window.devicePixelRatio

  this.game.scale.setGameSize((h > w) ? h : w, (h > w) ? w : h)
  ScreenProperties.calculateSize()

	this.name = 'sheep-game'

  var graphics = this.game.add.graphics(0, 0);
  graphics.beginFill(0xb7edff)
  graphics.drawRect(0,0,this.game.width,this.game.height)
  graphics.endFill()

  var floor = this.game.add.graphics(0, 350 * ScreenProperties.gameScale)
  floor.beginFill(0xb9d366)
  floor.drawRect(0, 0, this.game.width, this.game.height -floor.y)
  floor.endFill()

	var bgCloud = this.game.add.image(this.game.width, 20 * ScreenProperties.gameScale, 'cloud-main-menu')
  bgCloud.scale.setTo(ScreenProperties.gameScale)
  bgCloud.x -= bgCloud.width + (350 * ScreenProperties.gameScale)

  var img = this.game.add.image( this.game.width, this.game.height, 'atlas', 'hide-and-seek/flowers2.png')
  img.scale.setTo(ScreenProperties.gameScale)
  img.x-=img.width
  img.y-=img.height

  img = this.game.add.image( 0, this.game.height, 'atlas', 'hide-and-seek/flowers1.png')
  img.scale.setTo(ScreenProperties.gameScale)
  img.y-=img.height

  var treeLeft = this.game.add.image( 0, 0, 'sheep-tree-left')
  treeLeft.scale.setTo(ScreenProperties.gameScale)

  var treeRight = this.game.add.image( this.game.width, 0, 'sheep-tree')
  treeRight.scale.setTo(ScreenProperties.gameScale)
  treeRight.x -= treeRight.width

  sheepGroup = this.game.add.group()

	buttonList = []

  //levelParser.parse(this.game, LevelData)
	this.createSheepButton('a')
	this.createSheepButton('b')
	this.createSheepButton('bb')
	this.createSheepButton('c')
	this.createSheepButton('d')

	buttonList.x = (this.game.width/2)-500
	buttonList.y = 400 * ScreenProperties.gameScale

  //setup hud
  new HUD(this.game, true, false, true)

  winCount = 0

  sheepGroup.x = (this.game.width/2) - (sheepGroup.width /2 )

  if(!GameData.hasFoundMusicGame) {
    if(this.game.sound.usingWebAudio) {
      this.game.audioStore.voFoundMusicPlaySheep.play()
    }else {
      this.game.audioStore.fx.play('vo-found-music-play-sheep')
    }

    GameData.hasFoundMusicGame = true
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'selected', {'game_level_name':'music'});
}

Keyboard.prototype.shutdown = function() {
  this.game.sound.stopAll()
}

Keyboard.prototype.showBadgeAnimation = function() {

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'complete', {'game_level_name':'music'});

  winGroup = this.game.add.group()

  GameData.disableHud = true

  var graphics = this.game.add.graphics(0,0)
  this.game.add.graphics(this.game.width/2, this.game.height/2)
  graphics.beginFill(0xc1d871)
  graphics.drawRect(0, 0, this.game.width, this.game.height)
  graphics.endFill()
  graphics.alpha = 0
  this.game.add.tween(graphics).to({alpha: .9} , 800, Phaser.Easing.Linear.InOut, true)
  winGroup.add(graphics)

  badge = this.game.add.image(this.game.width/2, 0,'sheep-badge')
  badge.anchor.setTo(.5,.5)
  badge.scale.setTo(.8 * ScreenProperties.gameScale)

  badge.y = -badge.height
  var tween1 = this.game.add.tween(badge).to({y:this.game.height/2} , 1000, Phaser.Easing.Back.InOut, false, 500)
  var tween3 = this.game.add.tween(badge).to({y: -badge.height}, 600, Phaser.Easing.Back.InOut, false, 1800)
  tween1.chain(tween3)

  tween1.start()

  this.game.add.tween(badge.scale).to({x: badge.scale.x * .3, y: badge.scale.y * .3} , 800, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(badge.scale).to({x: badge.scale.x * 1.4, y: badge.scale.y * 1.4} , 800, Phaser.Easing.Back.InOut, true, 1450, 0, true)

  var playButton = this.game.add.button(0, this.game.height/2, 'buttons', playAgain, this, 'replay_over_192.png', 'replay_off_192.png', 'replay_over_192.png', 'replay_over_192.png')
  playButton.scale.setTo(ScreenProperties.gameScale)
  playButton.anchor.setTo(.5)
  playButton.input.useHandCursor = true
  playButton.x -= playButton.width
  winGroup.add(playButton)

  var homeButton = this.game.add.button(this.game.width, this.game.height/2, 'atlas', handleHome, this, 'buttons/homebtn_over_192.png', 'buttons/homebtn_off_192.png', 'buttons/homebtn_over_192.png', 'buttons/homebtn_over_192.png')
  homeButton.scale.setTo(ScreenProperties.gameScale)
  homeButton.anchor.setTo(.5)
  homeButton.input.useHandCursor = true
  homeButton.x += homeButton.width

  this.game.add.tween(playButton).to({x: this.game.width * .66} , 1000, Phaser.Easing.Back.InOut, true, 3100)
  this.game.add.tween(homeButton).to({x: this.game.width * .33} , 1000, Phaser.Easing.Back.InOut, true, 3100)

  winGroup.add(homeButton)

  if(this.game.sound.usingWebAudio) {
    this.game.audioStore.awardBadge.play()
    this.game.audioStore.voSheepBadge.play()
  }else {
     this.game.audioStore.fx.play('vo-sheep-badge')
  }

  homeButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voHome.play()
    }
  }, this)

  playButton.onInputOver.add(function() {
    if(this.game.sound.usingWebAudio && this.game.device.desktop) {
      this.game.audioStore.voPlay.play()
    }
  }, this)
}

function playAgain() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  //TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_level", 'playagain', {'game_level_name':'music'});

  GameData.disableHud = false

  winGroup.destroy()
  winGroup = null
}

function handleHome() {

  if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated || GameData.overlayIsOpen) {
    return
  }

  GameData.disableHud = false

  this.game.sound.stopAll()
  this.game.state.start('lobby')
}

Keyboard.prototype.createSheepButton = function(soundRef) {
	var sheep = new MusicalSheep(this.game, (buttonList.length /5) * (this.game.width * .9), 300 * ScreenProperties.gameScale, soundRef)
	sheepGroup.add(sheep)
  sheep.scale.setTo(ScreenProperties.gameScale)
	buttonList.push( sheep )

  sheep.x += sheep.width/2

	sheep.clickSignal.add(function() {

    if(GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
      return
    }

    if( !gameStarted ) {
      //TOTO ECHO STATS
      //gamesGrid.iStatsActionEvent ("game_level", 'started', {'game_level_name':'music'});
      gameStarted = true
    }

		winCount ++
		if(winCount === 50) {
      winCount = 0
			this.showBadgeAnimation()
		}
	}, this)
}

module.exports = Keyboard
