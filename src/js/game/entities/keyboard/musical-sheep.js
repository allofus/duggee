var Phaser = require('Phaser')
	, _ = require('lodash')
	, MathUtils = require('MathUtils')
	, ScreenProperties = require('../../../lib/screen-properties')
	, GameData = require('GameData')

	, blinkTimeout
	, pointer

var MusicalSheep = function (game, x, y, $soundClip ) {
  Phaser.Sprite.call(this, game, x, y, 'duggee', 'sheep/SHEEP_gameplay0001.png')
	this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputOver.add(handleOver, this)
	this.events.onInputDown.add(handleClick, this)
	this.anchor.setTo(.5)

	this.animations.add('idle', ['sheep/SHEEP_gameplay0001.png'], 15, false)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('sheep/SHEEP_gameplay', 2, 6, '.png', 4), 25, false)
	this.animations.add('bleat', Phaser.Animation.generateFrameNames('sheep/SHEEP_gameplay', 8, 34, '.png', 4), 30, false)

	this.animations.play('idle')

	this.soundClip = $soundClip

	if(this.game.sound.usingWebAudio) {
		this.sound = this.game.audioStore[$soundClip]
	}else {
		this.sound = this.game.audioStore.fx
	}

	blinkTimeout = setTimeout(_.bind(blink,this), MathUtils.getRandomInt(1500,5000))

	if(this.game.device.desktop) {
		pointer = this.game.input.mousePointer
	}else{
		pointer = this.game.input.pointer1
	}

	if(this.game.device.webGL) {
		this.emitter = this.game.add.emitter(this.x + this.width/2 + (64 * ScreenProperties.gameScale), this.y-(100 * ScreenProperties.gameScale), 40)
	}else {
		this.emitter = this.game.add.emitter(this.x + this.width/2 + (64 * ScreenProperties.gameScale), this.y-(100 * ScreenProperties.gameScale), 3)
	}

  this.emitter.makeParticles('atlas', ['music-note-blue.png', 'music-note-green.png', 'music-note-orange.png', 'music-note-red.png', 'music-note-2-red.png' , 'music-note-2-blue.png' , 'music-note-2-green.png' , 'music-note-2-orange.png'])
  this.emitter.setScale(.2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, .2 * ScreenProperties.gameScale, 1 * ScreenProperties.gameScale, 800)
  this.emitter.setXSpeed(-100 * ScreenProperties.gameScale, 100 * ScreenProperties.gameScale)
  this.emitter.setYSpeed(-120 * ScreenProperties.gameScale, -80 * ScreenProperties.gameScale)
  this.emitter.lifespan = 8000
  this.emitter.particleDrag.x = 50 * ScreenProperties.gameScale
  this.emitter.particleDrag.y = 50 * ScreenProperties.gameScale
  this.emitter.gravity = 0
  this.emitter.setAlpha(1, 0, 8000, Phaser.Easing.Linear.None)

  this.clickSignal = new Phaser.Signal()
}

MusicalSheep.prototype = Object.create(Phaser.Sprite.prototype)
MusicalSheep.prototype.constructor = MusicalSheep

MusicalSheep.prototype.update = function() {

	if(this.animations.currentAnim.name === 'blink' && this.animations.currentAnim.isFinished ) {
		this.animations.play('idle')
	}

	if(this.animations.currentAnim.name === 'bleat' && this.animations.currentAnim.isFinished) {
		this.animations.play('idle')
	}
}

MusicalSheep.prototype.destroy = function() {
	clearTimeout(blinkTimeout)
	blinkTimeout = null
}

function blink() {
	if(this.animations.currentAnim.name === 'idle') {
		this.animations.play('blink')
	}

	setTimeout(_.bind(blink,this), MathUtils.getRandomInt(1500,5000))
}

function playBleat() {

	if(GameData.isRotated || GameData.disableHud || GameData.overlayIsOpen) {
		return
	}

	if(this.game.sound.usingWebAudio) {
		this.sound.play()
	}else {
		this.game.audioStore.fx.play(this.soundClip)
	}

	this.clickSignal.dispatch()

	if(this.game.device.webGL) {
		this.emitter.explode(8000, 5)
	}else {
		this.emitter.emitParticle()
	}
}

function handleClick() {
	if(this.animations.currentAnim.name !== 'bleat') {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
    	return
  	}

  	if(GameData.isRotated || GameData.disableHud) {
			return
		}

		setTimeout(_.bind(playBleat, this), 100)
		this.animations.play('bleat')
	}
}

function handleOver() {
	if(pointer.isDown && this.animations.currentAnim.name !== 'bleat') {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
    	return
  	}

  	if(GameData.isRotated || GameData.disableHud) {
			return
		}

		setTimeout(_.bind(playBleat, this), 100)
		this.animations.play('bleat')
	}
}

module.exports = MusicalSheep
