var Phaser = require('Phaser')
  , ScreenProperties = require('Screen-Properties')
  , MathUtils = require('MathUtils')
  , GameData = require('GameData')

  , isStanding
  , interactCtr = 0

var Roli = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'atlas2', 'roli/ROLY_FINALCOMPILE0001.png')
	this.game.add.existing(this)

	this.animations.add('idle', ['roli/ROLY_FINALCOMPILE0001.png'], 25, true)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 2, 6, '.png', 4), 25, false)
	this.animations.add('pump-start', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 7, 9, '.png', 4), 25, false)
	this.animations.add('pump-cycle', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 10, 15, '.png', 4), 25, true)
	this.animations.add('pump-end', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 16, 30, '.png', 4), 25, false)
	this.animations.add('top-cycle', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 31, 78, '.png', 4), 25, true)
	this.animations.add('top-cycle-neutral-return', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 79, 138, '.png', 4), 25, false)

	this.animations.add('stand-idle', ['roli/ROLY_FINALCOMPILE0139.png'], 25, true)
	this.animations.add('stand-blink', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 140, 144, '.png', 4), 25, false)
	this.animations.add('dance1-start', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 145, 148, '.png', 4), 25, false)
	this.animations.add('dance1', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 149, 173, '.png', 4), 25, false)
	this.animations.add('dance1-end', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 174, 186, '.png', 4), 25, false)

	this.animations.add('start-yell-from-sit', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 187, 191, '.png', 4), 25, false)
	this.animations.add('yell', Phaser.Animation.generateFrameNames('roli/ROLY_FINALCOMPILE', 213, 237, '.png', 4), 25, false)


	isStanding = MathUtils.getRandomBoolean()

	this.animations.getAnimation('idle').onLoop.add(function() {
		if( this.animations.currentAnim.loopCount >= 30) {
			this.animations.play('blink')
		}
	}, this)

	this.animations.getAnimation('yell').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('start-yell-from-sit').onComplete.add(function() {
		this.animations.play('yell')

		if(this.game.sound.usingWebAudio) {
				this.game.audioStore.roliShout1.play()
			}else {
				if(MathUtils.getRandomBoolean()) {
					this.game.audioStore.fx.play('roli-shout-1')
				}else {
					this.game.audioStore.fx.play('roli-shout-2')
				}
			}
	}, this)

	this.animations.getAnimation('blink').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('pump-start').onComplete.add(function() {
		this.animations.play('pump-cycle')
		if(this.game.sound.usingWebAudio) {
			this.game.audioStore.spinningTop.play()
		}else {
			this.game.audioStore.fx.play('spinning-top')
		}
	}, this)

	this.animations.getAnimation('pump-cycle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >=2) {
			this.animations.play('pump-end')
		}
	}, this)

	this.animations.getAnimation('pump-end').onComplete.add(function() {
		this.animations.play('top-cycle')
	}, this)

	this.animations.getAnimation('top-cycle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >=2) {
			this.animations.play('top-cycle-neutral-return')
		}
	}, this)

	this.animations.getAnimation('top-cycle-neutral-return').onComplete.add(function() {
		this.animations.play('blink')
	}, this)

	this.animations.getAnimation('dance1-start').onComplete.add(function() {
		this.animations.play('dance1')
	}, this)

	this.animations.getAnimation('dance1').onComplete.add(function() {
		this.animations.play('dance1-end')
	}, this)

	this.animations.getAnimation('dance1-end').onComplete.add(function() {
		this.animations.play('stand-idle')
	}, this)

	var blinkWait = 30

	this.animations.getAnimation('stand-idle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= blinkWait) {
			this.animations.play('stand-blink')
			blinkWait = MathUtils.getRandomInt(30,60)
		}
	}, this)

	this.animations.getAnimation('stand-blink').onComplete.add(function() {
		this.animations.play('stand-idle')
	}, this)

	if(isStanding) {
		this.animations.play('stand-idle')
	}else {
		this.animations.play('idle')
	}

	this.scale.setTo(ScreenProperties.gameScale)

	this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(this.interact,this)
}

Roli.prototype = Object.create(Phaser.Sprite.prototype)
Roli.prototype.constructor = Roli

Roli.prototype.interact = function() {

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
  	return
	}

	if(this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink' || this.animations.currentAnim.name === 'stand-idle' || this.animations.currentAnim.name === 'stand-blink') {

		//TOTO ECHO STATS
  	//gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'roli'});

		if(isStanding) {
			this.animations.play('dance1-start')

			if(this.game.sound.usingWebAudio) {
				if(MathUtils.getRandomBoolean()) {
					this.game.audioStore.roliShout1.play()
				}else {
					this.game.audioStore.roliShout2.play()
				}
			}else {
				if(MathUtils.getRandomBoolean()) {
					this.game.audioStore.fx.play('roli-shout-1')
				}else {
					this.game.audioStore.fx.play('roli-shout-2')
				}
			}
		}else {

			if(interactCtr === 0) {
				this.animations.play('pump-start')
			}else {
				this.animations.play('start-yell-from-sit')
			}

			interactCtr ++

			if( interactCtr >= 2 ) {
				interactCtr = 0
			}
		}
	}
}

module.exports = Roli
