var Phaser = require('Phaser')
	, _ = require('lodash')
	, ScreenProperties = require('Screen-Properties')
	, MathUtils = require('MathUtils')
	, GameData = require('GameData')

	, idleTimeout
	, isOnMainMenu

var Duggee = function (game, x, y, $isOnMainMenu) {
  Phaser.Sprite.call(this, game, x, y, 'duggee', 'duggee/DUGGEE_0001.png')
	this.game.add.existing(this)

	this.animations.add('idle', ['duggee/DUGGEE_0001.png'], 25, true)
	this.animations.add('juggle', Phaser.Animation.generateFrameNames('duggee/DUGGEE_', 28, 108, '.png', 4), 25, false)
	this.animations.add('woof', Phaser.Animation.generateFrameNames('duggee/DUGGEE_', 2, 27, '.png', 4), 25, false)

	this.animations.play('idle')

	this.scale.setTo(1.2 * ScreenProperties.gameScale)
	this.anchor.setTo(.5,.5)

	this.animations.getAnimation('woof').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('juggle').onComplete.add(function() {
		this.animations.play('blink')
	}, this)

	isOnMainMenu = $isOnMainMenu

	var shouldWoof = true

	if(isOnMainMenu) {
		idleTimeout = setTimeout(_.bind(doIdleAnimation, this), 3000)
	}else {
		this.inputEnabled = true
		this.input.useHandCursor = true
		this.events.onInputDown.add(function() {

			if(GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
				return
			}

			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'duggee'});

			if(shouldWoof) {
				this.animations.play('woof')

				if(this.game.sound.usingWebAudio) {
					if(MathUtils.getRandomBoolean()) {
						this.game.audioStore.woof1.play()
					}else {
						this.game.audioStore.woof2.play()
					}
				}else {

					if(MathUtils.getRandomBoolean()) {
						this.game.audioStore.fx.play('woof-1')
					}else {
						this.game.audioStore.fx.play('woof-2')
					}
				}
			}else {
				this.animations.play('juggle')
			}

			shouldWoof = !shouldWoof
		},this)
	}
}

Duggee.prototype = Object.create(Phaser.Sprite.prototype)
Duggee.prototype.constructor = Duggee

function doIdleAnimation() {

	if(this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink') {
		if(MathUtils.getRandomBoolean()) {
			this.animations.play('woof')
			if(this.game.sound.usingWebAudio) {
				if(MathUtils.getRandomBoolean()) {
					this.game.audioStore.woof1.play()
				}else {
					this.game.audioStore.woof2.play()
				}
			}else {
				if(MathUtils.getRandomBoolean()) {
					this.game.audioStore.fx.play('woof-1')
				}else {
					this.game.audioStore.fx.play('woof-2')
				}
			}

		}else {
			this.animations.play('juggle')
		}
	}

	if(isOnMainMenu) {
		idleTimeout = setTimeout(_.bind(doIdleAnimation,this), 10000)
	}
}

Duggee.prototype.destroy = function() {
	clearTimeout(idleTimeout)
	idleTimeout = null
}

module.exports = Duggee
