var Phaser = require('Phaser')
	, ScreenProperties = require('Screen-Properties')
	, GameData = require('GameData')
	, interactCtr = 0

var Happy = function (game, x, y) {

  Phaser.Sprite.call(this, game, x, y, 'atlas', 'happy/HAPPY_FINALCOMPILE0001.png')
  this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(toggleTouch, this)

	this.game.add.existing(this)

	this.animations.add('idle', ['happy/HAPPY_FINALCOMPILE0001.png'], 25, true)
	this.animations.add('begin-plane', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 7, 16, '.png', 4), 25, false)
	this.animations.add('end-plane', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 59, 69, '.png', 4), 25, false)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 2, 6, '.png', 4), 25, false)
	this.animations.add('plane-cycle', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 17, 58, '.png', 4), 25, true)
	this.animations.add('link-to-face', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 70, 77, '.png', 4), 25, false)
	this.animations.add('face-cycle', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 78, 131, '.png', 4), 25, false)
	this.animations.add('link-face-to-idle', Phaser.Animation.generateFrameNames('happy/HAPPY_FINALCOMPILE', 132, 141, '.png', 4), 25, false)

	this.animations.play('idle')

	this.y += 20 * ScreenProperties.gameScale
	this.scale.setTo(ScreenProperties.gameScale)

	this.animations.getAnimation('idle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 30) {
			this.animations.play('blink')
		}
	}, this)

	this.animations.getAnimation('begin-plane').onComplete.add(function() {
		this.animations.play('plane-cycle')
	}, this)

	this.animations.getAnimation('plane-cycle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 1) {
			this.animations.play('end-plane')
		}
	}, this)

	this.animations.getAnimation('end-plane').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('blink').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('link-to-face').onComplete.add(function() {
		this.animations.play('face-cycle')
	}, this)

	this.animations.getAnimation('face-cycle').onComplete.add(function() {
		this.animations.play('link-face-to-idle')
	}, this)

	this.animations.getAnimation('link-face-to-idle').onComplete.add(function() {
		this.animations.play('idle')
	}, this)
}

Happy.prototype = Object.create(Phaser.Sprite.prototype)
Happy.prototype.constructor = Happy

function toggleTouch() {

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
  	return
	}

	if( this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink') {

		//TOTO ECHO STATS
  	//gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'happy'});

		if(interactCtr <= 0) {
			this.animations.play('begin-plane')

			if(this.game.sound.usingWebAudio) {
				this.game.audioStore.fliesPlane.play()
			}else {
				this.game.audioStore.fx.play('flies-plane')
			}
		}else {
			this.animations.play('link-to-face')

			if(this.game.sound.usingWebAudio) {
				this.game.audioStore.beatbox.play()
			}else {
				this.game.audioStore.fx.play('beatbox')
			}
		}

		interactCtr ++

		if( interactCtr >= 2 ) {
			interactCtr = 0
		}
	}
}

module.exports = Happy
