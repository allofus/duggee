var Phaser = require('Phaser')
	, ScreenProperties = require('Screen-Properties')
	, MathUtils = require('MathUtils')
	, GameData = require('GameData')

	, interactionCtr = 0

	, isStanding

var NorrieIdle = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'atlas', 'norrie/NORRIE_FINALCOMPILE_0002.png')
	this.game.add.existing(this)

	this.animations.add('idle', ['norrie/NORRIE_FINALCOMPILE_0002.png'], 25, true)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 3, 7, '.png', 4), 25, false)
	this.animations.add('pour', Phaser.Animation.generateFrameNames('norrie/NORRIE_TEAPOUR', 1, 61, '.png', 4), 25, false)

	this.animations.add('stand-up', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 55, 44, '.png', 4), 25, false)
	this.animations.add('return-to-sit-idle', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 44, 55, '.png', 4), 25, false)
	this.animations.add('stand-neutral', ['norrie/NORRIE_FINALCOMPILE_0056.png'], 25, true)
	this.animations.add('stand-neutral-blink', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 57, 61, '.png', 4), 25, false)
	this.animations.add('dance1-start', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 62, 73, '.png', 4), 25, false)
	this.animations.add('dance1', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 74, 173, '.png', 4), 25, false)
	this.animations.add('dance1-exit', Phaser.Animation.generateFrameNames('norrie/NORRIE_FINALCOMPILE_', 174, 179, '.png', 4), 25, false)

	isStanding = MathUtils.getRandomBoolean()

	if(isStanding) {
		this.animations.play('stand-neutral')
	}else {
		this.animations.play('idle')
	}

	this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(toggleTouch,this)

	this.scale.setTo(ScreenProperties.gameScale)

	this.animations.getAnimation('idle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 30) {
			this.animations.play('blink')
		}
	}, this)

	this.animations.getAnimation('stand-neutral').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 30) {
			this.animations.play('stand-neutral-blink')
		}
	}, this)
}

NorrieIdle.prototype = Object.create(Phaser.Sprite.prototype)
NorrieIdle.prototype.constructor = NorrieIdle

NorrieIdle.prototype.update = function() {
	this.checkAnimations()
}

NorrieIdle.prototype.checkAnimations = function() {

	if(this.animations.currentAnim.isFinished) {
		//play idle if pour is finished
		if(this.animations.currentAnim.name === 'pour' ) {
			this.animations.play('idle')
		}else if(this.animations.currentAnim.name === 'dance1-start') {
			this.animations.play('dance1')
		}else if(this.animations.currentAnim.name === 'dance1') {
			this.animations.play('dance1-exit')
		}else if(this.animations.currentAnim.name === 'dance1-exit') {
			this.animations.play('stand-neutral')
		}
	}
}

function toggleTouch() {

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
  	return
	}

	if(this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink') {
		this.animations.play('pour')

		//TOTO ECHO STATS
  	//gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'norrie'});

		if(this.game.sound.usingWebAudio) {
			if(MathUtils.getRandomBoolean()) {
				this.game.audioStore.norrieGiggle1.play()
			}else {
				this.game.audioStore.norrieGiggle2.play()
			}
    }else {

    	if(MathUtils.getRandomBoolean()) {
				this.game.audioStore.fx.play('norrie-giggle-1')
			}else {
				this.game.audioStore.fx.play('norrie-giggle-2')
			}
    }
	}

	if(this.animations.currentAnim.name === 'stand-neutral-blink' || this.animations.currentAnim.name === 'stand-neutral') {
		this.animations.play('dance1-start')

		if(this.game.sound.usingWebAudio) {
			if(MathUtils.getRandomBoolean()) {
				this.game.audioStore.norrieGiggle1.play()
			}else {
				this.game.audioStore.norrieGiggle2.play()
			}
    }else {
      if(MathUtils.getRandomBoolean()) {
				this.game.audioStore.fx.play('norrie-giggle-1')
			}else {
				this.game.audioStore.fx.play('norrie-giggle-2')
			}
    }
	}
}

module.exports = NorrieIdle
