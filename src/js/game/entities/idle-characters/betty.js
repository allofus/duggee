var Phaser = require('Phaser')
	, ScreenProperties = require('Screen-Properties')
	, GameData = require('GameData')
	, interactCtr

var Betty = function (game, x, y) {

  Phaser.Sprite.call(this, game, x, y, 'atlas2')

  this.scale.setTo(ScreenProperties.gameScale)

  this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(toggleTouch, this)

	this.game.add.existing(this)

	this.animations.add('idle', ['betty/BETTY_FINALCOMPILE0001.png'], 25, true)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 2, 6, '.png', 4), 25, false)
	this.animations.add('read-start', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 7, 13, '.png', 4), 25, false)
	this.animations.add('read-cycle', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 14, 78, '.png', 4), 60, true)
	this.animations.add('read-end', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 79, 84, '.png', 4), 25, false)
	this.animations.add('face', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 85, 175, '.png', 4), 25, false)
	this.animations.add('face-2', Phaser.Animation.generateFrameNames('betty/BETTY_FINALCOMPILE', 176, 245, '.png', 4), 25, false)

	this.animations.getAnimation('idle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 30) {
			this.animations.play('blink')
		}
	}, this)

	this.animations.getAnimation('blink').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('read-start').onComplete.add(function() {
		this.animations.play('read-cycle')
	}, this)

	this.animations.getAnimation('read-cycle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= 5) {
			this.animations.play('read-end')
		}

	}, this)

	this.animations.getAnimation('read-end').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('face').onComplete.add(function() {
		this.animations.play('face-2')
	}, this)

	this.animations.getAnimation('face-2').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	interactCtr = 0

  this.animations.play('idle')
}

Betty.prototype = Object.create(Phaser.Sprite.prototype)
Betty.prototype.constructor = Betty

function toggleTouch() {

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
  	return
	}

	//TOTO ECHO STATS
  //gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'betty'});

	if( this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink') {

		if(interactCtr === 0){
			this.animations.play('face')

			if(this.game.sound.usingWebAudio) {
      	this.game.audioStore.bettyFace.play()
    	}else {
      	this.game.audioStore.fx.play('betty-face')
    	}

		}else if(interactCtr === 1){
			this.animations.play('read-start')
		}

	 	interactCtr ++

		if(interactCtr === 2) {
			interactCtr = 0
	 	}
	}
}

module.exports = Betty
