var Phaser = require('Phaser')
	, MathUtils = require('MathUtils')
	, ScreenProperties = require('Screen-Properties')
	, GameData = require('GameData')

	, reactCtr

var Tag = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'atlas2', 'tag/TAG_FINALCOMPILE0001.png')
	this.game.add.existing(this)

	this.scale.setTo(ScreenProperties.gameScale, ScreenProperties.gameScale)

	this.animations.add('idle', ['tag/TAG_FINALCOMPILE0001.png'], 25, true)
	this.animations.add('blink', Phaser.Animation.generateFrameNames('tag/TAG_FINALCOMPILE', 2, 6, '.png', 4), 25, false)
	this.animations.add('push', Phaser.Animation.generateFrameNames('tag/TAG_FINALCOMPILE', 7, 36, '.png', 4), 25, true)
	this.animations.add('jump-cycle', Phaser.Animation.generateFrameNames('tag/TAG_FINALCOMPILE', 40, 63, '.png', 4), 25, false)
	this.animations.add('jump-exit', Phaser.Animation.generateFrameNames('tag/TAG_FINALCOMPILE', 64, 98, '.png', 4), 25, false)
	this.animations.add('jump-jumpstart', Phaser.Animation.generateFrameNames('tag/TAG_FINALCOMPILE', 37, 39, '.png', 4), 25, false)

	this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(this.react,this)

	var loopWait = 30
	this.animations.getAnimation('idle').onLoop.add(function() {
		if(this.animations.currentAnim.loopCount >= loopWait) {
			loopWait = MathUtils.getRandomInt(30,60)
			this.animations.play('blink')
		}
	}, this)

	this.animations.getAnimation('blink').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	this.animations.getAnimation('push').onLoop.add(function() {
		if( this.animations.currentAnim.loopCount >= 2 ) {
			this.animations.play('idle')
		}
	}, this)

	this.animations.getAnimation('jump-cycle').onComplete.add(function() {
		this.animations.play('jump-exit')
	}, this)

	this.animations.getAnimation('jump-jumpstart').onComplete.add(function() {
		this.animations.play('jump-cycle')
	}, this)

	this.animations.getAnimation('jump-exit').onComplete.add(function() {
		this.animations.play('idle')
	}, this)

	reactCtr = 0

	this.animations.play('idle')
}

Tag.prototype = Object.create(Phaser.Sprite.prototype)
Tag.prototype.constructor = Tag

Tag.prototype.react = function() {

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.isRotated || GameData.overlayIsOpen) {
  	return
	}

	if(this.animations.currentAnim.name === 'idle' || this.animations.currentAnim.name === 'blink') {

		//TOTO ECHO STATS
  	//gamesGrid.iStatsActionEvent ("game_click", 'behaviour', {'game_level':'hub', 'game_behaviour': 'tag'});

		if( reactCtr === 0 ) {
			this.animations.play('push')

			if(this.game.sound.usingWebAudio) {
				this.game.audioStore.train.play()
			}else {
				this.game.audioStore.fx.play('train')
			}
		}else {
			this.animations.play('jump-jumpstart')

			if(this.game.sound.usingWebAudio) {
				this.game.audioStore.tagGiggle.play()
			}else {
				this.game.audioStore.fx.play('tag-giggle')
			}
		}

		reactCtr ++

		if(reactCtr >= 2) {
			reactCtr = 0
		}
	}
}

module.exports = Tag
