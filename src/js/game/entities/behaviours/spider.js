var Phaser = require('Phaser')
	, MathUtils = require('MathUtils')
	, ScreenProperties = require('Screen-Properties')
	, _ = require('lodash')
	, GameData = require('GameData')

var isDown = false
	, animateDownTween
	, animateUpTween
	, timeout

var Spider = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'atlas', 'BBC_IMMERSIVE_SITE_v001_SPIDER.png')
	this.inputEnabled = true
	this.input.useHandCursor = true
	this.events.onInputDown.add(handleClick, this,0)

	this.game.add.existing(this)

	isDown = false
	animateDownTween = this.game.add.tween(this).to({y:-30*ScreenProperties.gameScale}, 1000, Phaser.Easing.Back.InOut)
	animateUpTween = this.game.add.tween(this).to({y:-(this.height + 30 * ScreenProperties.gameScale) }, 1000, Phaser.Easing.Back.InOut)
	animateUpTween.onComplete.add(this.createRandomDelay, this)

	this.createRandomDelay()
}

Spider.prototype = Object.create(Phaser.Sprite.prototype)
Spider.prototype.constructor = Spider

Spider.prototype.destroy = function() {
	clearTimeout(timeout)
}

Spider.prototype.createRandomDelay = function() {

	//set a new timeout
	timeout = setTimeout(_.bind(startAnim, this), MathUtils.getRandomInt(1500,2500))

	//reposition xPos.
	this.x = MathUtils.getRandomInt(100 * ScreenProperties.gameScale, 250 * ScreenProperties.gameScale)
}

function startAnim() {
	animateDownTween.start()
	isDown = true

	if(this.game.sound.usingWebAudio) {
		this.game.audioStore.spiderIn.play()
	}
}

function handleClick(){

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.overlayIsOpen || GameData.disableHud || GameData.isRotated) {
  	return
	}

	if(isDown) {
		isDown = false

		animateDownTween.stop()
		animateUpTween.start()

		if(this.game.sound.usingWebAudio) {
			this.game.audioStore.spiderOut.play()
		}else {
			this.game.audioStore.fx.play('spider-out')
		}
	}
}

module.exports = Spider
