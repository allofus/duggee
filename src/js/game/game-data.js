module.exports =
{
	roomWidth: 1500
  , isMuted: false

  , showRabbitOnBarrelGame: true
  , showChickenOnHideAndSeekGame: true

  , isFirstLobbyOpen: true
  , disableHud: false
  , isRotated: false
  , isFirstClick: true

  , overlayIsOpen: false

  , hasFoundBarrelGame: false
  , hasFoundMusicGame: false
  , hasFoundHideGame: false
}
