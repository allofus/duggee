//http://stackoverflow.com/questions/5574842/best-way-to-check-for-ie-less-than-9-in-javascript-without-library
var ie = (function(){

    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );

    return v > 4 ? v : undef

}())

var properties = require('properties')
  , template = require('../../templates/no-support.jade')
  , rotateTemplate = require('../../templates/rotate-device.jade')
  , brim = require('../lib/brim')
  , jQuery = require('jQuery')
  , viewportSize = require('../lib/viewport-size');

if(window.og) {
  properties.cdnAddress = window.og.gameDir;
  properties.exitButtonURL = window.og.exitGameUrl;
}

var head  = document.getElementsByTagName('head')[0];
var link  = document.createElement('link');
link.id   = 'cssId';
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = properties.cdnAddress + 'style/index.css';
link.media = 'all';
head.appendChild(link);


// if(properties.cdnAddress.indexOf('//') === 0) {
//   properties.cdnAddress.replace('//', 'http://')
// }

jQuery('body').append(rotateTemplate(properties))

function startGame() {

  jQuery('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', properties.cdnAddress +'style/index.css' ) );

  if (navigator.userAgent.indexOf('Silk') > -1) {

    var div = document.getElementById('silk-browser');
    div.style.display = 'inherit';
    return;
  }

  if(ie > 8 || ie === undefined) {

    var Phaser = require('Phaser')
    , _ = require('lodash')
    , states =
      { boot: require('./states/boot.js')
      , preloader: require('./states/preloader.js')
      , lobby: require('./states/lobby')
      , keyboard: require('./states/keyboard')
      , barrelGame: require('./states/barrel-game')
      , mainMenu: require('./states/main-menu')
      , hideAndSeek: require('./states/hide-and-seek')
      }

    if(!window.devicePixelRatio) {
      window.devicePixelRatio = 1
    }

    var div = document.getElementById('og-game-holder');
    var w = div.offsetWidth * window.devicePixelRatio
    , h = div.offsetHeight * window.devicePixelRatio

    , game = new Phaser.Game((h > w) ? h : w, (h > w) ? w : h, Phaser.AUTO, 'og-game-holder')

    // Automatically register each state.
    _.each(states, function(state, key) {
      game.state.add(key, state)
    })

    game.state.start('boot')
  } else {

    $('#og-game-holder').append(template(properties));
  }

  brim.create('og-game-holder', "Please swipe up.");
}

function getScreenOrientation() {
  return (window.innerWidth > window.innerHeight) ? 90 : 0;
}

function checkOrientation() {
  if (getScreenOrientation() === 0) {
    setTimeout(checkOrientation, 20);
  } else {
    clearTimeout(checkOrientation);
    startGame();
  }
}

//http://stackoverflow.com/questions/641857/javascript-window-resize-event
function addEvent(object, type, callback) {
  if (object == null || typeof (object) === 'undefined') {
    return;
  }

  if (object.addEventListener) {
    object.addEventListener(type, callback, false);
  } else if (object.attachEvent) {
    object.attachEvent('on' + type, callback);
  } else {
    object['on' + type] = callback;
  }
}

//addEvent(window, "DOMContentLoaded", checkOrientation);
checkOrientation();