var ScreenProperties = require('./screen-properties')

module.exports = function(){

  var loader = {}
  , MathsUtils = require('MathUtils')
  , title
  , bar
  , barMask

  loader.setupLoader = function($game){
    this.game = $game
    this.game.camera.reset()

    bar = this.game.add.image(this.game.width/2, 180 * ScreenProperties.gameScale, 'preloader-bar')
    bar.scale.setTo(ScreenProperties.gameScale)
    bar.anchor.setTo(.5)

    barMask = this.game.add.graphics(bar.x-(bar.width/2), bar.y-(bar.height/2))
    barMask.beginFill(0xFF3300)
    barMask.drawRect(0, 0, bar.width, bar.height * 1.4)
    barMask.endFill()

    barMask.scale.x = .1 * ScreenProperties.gameScale

    bar.mask = barMask

    title = this.game.add.bitmapText(this.game.width/2, 170 * ScreenProperties.gameScale, 'Berlin-Sans-40pt', 'Loading...0%', 40 * ScreenProperties.gameScale)
    title.x -= title.width/2

    this.game.load.onFileComplete.add(this.fileComplete, this)
    this.game.load.onLoadComplete.add(this.loadComplete, this)
  }

  loader.loadComplete = function(){
    loader.loaded = true
  }

  loader.fileComplete = function(){
    var fauxValue = MathsUtils.limit(this.game.load.progress - 1, 0, 100)

    fauxValue = Math.ceil(fauxValue)

    title.setText('Loading...' + fauxValue + '%')

    if(this.game.load.progress === 100) {
      title.setText('Finding Duggee...')
    }

    title.x = (this.game.width/2) - (title.width/2)
    barMask.scale.x = (fauxValue / 100)
  },

  loader.destroy = function(){
    if( title ){
      title.destroy()
      title = null

      bar.destroy()
      bar = null

      barMask.destroy()
      barMask = null
    }
  }

  loader.loaded = false

  return loader
}
