var ScreenProperties = require('./screen-properties')
  , helpMenu = {}
  , properties = require('properties')
  , GameData = require('GameData')

module.exports = function(game){

	var graphics
	  , closeButton
	  , instructions

	helpMenu.open = function(){

		if(graphics) {
			return
		}

		graphics = game.add.graphics(0, 0)

		if(game.state.getCurrentState().name === 'barrel-game') {
			graphics.beginFill(0xc1d871)
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'help', {'game_level':'barrels'});
		}else if(game.state.getCurrentState().name === 'sheep-game'){
			graphics.beginFill(0xc1d871)
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'help', {'game_level':'music'});
		}else if(game.state.getCurrentState().name === 'hide-and-seek') {
			graphics.beginFill(0x6affca)
			//TOTO ECHO STATS
			//gamesGrid.iStatsActionEvent ("game_click", 'help', {'game_level':'hide'});
		}

		graphics.drawRect(0,0,game.width,game.height)
		graphics.endFill()
		graphics.fixedToCamera = true

		if(game.state.getCurrentState().name === 'barrel-game') {
			if(GameData.showRabbitOnBarrelGame) {
				instructions = game.add.image(game.width/2, game.height/2, 'rabbit-instructions')
			}else {
				instructions = game.add.image(game.width/2, game.height/2, 'monkey-instructions')
			}

		}else if(game.state.getCurrentState().name === 'sheep-game'){
			instructions = game.add.image(game.width/2, game.height/2, 'sheep-instructions')
		}else if(game.state.getCurrentState().name === 'hide-and-seek') {
			if(GameData.showChickenOnHideAndSeekGame) {
				instructions = game.add.image(game.width/2, game.height/2, 'chicken-instructions')
			}else {
				instructions = game.add.image(game.width/2, game.height/2, 'frog-instructions')
			}
		}

		if(instructions) {
			instructions.anchor.setTo(.5)
			instructions.scale.setTo(ScreenProperties.gameScale)
		}

		closeButton = game.add.button(game.width - properties.buttonSize - properties.margin, properties.safeAreaPadding, 'atlas', helpMenu.close, this, 'buttons/Close_off_48.png', 'buttons/Close_over_48.png', 'buttons/Close_over_48.png', 'buttons/Close_over_48.png')
		closeButton.input.useHandCursor = true
		closeButton.fixedToCamera = true
		closeButton.scale.setTo(window.devicePixelRatio)

		closeButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voClose.play()
			}
		})
	}

	helpMenu.close = function(){
		if(!graphics || GameData.isRotated) {
			return
		}

		graphics.destroy()
		closeButton.destroy()
		instructions.destroy()

		graphics = null
		closeButton = null
		instructions = null
	}

	return helpMenu
}
