var display =
  { logicWidth: 832
  , logicHeight: 508
  , gameWidth: null
  , gameHeight: null
  , gameScale: 1
  , screen: ''
  }

function setupInitial() {
  var div = document.getElementById('gameHolder')

  //if not found, we are running on the BBC site
  if(!div) {
    div = document.getElementById('og-game-holder')
  }

  var w = div.offsetWidth// * window.devicePixelRatio
    , h = div.offsetHeight// * window.devicePixelRatio

  //By default we set
  display.screen = 'HD'
  display.srx = Math.max(w, h)
  display.sry = Math.min(w, h)
  display.gameWidth = w
}

setupInitial()

display.calculateSize = function() {

  setupInitial()

  var div = document.getElementById('gameHolder')

  //if not found, we are running on the BBC site
  if(!div) {
    div = document.getElementById('og-game-holder')
  }

  var w = div.offsetWidth
  , h = div.offsetHeight

  var ratio = display.logicWidth / display.logicHeight
  display.gameHeight = display.gameWidth / ratio
  display.gameScale = w / display.logicWidth

  var extraHeight = display.logicHeight
  extraHeight *= display.gameScale
  var scaleUpPercantage = h / extraHeight
  display.gameScale *= scaleUpPercantage

  display.gameScale *= window.devicePixelRatio
}

//We need these methods later to convert the logical game position to display position, So convertWidth(logicWidth) will be right edge for all screens
display.convertWidth = function(value) {
  return value / display.logicWidth * display.gameWidth
}

display.convertHeight = function(value) {
  return value / display.logicHeight * display.gameHeight
}

module.exports = display
