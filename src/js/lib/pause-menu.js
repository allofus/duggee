var ScreenProperties = require('./screen-properties')
  , pauseMenu = {}
  , GameData = require('GameData')
  , HelpOverlay = require('./help-menu')

  , properties = require('properties')

module.exports = function(game){

	var bg
		, graphics
		, homeButton
		, muteOnButton
		, muteOffButton
		, helpButton
		, playButton
		, helpOverlay
		, callback

	pauseMenu.open = function($callback){

		if(homeButton) {
			return
		}

		GameData.overlayIsOpen = true

		callback = $callback

		graphics = game.add.graphics(0, 0)
		graphics.beginFill(0xdd8c33)
		graphics.drawRect(0,0,game.width,game.height)
		graphics.endFill()

		bg = game.add.image(game.width/2,game.height/2,'pause-bg')
		bg.scale.setTo(ScreenProperties.gameScale)
		bg.anchor.setTo(.5)

		homeButton = game.add.button(properties.margin, properties.safeAreaPadding, 'atlas', handleHome, this, 'buttons/Home_off_48.png', 'buttons/Home_over_48.png', 'buttons/Home_over_48.png', 'buttons/Home_over_48.png')
		homeButton.input.useHandCursor = true
		homeButton.scale.setTo(window.devicePixelRatio)
		homeButton.onInputOver.add(function() {
	    if(game.sound.usingWebAudio && game.device.desktop) {
	      game.audioStore.voHome.play()
	    }
	  }, this)

		muteOnButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleMute, this, 'buttons/No_Sound_off_48.png', 'buttons/No_Sound_over_48.png', 'buttons/No_Sound_off_48.png', 'buttons/No_Sound_off_48.png')
		muteOnButton.input.useHandCursor = true
		muteOnButton.ignoreAutoTab = true
		muteOnButton.onInputOver.add(function() {
	    if(game.sound.usingWebAudio && game.device.desktop) {
	      game.audioStore.voMute.play()
	    }
	  }, this)
	  muteOnButton.scale.setTo(window.devicePixelRatio)

		muteOffButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleMute, this, 'buttons/Sound_off_48.png', 'buttons/Sound_over_48.png', 'buttons/Sound_off_48.png', 'buttons/Sound_off_48.png')
		muteOffButton.input.useHandCursor = true
		muteOffButton.onInputOver.add(function() {
	    if(game.sound.usingWebAudio && game.device.desktop) {
	      game.audioStore.voMute.play()
	    }
	  }, this)
	  muteOffButton.scale.setTo(window.devicePixelRatio)

		muteOnButton.visible = GameData.isMuted
		muteOffButton.visible = !GameData.isMuted

		if(game.state.getCurrentState().name !== 'main menu' && game.state.getCurrentState().name !== 'lobby') {
			helpButton = game.add.button(muteOnButton.x - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', handleShowHelp, this, 'buttons/Help_off_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png')
			helpButton.input.useHandCursor = true
			helpButton.scale.setTo(window.devicePixelRatio)
			helpButton.onInputOver.add(function() {
		    if(game.sound.usingWebAudio && game.device.desktop) {
		      game.audioStore.voHelp.play()
		    }
		  }, this)
		}

		playButton = game.add.button(game.width/2, 318 * ScreenProperties.gameScale, 'atlas', pauseMenu.close, this, 'buttons/play-hover.png', 'buttons/play.png', 'buttons/play-hover.png', 'buttons/play-hover.png')
  	playButton.scale.setTo(ScreenProperties.gameScale)
		playButton.anchor.setTo(.5)
		playButton.input.useHandCursor = true
		playButton.onInputOver.add(function() {
	    if(game.sound.usingWebAudio && game.device.desktop) {
	      game.audioStore.voPlay.play()
	    }
	  }, this)

		helpOverlay = new HelpOverlay(game)
	}

	function handleShowHelp() {
		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated) {
    	return
  	}
		helpOverlay.open()
	}

	function toggleMute() {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated) {
    	return
  	}

		GameData.isMuted = !GameData.isMuted

		muteOffButton.visible = !GameData.isMuted
		muteOnButton.visible = GameData.isMuted

		game.sound.mute = GameData.isMuted
	}

	function handleHome() {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.isRotated) {
    	return
  	}

  	GameData.overlayIsOpen = false

  	if(game.state.getCurrentState().name === 'barrel-game') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'home', {'game_level':'barrels'});
		}else if(game.state.getCurrentState().name === 'sheep-game'){
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'home', {'game_level':'music'});
		}else if(game.state.getCurrentState().name === 'hide-and-seek') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'home', {'game_level':'hide'});
		}else if(game.state.getCurrentState().name === 'lobby') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_click", 'home', {'game_level':'hub'});
		}

		game.sound.stopAll()
		game.state.start('lobby')
	}

	pauseMenu.close = function(){

		if(callback) {
			callback()
		}

		GameData.overlayIsOpen = false

		callback = null

		graphics.destroy()
		graphics = null

		homeButton.destroy()
		homeButton = null

		bg.destroy()
		bg = null

		playButton.destroy()
		playButton = null

		muteOnButton.destroy()
		muteOnButton = null

		muteOffButton.destroy()
		muteOffButton = null

		if( helpButton ) {
			helpButton.destroy()
			helpButton = null
		}

		helpOverlay.close()
		helpOverlay = null
	}

	return pauseMenu
}
