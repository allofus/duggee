var ScreenProperties = require('./screen-properties')
, GameData = require('GameData')
, rotateOverlay = {}

module.exports = function(game){

	rotateOverlay.open = function(){

		GameData.isRotated = !GameData.isRotated

		//TOTO ECHO STATS
  	//gamesGrid.iStatsActionEvent ("game_orientation", 'portrait')
  	console.log('rotated', GameData.isRotated)
	}

	game.scale.onOrientationChange.add(rotateOverlay.open, this)

	rotateOverlay.destroy = function() {
		game.scale.onOrientationChange.remove(rotateOverlay.open, this)
	}

	return rotateOverlay
}
