var PauseMenu = require('./pause-menu')
  , GameData = require('GameData')
  , HelpOverlay = require('./help-menu')
  , properties = require('properties')

  , puaseMenu
  , helpOverlay

  , RotateOverlay = require('./rotate-overlay')
  , rO

module.exports = function(game, showHome, showHelp, showPause, showMute, pauseCallback){

//	options = new Options(game)
	helpOverlay = new HelpOverlay(game)
	puaseMenu = new PauseMenu(game)

	if(showMute) {
		var muteOnButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleMute, this, 'buttons/No_Sound_off_48.png', 'buttons/No_Sound_over_48.png', 'buttons/No_Sound_off_48.png', 'buttons/No_Sound_off_48.png')
		muteOnButton.input.useHandCursor = true
		muteOnButton.ignoreAutoTab = true
		muteOnButton.scale.setTo(window.devicePixelRatio)

		var muteOffButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleMute, this, 'buttons/Sound_off_48.png', 'buttons/Sound_over_48.png', 'buttons/Sound_off_48.png', 'buttons/Sound_off_48.png')
		muteOffButton.input.useHandCursor = true
		muteOffButton.scale.setTo(window.devicePixelRatio)

		muteOnButton.visible = GameData.isMuted
		muteOffButton.visible = !GameData.isMuted

		muteOffButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voMute.play()
			}
		})

		muteOnButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voMute.play()
			}
		})
	}

	var helpButton

	if(showHelp && !showMute) {
		helpButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleOptions, this, 'buttons/Help_off_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png')
		helpButton.input.useHandCursor = true
		helpButton.scale.setTo(window.devicePixelRatio)
	}else if(showHelp && showMute) {
		helpButton = game.add.button(muteOnButton.x - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleOptions, this, 'buttons/Help_off_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png', 'buttons/Help_over_48.png')
		helpButton.input.useHandCursor = true
		helpButton.scale.setTo(window.devicePixelRatio)
	}

	if(showHelp) {
		helpButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voHelp.play()
			}
		})
	}

	if(showPause) {
		var pauseButton = game.add.button(game.width - properties.margin - properties.buttonSize, properties.safeAreaPadding, 'atlas', toggleOptions, this, 'buttons/Pause_off_48.png', 'buttons/Pause_over_48.png', 'buttons/Pause_over_48.png', 'buttons/Pause_over_48.png')
		pauseButton.input.useHandCursor = true

		pauseButton.scale.setTo(window.devicePixelRatio)

		pauseButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voPause.play()
			}
		})
	}

	var homeButton

	if(showHome) {
		//home button
		homeButton = game.add.button(properties.margin, properties.safeAreaPadding, 'atlas', handleHome, this, 'buttons/Back_off_48.png', 'buttons/Back_over_48.png', 'buttons/Back_over_48.png', 'buttons/Back_over_48.png')
		homeButton.input.useHandCursor = true

		homeButton.scale.setTo(window.devicePixelRatio)

		homeButton.onInputOver.add(function() {
			if(game.sound.usingWebAudio && game.device.desktop) {
				game.audioStore.voBack.play()
			}
		})
	}

	function handleHome() {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.disableHud || GameData.isRotated || GameData.overlayIsOpen) {
			return
		}

		homeButton.inputEnabled = false
  	game.sound.stopAll()
		game.state.start('lobby')
	}

	function toggleMute() {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.disableHud || GameData.isRotated || GameData.overlayIsOpen) {
			return
		}

		if(GameData.isFirstClick) {
    	//TOTO ECHO STATS
    	//gamesGrid.iStatsActionEvent ("game_first_click", 'sound_off', {'game_level':'start'});
    	GameData.isFirstClick = false
  	}



		GameData.isMuted = !GameData.isMuted

		muteOffButton.visible = !GameData.isMuted
		muteOnButton.visible = GameData.isMuted

		game.sound.mute = GameData.isMuted

		var result

		if( GameData.isMuted ) {
			result = 'on'
		}else {
			result = 'off'
		}

		if(game.state.getCurrentState().name === 'barrel-game') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_sound", result, {'game_level':'barrels'});
		}else if(game.state.getCurrentState().name === 'sheep-game'){
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_sound", result, {'game_level':'music'});
		}else if(game.state.getCurrentState().name === 'hide-and-seek') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_sound", result, {'game_level':'hide'});
		}else if(game.state.getCurrentState().name === 'lobby') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_sound", result, {'game_level':'hub'});
		}else if(game.state.getCurrentState().name === 'main menu') {
			//TOTO ECHO STATS
  		//gamesGrid.iStatsActionEvent ("game_sound", result, {'game_level':'start'});
		}
	}

	function toggleOptions() {

		if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1 || GameData.disableHud || GameData.isRotated || GameData.overlayIsOpen) {
			return
		}

		puaseMenu.open(pauseCallback)

		if( pauseCallback ) {
			pauseCallback()
		}
	}

	rO = new RotateOverlay(game)

	if(Phaser.DOM.getScreenOrientation().indexOf('landscape') === -1) {
		rO.open()
	}
}
